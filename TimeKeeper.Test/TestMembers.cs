﻿using Library.DAL;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;
using TimeKeeper.API.Controllers;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain;
using TimeKeeper.DTO.Models;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.Test
{
    [TestFixture]
    public class TestMembers
    {
        public TestContext context;
        public IRepository<Member> members;
        public IDeltaLog logger;

        public TestMembers()
        {
            context = new TestContext();
            context.Seed();
            members = new Repository<Member>(context);
            logger = new DeltaLogger();
        }

        [Test]
        public async Task GetAllMembers()
        {
            var members = new MembersController(context, logger);

            var response = await members.GetAll() as ObjectResult;
            var value = response.Value as List<MemberModel>;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(3, value.Count);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetMemberById(int id)
        {
            var member = new MembersController(context, logger);

            var response = await member.Get(id) as ObjectResult;
            var value = response.Value as MemberModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
        }

        [TestCase(55)]
        public async Task GetByWrongId(int id)
        {
            var member = new MembersController(context, logger);

            var response = await member.Get(id) as ObjectResult;

            Assert.AreEqual(400, response.StatusCode);
        }

        [Test]
        public async Task InsertMember()
        {
            var member = new MembersController(context, logger);

            var response = await member.Post(new Member
            {
                Id = 4,
                Employee = context.Employees.Find(1),
                Team = context.Teams.Find(2),
                Role = context.Roles.Find(2),
                HoursWeekly = 30m,
                Status = context.MemberStatuses.Find(1)
            }) as ObjectResult;
            var value = response.Value as MemberModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(30m, value.HoursWeekly);
        }

        [TestCase(3)]
        public async Task ChangeMember(int id)
        {
            var member = new MembersController(context, logger);
            Member m = new Member
            {
                Id = 3,
                Employee = context.Employees.Find(1),
                Team = context.Teams.Find(2),
                Role = context.Roles.Find(2),
                HoursWeekly = 35m,
                Status = context.MemberStatuses.Find(1)
            };

            var response = await member.Put(m, id) as ObjectResult;
            var value = response.Value as MemberModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
            Assert.AreEqual(35m, value.HoursWeekly);
        }

        [TestCase(4)]
        public async Task DeleteMember(int id)
        {
            var member = new MembersController(context, logger);

            var response = await member.Delete(id) as ObjectResult;

            var state = await member.GetAll() as ObjectResult;
            var value = state.Value as List<MemberModel>;

            Assert.AreEqual(200, state.StatusCode);
            Assert.AreEqual(3, value.Count);
        }
    }
}