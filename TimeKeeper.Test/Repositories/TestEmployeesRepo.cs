﻿using Library.DAL;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain;

namespace TimeKeeper.Test.Repositories
{
    [TestFixture]
    public class TestEmployeesRepo
    {
        private static TestContext context;
        private static IRepository<Employee> employees;

        public TestEmployeesRepo()
        {
            context = new TestContext();
            context.Seed();
            employees = new Repository<Employee>(context);
        }

        [Test]
        public async Task GetAll()
        {
            var collection = await employees.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetById(int id)
        {
            Employee employee = await employees.Get(id);
            Assert.False(employee == null);
            Assert.AreEqual(id, employee.Id);
        }

        [Test]
        public async Task GetByWrongId()
        {
            Employee employee = await employees.Get(55);
            Assert.IsNull(employee);
        }

        [Test]
        public void InsertEmployee()
        {
            Employee e = new Employee
            {
                FirstName = "Tzukuru",
                LastName = "Tazaki",
                Image = "TT",
                Email = "tzukuru@tazaki.com",
                Phone = "061100100",
                BirthDate = new DateTime(1980, 2, 28),
                BeginDate = new DateTime(2017, 8, 13),
                EndDate = null,
                Status = context.EmployeeStatuses.Find(1),
                Position = context.PositionTypes.Find(1)
            };
            employees.Insert(e);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual(4, e.Id);
        }

        [Test]
        public void UpdateEmployee()
        {
            int id = 4;
            Employee e = new Employee
            {
                Id = id,
                FirstName = "Toru",
                LastName = "Watanabe",
                Image = "TW",
                Email = "toru@watanabe.com",
                Phone = "061100100",
                BirthDate = new DateTime(1980, 2, 28),
                BeginDate = new DateTime(2017, 8, 13),
                EndDate = null,
                Status = context.EmployeeStatuses.Find(1),
                Position = context.PositionTypes.Find(1)
            };
            employees.Update(e, id);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual("Toru", e.FirstName);
            Assert.AreEqual("Watanabe", e.LastName);
        }

        [Test]
        public void WrongUpdate()
        {
            int id = 55;
            Employee e = new Employee
            {
                Id = 55,
                FirstName = "Toru",
                LastName = "Watanabe",
                Image = "TW",
                Email = "toru@watanabe.com",
                Phone = "061100100",
                BirthDate = new DateTime(1980, 2, 28),
                BeginDate = new DateTime(2017, 8, 13),
                EndDate = null,
                Status = context.EmployeeStatuses.Find(1),
                Position = context.PositionTypes.Find(1)
            };
            employees.Update(e, id + 1);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }

        [Test]
        public async Task DeleteEmployee()
        {
            employees.Delete(4);
            int N = context.SaveChanges();
            var collection = await employees.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [Test]
        public void WrongDelete()
        {
            employees.Delete(55);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }
    }
}