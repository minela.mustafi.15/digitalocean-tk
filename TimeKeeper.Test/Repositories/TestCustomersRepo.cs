﻿using Library.DAL;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Test.Repositories
{
    [TestFixture]
    public class TestCustomersRepo
    {
        private static TestContext context;
        private static IRepository<Customer> customers;

        public TestCustomersRepo()
        {
            context = new TestContext();
            context.Seed();
            customers = new Repository<Customer>(context);
        }

        [Test]
        public async Task GetAll()
        {
            var collection = await customers.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetById(int id)
        {
            Customer customer = await customers.Get(id);
            Assert.False(customer == null);
            Assert.AreEqual(id, customer.Id);
        }

        [Test]
        public async Task GetByWrongId()
        {
            Customer customer = await customers.Get(55);
            Assert.IsNull(customer);
        }

        [Test]
        public void InsertCustomer()
        {
            Customer c = new Customer
            {
                Name = "New client",
                Image = "image_nc",
                Contact = "NC contact",
                Email = "ncemail@email.nc",
                Phone = "033225883",
                Address = { Road = "Ulica nc", City = "Sa", Country = "BiH", ZipCode = "71000" },
                Status = context.CustomerStatuses.Find(1)
            };
            customers.Insert(c);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual(4, c.Id);
        }

        [Test]
        public void UpdateCustomer()
        {
            int id = 4;
            Customer c = new Customer
            {
                Id = id,
                Name = "New client",
                Image = "image_nc",
                Contact = "NC contact",
                Email = "ncemail@email.nc",
                Phone = "033225883",
                Address = { Road = "Ulica nc", City = "Sa", Country = "BiH", ZipCode = "71000" },
                Status = context.CustomerStatuses.Find(1)
            };
            customers.Update(c, id);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual("New client", c.Name);
        }

        [Test]
        public void WrongUpdate()
        {
            int id = 55;
            Customer c = new Customer
            {
                Id = 55,
                Name = "New client",
                Image = "image_nc",
                Contact = "NC contact",
                Email = "ncemail@email.nc",
                Phone = "033225883",
                Address = { Road = "Ulica nc", City = "Sa", Country = "BiH", ZipCode = "71000" },
                Status = context.CustomerStatuses.Find(1)
            };
            customers.Update(c, id + 1);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }

        [Test]
        public async Task DeleteCustomer()
        {
            customers.Delete(4);
            int N = context.SaveChanges();
            var collection = await customers.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [Test]
        public void WrongDelete()
        {
            customers.Delete(55);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }
    }
}