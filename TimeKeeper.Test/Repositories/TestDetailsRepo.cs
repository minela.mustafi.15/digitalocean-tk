﻿using Library.DAL;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Test.Repositories
{
    [TestFixture]
    public class TestDetailsRepo
    {
        private static TestContext context;
        private static IRepository<Detail> details;

        public TestDetailsRepo()
        {
            context = new TestContext();
            context.Seed();
            details = new Repository<Detail>(context);
        }

        [Test]
        public async Task GetAll()
        {
            var collection = await details.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetById(int id)
        {
            Detail detail = await details.Get(id);
            Assert.False(detail == null);
            Assert.AreEqual(id, detail.Id);
        }

        [Test]
        public async Task GetByWrongId()
        {
            Detail detail = await details.Get(55);
            Assert.IsNull(detail);
        }

        [Test]
        public void InsertDetail()
        {
            Detail d = new Detail
            {
                Day = context.Calendar.Find(1),
                Project = context.Projects.Find(1),
                Description = "New detail",
                Hours = 30
            };
            details.Insert(d);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual(4, d.Id);
        }

        [Test]
        public void UpdateDetail()
        {
            int id = 4;
            Detail d = new Detail
            {
                Id = id,
                Day = context.Calendar.Find(1),
                Project = context.Projects.Find(1),
                Description = "Changed detail",
                Hours = 30
            };
            details.Update(d, id);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual(id, d.Id);
            Assert.AreEqual("Changed detail", d.Description);
        }

        [Test]
        public void WrongUpdate()
        {
            int id = 55;
            Detail d = new Detail
            {
                Id = 55,
                Day = context.Calendar.Find(1),
                Project = context.Projects.Find(1),
                Description = "Changed detail",
                Hours = 30
            };
            details.Update(d, id + 1);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }

        [Test]
        public async Task DeleteDetail()
        {
            details.Delete(4);
            int N = context.SaveChanges();
            var collection = await details.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [Test]
        public void WrongDelete()
        {
            details.Delete(55);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }
    }
}