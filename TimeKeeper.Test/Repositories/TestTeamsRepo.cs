﻿using Library.DAL;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain;

namespace TimeKeeper.Test.Repositories
{
    [TestFixture]
    public class TestTeamsRepo
    {
        private static TestContext context;
        private static IRepository<Team> teams;

        public TestTeamsRepo()
        {
            context = new TestContext();
            context.Seed();
            teams = new Repository<Team>(context);
        }

        [Test]
        public async Task GetAll()
        {
            var collection = await teams.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetById(int id)
        {
            Team team = await teams.Get(id);
            Assert.False(team == null);
            Assert.AreEqual(id, team.Id);
        }

        [Test]
        public async Task GetByWrongId()
        {
            Team team = await teams.Get(55);
            Assert.IsNull(team);
        }

        [Test]
        public void InsertTeam()
        {
            Team t = new Team
            {
                Name = "Yellow"
            };
            teams.Insert(t);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual(4, t.Id);
        }

        [Test]
        public void UpdateTeam()
        {
            int id = 4;
            Team t = new Team
            {
                Id = id,
                Name = "Purple"
            };
            teams.Update(t, id);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual("Purple", t.Name);
        }

        [Test]
        public void WrongUpdate()
        {
            int id = 55;
            Team t = new Team
            {
                Id = 55,
                Name = "Orange"
            };
            teams.Update(t, id + 1);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }

        [Test]
        public async Task DeleteTeam()
        {
            teams.Delete(4);
            int N = context.SaveChanges();
            var collection = await teams.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [Test]
        public void WrongDelete()
        {
            teams.Delete(55);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }
    }
}