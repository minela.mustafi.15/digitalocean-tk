﻿using Library.DAL;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain;

namespace TimeKeeper.Test.Repositories
{
    [TestFixture]
    public class TestRolesRepo
    {
        private static TestContext context;
        private static IRepository<Role> roles;

        // initialize database
        public TestRolesRepo()
        {
            context = new TestContext();
            context.Seed();
            roles = new Repository<Role>(context);
        }

        [Test]
        public async Task GetAll()
        {
            var collection = await roles.Get();
            Assert.AreEqual(2, collection.Count());
        }

        [TestCase(1)]
        [TestCase(2)]
        public async Task GetById(int id)
        {
            Role role = await roles.Get(id);
            Assert.False(role == null);
            Assert.AreEqual(id, role.Id);
        }

        [Test]
        public async Task GetByWrongId()
        {
            Role role = await roles.Get(55);
            Assert.IsNull(role);
        }

        [Test]
        public void InsertRole()
        {
            Role r = new Role
            {
                Id = 3,
                Name = "UX/UI",
                HourlyPrice = 17,
                MonthlyPrice = 2700
            };
            roles.Insert(r);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual(3, r.Id);
        }

        [Test]
        public void UpdateRole()
        {
            int id = 3;
            Role r = new Role
            {
                Id = id,
                Name = "UX/UI",
                HourlyPrice = 17,
                MonthlyPrice = 2800
            };
            roles.Update(r, id);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual("UX/UI", r.Name);
        }

        [Test]
        public void WrongUpdate()
        {
            int id = 55;
            Role r = new Role
            {
                Id = 55,
                Name = "UX/UI",
                HourlyPrice = 17,
                MonthlyPrice = 2800
            };
            roles.Update(r, id + 1);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }

        [Test]
        public async Task DeleteRole()
        {
            roles.Delete(3);
            int N = context.SaveChanges();
            var collection = await roles.Get();
            Assert.AreEqual(2, collection.Count());
        }

        [Test]
        public void WrongDelete()
        {
            roles.Delete(55);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }
    }
}