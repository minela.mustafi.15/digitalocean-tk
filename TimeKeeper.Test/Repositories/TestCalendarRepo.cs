﻿using Library.DAL;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Test.Repositories
{
    [TestFixture]
    public class TestCalendarRepo
    {
        private static TestContext context;
        private static IRepository<Day> days;

        public TestCalendarRepo()
        {
            context = new TestContext();
            context.Seed();
            days = new Repository<Day>(context);
        }

        [Test]
        public async Task GetAll()
        {
            var collection = await days.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetById(int id)
        {
            Day day = await days.Get(id);
            Assert.False(day == null);
            Assert.AreEqual(id, day.Id);
        }

        [Test]
        public async Task GetByWrongId()
        {
            var day = days.Get(55);
            await day;
            Assert.IsNull(day);
        }

        [Test]
        public void InsertDay()
        {
            Day d = new Day
            {
                Date = new DateTime(2019, 05, 05),
                Employee = context.Employees.Find(1),
                Type = context.DayTypes.Find(1)
            };
            days.Insert(d);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual(4, d.Id);
        }

        [Test]
        public void UpdateDay()
        {
            int id = 4;
            Day d = new Day
            {
                Id = id,
                Date = new DateTime(2019, 10, 10),
                Employee = context.Employees.Find(1),
                Type = context.DayTypes.Find(1)
            };
            days.Update(d, id);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual(id, d.Id);
        }

        [Test]
        public void WrongUpdate()
        {
            int id = 55;
            Day d = new Day
            {
                Id = 55,
                Date = new DateTime(2019, 10, 10),
                Employee = context.Employees.Find(1),
                Type = context.DayTypes.Find(1)
            };
            days.Update(d, id + 1);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }

        [Test]
        public async Task DeleteDay()
        {
            days.Delete(4);
            int N = context.SaveChanges();
            var collection = await days.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [Test]
        public void WrongDelete()
        {
            days.Delete(55);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }
    }
}