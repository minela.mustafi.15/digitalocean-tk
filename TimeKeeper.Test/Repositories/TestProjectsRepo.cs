﻿using Library.DAL;
using NUnit.Framework;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Test.Repositories
{
    [TestFixture]
    public class TestProjectsRepo
    {
        private static TestContext context;
        private static IRepository<Project> projects;

        public TestProjectsRepo()
        {
            context = new TestContext();
            context.Seed();
            projects = new Repository<Project>(context);
        }

        [Test]
        public async Task GetAll()
        {
            var collection = await projects.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetById(int id)
        {
            Project project = await projects.Get(id);
            Assert.False(project == null);
            Assert.AreEqual(id, project.Id);
        }

        [Test]
        public async Task GetByWrongId()
        {
            Project project = await projects.Get(55);
            Assert.IsNull(project);
        }

        [Test]
        public void InsertProject()
        {
            Project p = new Project
            {
                Id = 4,
                Name = "New project",
                Description = "Project New Project",
                Customer = context.Customers.Find(1),
                Team = context.Teams.Find(1),
                BeginDate = new DateTime(2018, 1, 30),
                EndDate = null,
                Status = context.ProjectStatuses.Find(1),
                Pricing = context.PricingTypes.Find(1),
                Ammount = 10000
            };
            projects.Insert(p);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual(4, p.Id);
        }

        [Test]
        public void UpdateProject()
        {
            int id = 4;
            Project p = new Project
            {
                Id = id,
                Name = "New project",
                Description = "Project New Project",
                Customer = context.Customers.Find(1),
                Team = context.Teams.Find(1),
                BeginDate = new DateTime(2018, 1, 30),
                EndDate = null,
                Status = context.ProjectStatuses.Find(1),
                Pricing = context.PricingTypes.Find(1),
                Ammount = 10000
            };
            projects.Update(p, id);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual("New project", p.Name);
        }

        [Test]
        public void WrongUpdate()
        {
            int id = 55;
            Project p = new Project
            {
                Id = 55,
                Name = "New new project",
                Description = "Project New Project",
                Customer = context.Customers.Find(1),
                Team = context.Teams.Find(1),
                BeginDate = new DateTime(2018, 1, 30),
                EndDate = null,
                Status = context.ProjectStatuses.Find(1),
                Pricing = context.PricingTypes.Find(1),
                Ammount = 10000
            };
            projects.Update(p, id + 1);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }

        [Test]
        public async Task DeleteProject()
        {
            projects.Delete(4);
            int N = context.SaveChanges();
            var collection = await projects.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [Test]
        public void WrongDelete()
        {
            projects.Delete(55);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }
    }
}