﻿using Library.DAL;
using NUnit.Framework;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain;

namespace TimeKeeper.Test.Repositories
{
    [TestFixture]
    public class TestMembersRepo
    {
        private static TestContext context;
        private static IRepository<Member> members;

        public TestMembersRepo()
        {
            context = new TestContext();
            context.Seed();
            members = new Repository<Member>(context);
        }

        [Test]
        public async Task GetAll()
        {
            var collection = await members.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetById(int id)
        {
            Member member = await members.Get(id);
            Assert.False(member == null);
            Assert.AreEqual(id, member.Id);
        }

        [Test]
        public async Task GetByWrongId()
        {
            Member member = await members.Get(55);
            Assert.IsNull(member);
        }

        [Test]
        public void InsertMember()
        {
            Member m = new Member
            {
                Employee = context.Employees.Find(1),
                Team = context.Teams.Find(2),
                Role = context.Roles.Find(2),
                HoursWeekly = 30m,
                Status = context.MemberStatuses.Find(1)
            };
            members.Insert(m);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual(4, m.Id);
        }

        [Test]
        public void UpdateMember()
        {
            int id = 4;
            Member m = new Member
            {
                Id = id,
                Employee = context.Employees.Find(1),
                Team = context.Teams.Find(2),
                Role = context.Roles.Find(2),
                HoursWeekly = 35m,
                Status = context.MemberStatuses.Find(1)
            };
            members.Update(m, id);
            int N = context.SaveChanges();
            Assert.AreEqual(1, N);
            Assert.AreEqual(35m, m.HoursWeekly);
        }

        [Test]
        public void WrongUpdate()
        {
            int id = 55;
            Member m = new Member
            {
                Id = 55,
                Employee = context.Employees.Find(1),
                Team = context.Teams.Find(2),
                Role = context.Roles.Find(2),
                HoursWeekly = 35m,
                Status = context.MemberStatuses.Find(1)
            };
            members.Update(m, id + 1);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }

        [Test]
        public async Task DeleteMember()
        {
            members.Delete(4);
            int N = context.SaveChanges();
            var collection = await members.Get();
            Assert.AreEqual(3, collection.Count());
        }

        [Test]
        public void WrongDelete()
        {
            members.Delete(55);
            int N = context.SaveChanges();
            Assert.AreEqual(0, N);
        }
    }
}