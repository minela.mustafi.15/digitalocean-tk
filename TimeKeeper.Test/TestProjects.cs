﻿using Library.DAL;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TimeKeeper.API.Controllers;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Models;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.Test
{
    [TestFixture]
    public class TestProjects
    {
        public TestContext context;
        public IRepository<Project> projects;
        public IDeltaLog logger;

        public TestProjects()
        {
            context = new TestContext();
            context.Seed();
            projects = new Repository<Project>(context);
            logger = new DeltaLogger();
        }

        [Test]
        public async Task GetAllProjects()
        {
            var projects = new ProjectsController(context, logger);

            var response = await projects.GetAll() as ObjectResult;
            var value = response.Value as List<ProjectModel>;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(3, value.Count);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetProjectById(int id)
        {
            var project = new ProjectsController(context, logger);

            var response = await project.Get(id) as ObjectResult;
            var value = response.Value as ProjectModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
        }

        [TestCase(55)]
        public async Task GetByWrongId(int id)
        {
            var project = new ProjectsController(context, logger);

            var response = await project.Get(id) as ObjectResult;

            Assert.AreEqual(400, response.StatusCode);
        }

        [Test]
        public async Task InsertProject()
        {
            var project = new ProjectsController(context, logger);

            var response = await project.Post(new Project
            {
                Id = 4,
                Name = "New project",
                Description = "Project New Project",
                Customer = context.Customers.Find(1),
                Team = context.Teams.Find(1),
                BeginDate = new DateTime(2018, 1, 30),
                EndDate = null,
                Status = context.ProjectStatuses.Find(1),
                Pricing = context.PricingTypes.Find(1),
                Ammount = 10000
            }) as ObjectResult;
            var value = response.Value as ProjectModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual("New project", value.Name);
        }

        [TestCase(3)]
        public async Task ChangeProject(int id)
        {
            var project = new ProjectsController(context, logger);
            Project p = new Project
            {
                Id = 3,
                Name = "Changed name",
                Description = "Project New Project",
                Customer = context.Customers.Find(1),
                Team = context.Teams.Find(1),
                BeginDate = new DateTime(2018, 1, 30),
                EndDate = null,
                Status = context.ProjectStatuses.Find(1),
                Pricing = context.PricingTypes.Find(1),
                Ammount = 10000
            };

            var response = await project.Put(id, p) as ObjectResult;
            var value = response.Value as ProjectModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
            Assert.AreEqual("Changed name", value.Name);
        }

        [TestCase(4)]
        public async Task DeleteProject(int id)
        {
            var project = new ProjectsController(context, logger);

            var response = await project.Delete(id) as ObjectResult;

            var state = await project.GetAll() as ObjectResult;
            var value = state.Value as List<ProjectModel>;

            Assert.AreEqual(200, state.StatusCode);
            Assert.AreEqual(3, value.Count);
        }
    }
}