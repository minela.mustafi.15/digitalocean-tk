﻿using Library.DAL;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using TimeKeeper.API.Controllers;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Models;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.Test
{
    [TestFixture]
    public class TestCalendar
    {
        public TestContext context;
        public IRepository<Calendar> calendar;
        public IDeltaLog logger;

        public TestCalendar()
        {
            context = new TestContext();
            context.Seed();
            calendar = new Repository<Calendar>(context);
            logger = new DeltaLogger();
        }

        [Test]
        public async Task GetCalendar()
        {
            var calendar = new CalendarController(context, logger);

            var response = await calendar.Get() as ObjectResult;
            var value = response.Value as List<DayModel>;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(3, value.Count);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetDayById(int id)
        {
            var day = new CalendarController(context, logger);

            var response = await day.Get(id) as ObjectResult;
            var value = response.Value as DayModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
        }

        [TestCase(55)]
        public async Task GetByWrongId(int id)
        {
            var day = new CalendarController(context, logger);

            var response = await day.Get(id) as ObjectResult;

            Assert.AreEqual(400, response.StatusCode);
        }

        [Test]
        public async Task InsertDay()
        {
            var day = new CalendarController(context, logger);

            var response = await day.Post(new Day
            {
                Id = 4,
                Date = new DateTime(2019, 05, 05),
                Employee = context.Employees.Find(1),
                Type = context.DayTypes.Find(1)
            }) as ObjectResult;
            var value = response.Value as DayModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual("2019-05-05", value.Date.ToString("yyyy-MM-dd"));
        }

        [TestCase(3)]
        public async Task ChangeDay(int id)
        {
            var day = new CalendarController(context, logger);
            Day d = new Day
            {
                Id = 3,
                Date = new DateTime(2019, 10, 10),
                Employee = context.Employees.Find(1),
                Type = context.DayTypes.Find(1)
            };

            var response = await day.Put(id, d) as ObjectResult;
            var value = response.Value as DayModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
            Assert.AreEqual("2019-10-10", value.Date.ToString("yyyy-MM-dd"));
        }

        [TestCase(4)]
        public async Task DeleteDay(int id)
        {
            var day = new CalendarController(context, logger);

            var response = await day.Delete(id) as ObjectResult;

            var state = await day.Get() as ObjectResult;
            var value = state.Value as List<DayModel>;

            Assert.AreEqual(200, state.StatusCode);
            Assert.AreEqual(3, value.Count);
        }
    }
}