﻿using Library.DAL;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;
using TimeKeeper.API.Controllers;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain;
using TimeKeeper.DTO.Models;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.Test
{
    [TestFixture]
    public class TestRoles
    {
        public TestContext context;
        public IRepository<Role> roles;
        public IDeltaLog logger;

        public TestRoles()
        {
            context = new TestContext();
            context.Seed();
            roles = new Repository<Role>(context);
            logger = new DeltaLogger();
        }

        [Test]
        public async Task GetAllRoles()
        {
            var roles = new RolesController(context, logger);

            var response = await roles.GetAll() as ObjectResult;
            var value = response.Value as List<RoleModel>;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(2, value.Count);
        }

        [TestCase(1)]
        [TestCase(2)]
        public async Task GetRoleById(int id)
        {
            var role = new RolesController(context, logger);

            var response = await role.Get(id) as ObjectResult;
            var value = response.Value as RoleModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
        }

        [TestCase(55)]
        public async Task GetByWrongId(int id)
        {
            var role = new RolesController(context, logger);

            var response = await role.Get(id) as ObjectResult;

            Assert.AreEqual(400, response.StatusCode);
        }

        [Test]
        public async Task InsertRole()
        {
            var role = new RolesController(context, logger);

            var response = await role.Post(new Role
            {
                Id = 3,
                Name = "UX/UI",
                HourlyPrice = 17,
                MonthlyPrice = 2700
            }) as ObjectResult;
            var value = response.Value as RoleModel;

            var status = await role.GetAll() as ObjectResult;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual("UX/UI", value.Name);
        }

        [TestCase(2)]
        public async Task ChangeTeam(int id)
        {
            var role = new RolesController(context, logger);
            Role r = new Role
            {
                Id = 2,
                Name = "UX/UI",
                HourlyPrice = 17,
                MonthlyPrice = 2800
            };

            var response = await role.Put(id, r) as ObjectResult;
            var value = response.Value as RoleModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
            Assert.AreEqual(2800, value.MonthlyPrice);
        }

        [TestCase(3)]
        public async Task DeleteTeam(int id)
        {
            var role = new RolesController(context, logger);

            var response = await role.Delete(id) as ObjectResult;

            var state = await role.GetAll() as ObjectResult;
            var value = state.Value as List<RoleModel>;

            Assert.AreEqual(200, state.StatusCode);
            Assert.AreEqual(2, value.Count);
        }
    }
}