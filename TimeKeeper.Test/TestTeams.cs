﻿using Library.DAL;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;
using TimeKeeper.API.Controllers;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain;
using TimeKeeper.DTO.Models;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.Test
{
    [TestFixture]
    public class TestTeams

    {
        public TestContext context;
        public IRepository<Team> teams;
        public IDeltaLog logger;

        public TestTeams()
        {
            context = new TestContext();
            context.Seed();
            teams = new Repository<Team>(context);
            logger = new DeltaLogger();
        }

        [Test]
        public async Task GetAllTeams()
        {
            var teams = new TeamsController(context, logger);

            var response = await teams.GetAll() as ObjectResult;
            var value = response.Value as List<TeamModel>;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(3, value.Count);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetTeamById(int id)
        {
            var team = new TeamsController(context, logger);

            var response = await team.GetAll() as ObjectResult;
            var value = response.Value as TeamModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
        }

        [TestCase(55)]
        public async Task GetByWrongId(int id)
        {
            var team = new TeamsController(context, logger);

            var response = await team.Get(id) as ObjectResult;

            Assert.AreEqual(400, response.StatusCode);
        }

        [Test]
        public async Task InsertTeam()
        {
            var team = new TeamsController(context, logger);

            var response = await team.Post(new Team { Id = 4, Name = "Yellow" }) as ObjectResult;
            var value = response.Value as TeamModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual("Yellow", value.Name);
        }

        [TestCase(3)]
        public async Task ChangeTeam(int id)
        {
            var team = new TeamsController(context, logger);
            Team t = new Team { Id = 3, Name = "Purple", Status = true };

            var response = await team.Put(id, t) as ObjectResult;
            var value = response.Value as TeamModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
            Assert.AreEqual("Purple", value.Name);
        }

        [TestCase(4)]
        public async Task DeleteTeam(int id)
        {
            var team = new TeamsController(context, logger);

            var response = await team.Delete(id) as ObjectResult;

            var state = await team.GetAll() as ObjectResult;

            var value = state.Value as List<TeamModel>;

            Assert.AreEqual(200, state.StatusCode);
            Assert.AreEqual(3, value.Count);
        }
    }
}