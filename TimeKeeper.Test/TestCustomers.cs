﻿using Library.DAL;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;
using TimeKeeper.API.Controllers;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Models;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.Test
{
    [TestFixture]
    public class TestCustomers
    {
        public TestContext context;
        public IRepository<Customer> customers;
        public IDeltaLog logger;

        public TestCustomers()
        {
            context = new TestContext();
            context.Seed();
            customers = new Repository<Customer>(context);
            logger = new DeltaLogger();
        }

        [Test]
        public async Task GetAllCustomers()
        {
            var customers = new CustomersController(context, logger);

            var response = await customers.GetAll() as ObjectResult;
            var value = response.Value as List<CustomerModel>;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(3, value.Count);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetCustomerById(int id)
        {
            var customer = new CustomersController(context, logger);

            var response = await customer.Get(id) as ObjectResult;
            var value = response.Value as CustomerModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
        }

        [TestCase(55)]
        public async Task GetByWrongId(int id)
        {
            var customer = new CustomersController(context, logger);

            var response = await customer.Get(id) as ObjectResult;

            Assert.AreEqual(400, response.StatusCode);
        }

        [Test]
        public async Task InsertCustomer()
        {
            var customer = new CustomersController(context, logger);

            var response = await customer.Post(new Customer
            {
                Id = 4,
                Name = "New client",
                Image = "image_nc",
                Contact = "NC contact",
                Email = "ncemail@email.nc",
                Phone = "033225883",
                Address = { Road = "Ulica nc", City = "Sa", Country = "BiH", ZipCode = "71000" },
                Status = context.CustomerStatuses.Find(1)
            }) as ObjectResult;
            var value = response.Value as CustomerModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual("New client", value.Name);
        }

        [TestCase(3)]
        public async Task ChangeCustomer(int id)
        {
            var customer = new CustomersController(context, logger);
            Customer c = new Customer
            {
                Id = 3,
                Name = "New name",
                Image = "image_nc",
                Contact = "NC contact",
                Email = "ncemail@email.nc",
                Phone = "033225883",
                Address = { Road = "Ulica nc", City = "Sa", Country = "BiH", ZipCode = "71000" },
                Status = context.CustomerStatuses.Find(1)
            };

            var response = await customer.Put(id, c) as ObjectResult;
            var value = response.Value as CustomerModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
            Assert.AreEqual("New name", value.Name);
        }

        [TestCase(4)]
        public async Task DeleteCustomer(int id)
        {
            var customer = new CustomersController(context, logger);

            var response = await customer.Delete(id) as ObjectResult;

            ObjectResult state = await customer.Get(id) as ObjectResult;
            var value = state.Value as List<CustomerModel>;

            Assert.AreEqual(200, state.StatusCode);
            Assert.AreEqual(3, value.Count);
        }
    }
}