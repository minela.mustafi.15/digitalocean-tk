﻿using Microsoft.EntityFrameworkCore;
using TimeKeeper.DAL;

namespace TimeKeeper.Test
{
    public class TestContext : TimeKeeperContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            builder.UseNpgsql("User ID=postgres; Password=postgres; Server=localhost; Port=5432; Database=testera; Integrated Security=true; Pooling=true;");
        }
    }
}