﻿using System;
using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Test
{
    public static class TestDatabase
    {
        public static void Seed(this TestContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            context.CustomerStatuses.Add(new CustomerStatus { Id = 1, Type = 1, Value = "Prospect" });
            context.CustomerStatuses.Add(new CustomerStatus { Id = 2, Type = 2, Value = "Client" });

            context.DayTypes.Add(new DayType { Id = 1, Type = 1, Value = "Workday" });
            context.DayTypes.Add(new DayType { Id = 2, Type = 2, Value = "Holiday" });

            context.EmployeeStatuses.Add(new EmployeeStatus { Id = 1, Type = 1, Value = "Active" });
            context.EmployeeStatuses.Add(new EmployeeStatus { Id = 2, Type = 2, Value = "Trial" });

            context.PricingTypes.Add(new PricingType { Id = 1, Type = 1, Value = "Fixed bid" });
            context.PricingTypes.Add(new PricingType { Id = 2, Type = 2, Value = "Hourly" });

            context.ProjectStatuses.Add(new ProjectStatus { Id = 1, Type = 1, Value = "In progress" });
            context.ProjectStatuses.Add(new ProjectStatus { Id = 2, Type = 2, Value = "Finished" });

            context.PositionTypes.Add(new PositionType { Id = 1, Type = 1, Value = "Developer" });
            context.PositionTypes.Add(new PositionType { Id = 2, Type = 2, Value = "QA Engineer" });

            context.MemberStatuses.Add(new MemberStatus { Id = 1, Type = 1, Value = "Active" });
            context.MemberStatuses.Add(new MemberStatus { Id = 2, Type = 2, Value = "Waiting for the task" });

            context.SaveChanges();

            context.Teams.Add(new Team { Name = "Red", Description = "Red Team", Status = true });
            context.Teams.Add(new Team { Name = "Blue", Description = "Blue Team", Status = true });
            context.Teams.Add(new Team { Name = "Green", Description = "Green Team", Status = true });

            context.SaveChanges();

            context.Roles.Add(new Role { Id = 1, Name = "Software Dev", HourlyPrice = 18, MonthlyPrice = 3000 });
            context.Roles.Add(new Role { Id = 2, Name = "QA Eng", HourlyPrice = 15, MonthlyPrice = 2600 });

            context.SaveChanges();

            context.Customers.Add(new Customer
            {
                Name = "Small Customer",
                Image = "image_s",
                Contact = "S contact",
                Email = "semail@email.s",
                Phone = "033225883",
                Address = { Road = "Ulica s", City = "Sa", Country = "BiH", ZipCode = "71000" },
                Status = context.CustomerStatuses.Find(1)
            });
            context.Customers.Add(new Customer
            {
                Name = "Medium Customer",
                Contact = "M contact",
                Email = "memail@email.m",
                Address = { Road = "Ulica m", City = "Sa", Country = "BiH", ZipCode = "71000" },
                Status = context.CustomerStatuses.Find(1)
            });
            context.Customers.Add(new Customer
            {
                Name = "Big Customer",
                Contact = "B contact",
                Email = "bemail@email.b",
                Address = { Road = "Ulica b", City = "Sa", Country = "BiH", ZipCode = "71000" },
                Status = context.CustomerStatuses.Find(1)
            });

            context.SaveChanges();

            context.Projects.Add(new Project
            {
                Name = "TimeKeeper",
                Description = "Project Time Keeper",
                Customer = context.Customers.Find(1),
                Team = context.Teams.Find(1),
                BeginDate = new DateTime(2018, 1, 30),
                EndDate = null,
                Status = context.ProjectStatuses.Find(1),
                Pricing = context.PricingTypes.Find(1),
                Ammount = 10000
            });
            context.Projects.Add(new Project
            {
                Name = "RoomReservations",
                Description = "Project Room Reservations",
                Customer = context.Customers.Find(2),
                Team = context.Teams.Find(2),
                BeginDate = new DateTime(2017, 2, 14),
                EndDate = null,
                Status = context.ProjectStatuses.Find(1),
                Pricing = context.PricingTypes.Find(1),
                Ammount = 9500
            });
            context.Projects.Add(new Project
            {
                Name = "Library",
                Description = "Project Room Reservations",
                Customer = context.Customers.Find(3),
                Team = context.Teams.Find(3),
                BeginDate = new DateTime(2019, 3, 17),
                EndDate = null,
                Status = context.ProjectStatuses.Find(1),
                Pricing = context.PricingTypes.Find(1),
                Ammount = 11000
            });

            context.SaveChanges();

            context.Employees.Add(new Employee
            {
                FirstName = "Dorian",
                LastName = "Gray",
                Image = "DG",
                Email = "dorian@gray.com",
                Phone = "061100100",
                BirthDate = new DateTime(1980, 2, 28),
                BeginDate = new DateTime(2017, 8, 13),
                EndDate = null,
                Status = context.EmployeeStatuses.Find(1),
                Position = context.PositionTypes.Find(1)
            });
            context.Employees.Add(new Employee
            {
                FirstName = "Anna",
                LastName = "Karenjina",
                Image = "AK",
                Email = "anna@karenjina.com",
                Phone = "061200200",
                BirthDate = new DateTime(1990, 11, 11),
                BeginDate = new DateTime(2016, 12, 30),
                EndDate = null,
                Status = context.EmployeeStatuses.Find(1),
                Position = context.PositionTypes.Find(2)
            });
            context.Employees.Add(new Employee
            {
                FirstName = "Harry",
                LastName = "Potter",
                Image = "HP",
                Email = "harry@potter.com",
                Phone = "061300300",
                BirthDate = new DateTime(1995, 6, 25),
                BeginDate = new DateTime(2018, 7, 9),
                EndDate = null,
                Status = context.EmployeeStatuses.Find(1),
                Position = context.PositionTypes.Find(1)
            });

            context.SaveChanges();

            context.Members.Add(new Member
            {
                Employee = context.Employees.Find(1),
                Team = context.Teams.Find(1),
                Role = context.Roles.Find(1),
                HoursWeekly = 40,
                Status = context.MemberStatuses.Find(1)
            });
            context.Members.Add(new Member
            {
                Employee = context.Employees.Find(2),
                Team = context.Teams.Find(2),
                Role = context.Roles.Find(2),
                HoursWeekly = 40,
                Status = context.MemberStatuses.Find(1)
            });
            context.Members.Add(new Member
            {
                Employee = context.Employees.Find(3),
                Team = context.Teams.Find(3),
                Role = context.Roles.Find(1),
                HoursWeekly = 40,
                Status = context.MemberStatuses.Find(1)
            });

            context.SaveChanges();

            context.Calendar.Add(new Day
            {
                Date = new DateTime(2019, 10, 21),
                Employee = context.Employees.Find(1),
                Type = context.DayTypes.Find(1)
            });
            context.Calendar.Add(new Day
            {
                Date = new DateTime(2019, 5, 15),
                Employee = context.Employees.Find(2),
                Type = context.DayTypes.Find(1)
            });
            context.Calendar.Add(new Day
            {
                Date = new DateTime(2019, 4, 21),
                Employee = context.Employees.Find(3),
                Type = context.DayTypes.Find(1)
            });

            context.SaveChanges();

            context.Details.Add(new Detail
            {
                Day = context.Calendar.Find(1),
                Project = context.Projects.Find(1),
                Description = "Backend stuff",
                Hours = 30
            });
            context.Details.Add(new Detail
            {
                Day = context.Calendar.Find(2),
                Project = context.Projects.Find(2),
                Description = "Frontend stuff",
                Hours = 30
            });
            context.Details.Add(new Detail
            {
                Day = context.Calendar.Find(3),
                Project = context.Projects.Find(3),
                Description = "QA stuff",
                Hours = 30
            });

            context.SaveChanges();
        }
    }
}