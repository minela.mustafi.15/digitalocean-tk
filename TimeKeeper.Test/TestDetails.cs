﻿using Library.DAL;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;
using TimeKeeper.API.Controllers;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Models;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.Test
{
    [TestFixture]
    public class TestDetails
    {
        public TestContext context;
        public IRepository<Detail> details;
        public IDeltaLog logger;

        public TestDetails()
        {
            context = new TestContext();
            context.Seed();
            details = new Repository<Detail>(context);
            logger = new DeltaLogger();
        }

        [Test]
        public async Task GetAllDetails()
        {
            var details = new DetailsController(context, logger);

            var response = await details.GetAll() as ObjectResult;
            var value = response.Value as List<DetailModel>;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(3, value.Count);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetDetailById(int id)
        {
            var detail = new DetailsController(context, logger);

            var response = await detail.Get(id) as ObjectResult;
            var value = response.Value as DetailModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
        }

        [TestCase(55)]
        public async Task GetByWrongId(int id)
        {
            var detail = new DetailsController(context, logger);

            var response = await detail.Get(id) as ObjectResult;

            Assert.AreEqual(400, response.StatusCode);
        }

        [Test]
        public async Task InsertDetail()
        {
            var detail = new DetailsController(context, logger);

            var response = await detail.Post(new Detail
            {
                Id = 4,
                Day = context.Calendar.Find(1),
                Project = context.Projects.Find(1),
                Description = "New detail",
                Hours = 30
            }) as ObjectResult;
            var value = response.Value as DetailModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual("New detail", value.Description);
        }

        [TestCase(3)]
        public async Task ChangeDetail(int id)
        {
            var detail = new DetailsController(context, logger);
            Detail d = new Detail
            {
                Id = 3,
                Day = context.Calendar.Find(1),
                Project = context.Projects.Find(1),
                Description = "Changed detail",
                Hours = 30
            };

            var response = await detail.Put(id, d) as ObjectResult;
            var value = response.Value as DetailModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
            Assert.AreEqual("Changed detail", value.Description);
        }

        [TestCase(4)]
        public async Task DeleteDetail(int id)
        {
            var detail = new DetailsController(context, logger);

            var response = await detail.Delete(id) as ObjectResult;

            var state = await detail.GetAll() as ObjectResult;
            var value = state.Value as List<DetailModel>;

            Assert.AreEqual(200, state.StatusCode);
            Assert.AreEqual(3, value.Count);
        }
    }
}