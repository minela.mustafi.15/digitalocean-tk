﻿using Library.DAL;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TimeKeeper.API.Controllers;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain;
using TimeKeeper.DTO.Models;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.Test
{
    [TestFixture]
    public class TestEmployees
    {
        public TestContext context;
        public IRepository<Employee> employees;
        public IDeltaLog logger;

        public TestEmployees()
        {
            context = new TestContext();
            context.Seed();
            employees = new Repository<Employee>(context);
            logger = new DeltaLogger();
        }

        [Test]
        public async Task GetAllEmployees()
        {
            var employees = new EmployeesController(context, logger);

            var response = await employees.GetAll() as ObjectResult;
            var value = response.Value as List<EmployeeModel>;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(3, value.Count);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public async Task GetEmployeeById(int id)
        {
            var employee = new EmployeesController(context, logger);

            var response = await employee.Get(id) as ObjectResult;
            var value = response.Value as EmployeeModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
        }

        [TestCase(55)]
        public async Task GetByWrongId(int id)
        {
            var employee = new EmployeesController(context, logger);

            var response = await employee.Get(id) as ObjectResult;

            Assert.AreEqual(400, response.StatusCode);
        }

        [Test]
        public async Task InsertEmployee()
        {
            var employee = new EmployeesController(context, logger);

            var response = await employee.Post(new Employee
            {
                Id = 4,
                FirstName = "Tzukuru",
                LastName = "Tazaki",
                Image = "TT",
                Email = "tzukuru@tazaki.com",
                Phone = "061100100",
                BirthDate = new DateTime(1980, 2, 28),
                BeginDate = new DateTime(2017, 8, 13),
                EndDate = null,
                Status = context.EmployeeStatuses.Find(1),
                Position = context.PositionTypes.Find(1)
            }) as ObjectResult;
            var value = response.Value as EmployeeModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual("Tzukuru", value.FirstName);
            Assert.AreEqual("Tazaki", value.LastName);
        }

        [TestCase(3)]
        public async Task ChangeEmployee(int id)
        {
            var employee = new EmployeesController(context, logger);
            Employee e = new Employee
            {
                Id = 3,
                FirstName = "Toru",
                LastName = "Watanabe",
                Image = "TW",
                Email = "toru@watanabe.com",
                Phone = "061100100",
                BirthDate = new DateTime(1980, 2, 28),
                BeginDate = new DateTime(2017, 8, 13),
                EndDate = null,
                Status = context.EmployeeStatuses.Find(1),
                Position = context.PositionTypes.Find(1)
            };

            var response = await employee.Put(id, e) as ObjectResult;
            var value = response.Value as EmployeeModel;

            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(id, value.Id);
            Assert.AreEqual("Toru", value.FirstName);
            Assert.AreEqual("Watanabe", value.LastName);
        }

        [TestCase(4)]
        public async Task DeleteEmployee(int id)
        {
            var employee = new EmployeesController(context, logger);

            var response = await employee.Delete(id) as ObjectResult;

            var state = await employee.GetAll() as ObjectResult;
            var value = state.Value as List<EmployeeModel>;

            Assert.AreEqual(200, state.StatusCode);
            Assert.AreEqual(3, value.Count);
        }
    }
}