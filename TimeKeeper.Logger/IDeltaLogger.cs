﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeKeeper.Logger
{
    public interface IDeltaLogger
    {
        void Trace(string message);
        void Debug(string message);
        void Info(string message);
        void Warn(string message);
        void Error(string message);
        void Fatal(string message);
    }
}
