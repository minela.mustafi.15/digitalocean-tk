﻿using NLog;
using System;
using System.Collections.Generic;
using System.Text;

namespace TimeKeeper.Logger
{
    class NLoggerDelta : IDeltaLogger
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();
        public NLoggerDelta()
        {

        }
        public void Debug(string message)
        {
            logger.Debug(message);
        }

        public void Error(string message)
        {
            logger.Error(message);
        }

        public void Fatal(string message)
        {
            logger.Fatal(message);
        }

        public void Info(string message)
        {
            logger.Info(message);
        }

        public void Trace(string message)
        {
            logger.Trace(message);
        }

        public void Warn(string message)
        {
            logger.Warn(message);
        }
    }
}
