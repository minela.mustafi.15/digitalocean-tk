﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeKeeper.DeltaLog
{
    public interface IDeltaLog
    {
        void Trace(string message);
        void Information(string message);
        void Warning(string message);
        void Debug(string message);
        void Error(string message);
        void Fatal(string message);
    }
}
