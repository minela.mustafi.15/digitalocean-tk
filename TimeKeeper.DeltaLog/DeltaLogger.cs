﻿using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using TimeKeeper.Mail;

namespace TimeKeeper.DeltaLog
{
    public class DeltaLogger : IDeltaLog
    {
        protected static IEmailService Email;
        public DeltaLogger()
        {

        }

        public DeltaLogger(IEmailService email)
        {
            Email = email;
        }


        public static Logger logger = LogManager.GetCurrentClassLogger();
        public void Debug(string message)
        {
            logger.Debug(message);
        }

        public void Error(string message)
        {
            string mailTo = "sojourner005@gmail.com";
            string subject = $"Error occured";
            string body = $"Errror : {message}";
            Email.Send(mailTo, subject, body);
            logger.Error(message);

        }

        public void Information(string message)
        {
            logger.Info(message);
        }

        public void Trace(string message)
        {
            logger.Trace(message);
        }

        public void Warning(string message)
        {
            logger.Warn(message);
        }

        public void Fatal(string message)
        {
            logger.Fatal(message);
        }
    }
}
