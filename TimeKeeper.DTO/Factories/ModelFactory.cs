﻿using System.Collections.Generic;
using System.Linq;
using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Models;

namespace TimeKeeper.DTO.Factories
{
    public static class ModelFactory
    {
        public static TeamModel Create(this Team team)
        {
            return new TeamModel
            {
                Id = team.Id,
                Name = team.Name,
                Description = team.Description,
                Members = team.Members.Where(x => x.Status.Value != "leaver").Select(x => x.Master("team")).ToList(),
                Projects = team.Projects.Select(x => x.Master()).ToList(),
                Status = true
            };
        }

        public static MemberModel Create(this Member member)
        {
            return new MemberModel
            {
                Id = member.Id,
                Employee = member.Employee.Master(),
                Team = member.Team.Master(),
                Role = member.Role.Master(),
                HoursWeekly = member.HoursWeekly,
                Status = member.Status.Master()
            };
        }

        public static RoleModel Create(this Role role)
        {
            return new RoleModel
            {
                Id = role.Id,
                Name = role.Name,
                HourlyPrice = role.HourlyPrice,
                MonthlyPrice = role.MonthlyPrice,
                Members = role.Members.Select(y => y.Master("team")).ToList()
            };
        }

        public static ProjectModel Create(this Project project)
        {
            return new ProjectModel
            {
                Id = project.Id,
                Name = project.Name,
                Description = project.Description,
                Customer = project.Customer.Master(),
                Team = project.Team.Master(),
                BeginDate = project?.BeginDate,
                EndDate = project?.EndDate,
                Status = project.Status.Master(),
                Pricing = project.Pricing.Master(),
                Amount = project.Ammount,
                Details = project.Details.Select(x => x.Master()).ToList()
            };
        }

        public static EmployeeModel Create(this Employee employee)
        {
            return new EmployeeModel
            {
                Id = employee.Id,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                FullName = employee.FullName,
                Email = employee.Email,
                Phone = employee.Phone,
                Position = employee.Position.Master(),
                Image = employee.Image,
                Status = employee.Status.Master(),
                BeginDate = employee?.BeginDate,
                EndDate = employee?.EndDate,
                BirthDate = employee.BirthDate,
                Age = employee.Age,
                Calendar = employee.Calendar.Select(x => x.Master()).ToList(),
                Members = employee.Members.Select(x => x.Master("team")).ToList()
            };
        }

        public static DetailModel Create(this Detail detail)
        {
            return new DetailModel
            {
                Id = detail.Id,
                Description = detail.Description,
                Day = detail.Day.Master(),
                Project = detail.Project.Master(),
                Hours = detail.Hours
            };
        }

        public static CustomerModel Create(this Customer customer)
        {
            return new CustomerModel
            {
                Id = customer.Id,
                Name = customer.Name,
                Address = customer.Address,
                Contact = customer.Contact,
                Email = customer.Email,
                Image = customer.Image,
                Phone = customer.Phone,
                Status = customer.Status.Master(),
                Projects = customer.Projects.Select(x => x.Master()).ToList()
            };
        }

        public static DayModel Create(this Day day)
        {
            return new DayModel
            {
                Id = day.Id,
                Date = day.Date,
                Employee = day.Employee.Master(),
                DayType = day.Type.Master(),
                Details = day.Details.Select(x => x.Master()).ToList(),
            };
        }

        public static UserModel Create(this User user)
        {
            return new UserModel
            {
                Id = user.Id,
                Name = user.Name,
                Username = user.Username,
               // Password = user.Password,
                Role = user.Role
            };
        }

        public static EmployeeTrackerModel CreateTimeModel(this Employee employee)
        {
            return new EmployeeTrackerModel
            {
                Employee = employee.Master(),
                HourTypes = new Dictionary<string, decimal>(),
            };
        }

        /*---------------*------------------*/
        public static TeamModel CreateMobile(this Team team)
        {
            return new TeamModel
            {
                Id = team.Id,
                Name = team.Name,
                Description = team.Description,
                Members = team.Members.Where(x => x.Status.Value != "leaver").Select(x => x.Master("team")).ToList(),
                Status = true
            };
        }

        public static CustomerModel CreateMobile(this Customer customer)
        {
            return new CustomerModel
            {
                Id = customer.Id,
                Name = customer.Name,
                Address = customer.Address,
                Contact = customer.Contact,
                Email = customer.Email,
                Image = customer.Image,
                Phone = customer.Phone,
                Status = customer.Status.Master()
            };
        }

        public static ProjectModel CreateMobile(this Project project)
        {
            return new ProjectModel
            {
                Id = project.Id,
                Name = project.Name,
                Description = project.Description,
                Customer = project.Customer.Master(),
                Team = project.Team.Master(),
                BeginDate = project?.BeginDate,
                EndDate = project?.EndDate,
                Status = project.Status.Master(),
                Pricing = project.Pricing.Master(),
                Amount = project.Ammount,
                Details = project.Details.Select(x => x.Master()).ToList()
            };
        }
    }
}