﻿using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Models;

namespace TimeKeeper.DTO.Factories
{
    public static class MasterFactory
    {
        public static string Monogram(this string str)
        {
            string[] S = str.Split();
            string M = "";
            foreach (string x in S) M += x.Substring(0, 1);
            return M.ToUpper();
        }

        public static MasterModel Master(this MemberStatus memberStatus)
        {
            return new MasterModel
            {
                Id = memberStatus.Id,
                Name = memberStatus.Value
            };
        }

        public static MasterModel Master(this CustomerStatus customerStatus)
        {
            return new MasterModel
            {
                Id = customerStatus.Id,
                Name = customerStatus.Value
            };
        }

        public static MasterModel Master(this ProjectStatus projectStatus)
        {
            return new MasterModel
            {
                Id = projectStatus.Id,
                Name = projectStatus.Value
            };
        }

        public static MasterModel Master(this PricingType pricingType)
        {
            return new MasterModel
            {
                Id = pricingType.Id,
                Name = pricingType.Value
            };
        }

        public static MasterModel Master(this EmployeeStatus empStatus)
        {
            return new MasterModel
            {
                Id = empStatus.Id,
                Name = empStatus.Value
            };
        }

        public static MasterModel Master(this PositionType positionType)
        {
            return new MasterModel
            {
                Id = positionType.Id,
                Name = positionType.Value
            };
        }

        public static MasterModel Master(this DayType dayType)
        {
            return new MasterModel
            {
                Id = dayType.Id,
                Name = dayType.Value
            };
        }

        public static MasterModel Master(this Team t)
        {
            return new MasterModel
            {
                Id = t.Id,
                Name = t.Name
            };
        }

        public static MasterModel Master(this Member m, string what)
        {
            return new MasterModel
            {
                Id = (what == "team") ? m.Id : m.Team.Id,
                Name = ((what == "team") ? m.Employee.FullName : m.Team.Name) + ", " + m.Role.Name.Monogram()
            };
        }

        public static MasterModel Master(this Project p)
        {
            return new MasterModel
            {
                Id = p.Id,
                Name = p.Name
            };
        }

        public static MasterModel Master(this Day d)
        {
            return new MasterModel
            {
                Id = d.Id,
                Name = d.Type.Value
            };
        }

        public static MasterModel Master(this Role r)
        {
            return new MasterModel
            {
                Id = r.Id,
                Name = r.Name
            };
        }

        public static MasterModel Master(this Customer c)
        {
            return new MasterModel
            {
                Id = c.Id,
                Name = c.Name
            };
        }

        public static MasterModel Master(this Employee e)
        {
            return new MasterModel
            {
                Id = e.Id,
                Name = $"{e.FirstName} {e.LastName}"
            };
        }

        public static MasterModel Master(this Detail d)
        {
            return new MasterModel
            {
                Id = d.Id,
                Name = d.Description,
            };
        }

        public static MasterModel Master(this User u)
        {
            return new MasterModel
            {
                Id = u.Id,
                Name = u.Name
            };
        }
    }
}