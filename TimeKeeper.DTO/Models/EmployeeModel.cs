﻿using System;
using System.Collections.Generic;

namespace TimeKeeper.DTO.Models
{
    public class EmployeeModel
    {
        public EmployeeModel()
        {
            Calendar = new List<MasterModel>();
            Members = new List<MasterModel>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Image { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Age { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public MasterModel Status { get; set; }
        public MasterModel Position { get; set; }
        public IList<MasterModel> Members { get; set; }
        public IList<MasterModel> Calendar { get; set; }
    }
}