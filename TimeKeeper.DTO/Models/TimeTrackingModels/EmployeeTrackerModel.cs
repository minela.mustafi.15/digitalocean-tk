﻿using System.Collections.Generic;

namespace TimeKeeper.DTO.Models
{
    public class EmployeeTrackerModel
    {
        public EmployeeTrackerModel()
        {
            HourTypes = new Dictionary<string, decimal>();
        }

        public MasterModel Employee { get; set; }
        public Dictionary<string, decimal> HourTypes { get; set; }
    }
}