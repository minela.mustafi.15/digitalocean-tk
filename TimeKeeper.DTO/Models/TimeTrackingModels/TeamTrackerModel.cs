﻿using System.Collections.Generic;

namespace TimeKeeper.DTO.Models
{
    public class TeamTrackerModel
    {
        public TeamTrackerModel()
        {
            hourTypes = new Dictionary<string, int>();
        }

        public MasterModel Employee { get; set; }
        public Dictionary<string, int> hourTypes { get; set; }
    }
}