﻿using System;
using System.Collections.Generic;

namespace TimeKeeper.DTO.Models
{
    public class DayModel
    {
        public DayModel()
        {
            Details = new List<MasterModel>();
        }

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public MasterModel Employee { get; set; }
        public MasterModel DayType { get; set; }
        public decimal TotalHours { get; set; }

        public IList<MasterModel> Details { get; set; }
    }
}