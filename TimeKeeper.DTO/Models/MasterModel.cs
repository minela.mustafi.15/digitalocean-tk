﻿namespace TimeKeeper.DTO.Models
{
    public class MasterModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}