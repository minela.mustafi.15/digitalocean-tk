﻿namespace TimeKeeper.DTO.Models
{
    public class EmployeeKeyDictionary
    {
        public EmployeeKeyDictionary(MasterModel employee, decimal value)
        {
            Employee = employee;
            Value = value;
        }

        public MasterModel Employee { get; set; }
        public decimal Value { get; set; }
    }
}