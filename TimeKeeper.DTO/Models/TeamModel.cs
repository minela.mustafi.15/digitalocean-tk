﻿using System.Collections.Generic;

namespace TimeKeeper.DTO.Models
{
    public class TeamModel
    {
        public TeamModel()
        {
            Members = new List<MasterModel>();
            Projects = new List<MasterModel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<MasterModel> Members { get; set; }
        public List<MasterModel> Projects { get; set; }
        public bool Status { get; set; }
    }
}