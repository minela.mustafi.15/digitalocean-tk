﻿using System;
using System.Collections.Generic;

namespace TimeKeeper.DTO.Models
{
    public class ProjectModel
    {
        public ProjectModel()
        {
            Details = new List<MasterModel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public  MasterModel Customer { get; set; }
        public  MasterModel Team { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public  MasterModel Status { get; set; }
        public  MasterModel Pricing { get; set; }
        public decimal Amount { get; set; }
        public IList<MasterModel> Details { get; set; }
    }
}