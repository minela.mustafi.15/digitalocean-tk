﻿namespace TimeKeeper.DTO.Models
{
    public class TeamKeyDictionary
    {
        public TeamKeyDictionary(MasterModel team, int value)
        {
            Team = team;
            Value = value;
        }

        public MasterModel Team { get; set; }
        public int Value { get; set; }
    }
}