﻿using System.Collections.Generic;

namespace TimeKeeper.DTO.Models
{
    public class EmployeeProjectModel
    {
        public string EmployeeName { get; set; }

        public Dictionary<int, decimal> HoursPerYears { get; set; }

        public decimal TotalHoursPerProjectPerEmployee { get; set; }
    }
}