﻿using System.Collections.Generic;

namespace TimeKeeper.DTO.Models
{
    public class ProjectMonthlyModel
    {
        public ProjectMonthlyModel()
        {
            Projects = new List<MasterModel>();
            Employees = new List<EmployeeMonthlyProjectModel>();
        }

        public List<MasterModel> Projects { get; set; }
        public List<EmployeeMonthlyProjectModel> Employees { get; set; }
    }
}