﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeKeeper.DTO.Models.ReportModels
{
    public class MonthlyRawModel
    {
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public int ProjId { get; set; }
        public string ProjName { get; set; }
        public decimal Hours { get; set; }
    }
}
