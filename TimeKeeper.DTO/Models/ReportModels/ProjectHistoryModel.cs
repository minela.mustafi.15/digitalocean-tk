﻿using System.Collections.Generic;

namespace TimeKeeper.DTO.Models
{
    public class ProjectHistoryModel
    {
        public ProjectHistoryModel()
        {
            Employees = new List<EmployeeProjectModel>();
            TotalYearlyProjectHours = new Dictionary<int, decimal>();
        }

        public List<EmployeeProjectModel> Employees { get; set; }

        public Dictionary<int, decimal> TotalYearlyProjectHours { get; set; }
        public decimal TotalHoursPerProject { get; set; }
    }
}