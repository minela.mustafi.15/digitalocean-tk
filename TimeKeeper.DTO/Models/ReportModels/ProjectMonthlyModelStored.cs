﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeKeeper.DTO.Models
{
     public class ProjectMonthlyModelStored
    {
        public ProjectMonthlyModelStored()
        {
            Projects = new List<MasterModel>();
            Employees = new List<EmployeeProjectModelStored>();
        }
        public List<MasterModel> Projects { get; set; }
        public List<EmployeeProjectModelStored> Employees { get; set; }
    }
}
