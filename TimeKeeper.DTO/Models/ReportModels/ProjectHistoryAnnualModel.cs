﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeKeeper.DTO.Models.ReportModels
{
    public class ProjectHistoryAnnualModel
    {
        public ProjectHistoryAnnualModel()
        {
            Employees = new List<EmployeeProjectHistoryStored>();
            Years = new List<int>();
        }
        public List<int> Years { get; set; }
        public List<EmployeeProjectHistoryStored> Employees { get; set; }
    }
}
