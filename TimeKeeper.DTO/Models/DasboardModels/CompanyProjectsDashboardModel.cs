﻿namespace TimeKeeper.DTO.Models.DasboardModels
{
    public class CompanyProjectsDashboardModel
    {
        public MasterModel Project { get; set; }
        public decimal Revenue { get; set; }
    }
}