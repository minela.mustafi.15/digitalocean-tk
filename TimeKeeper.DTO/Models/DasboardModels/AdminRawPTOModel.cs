﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeKeeper.DTO.Models.DasboardModels
{
    public class AdminRawPTOModel
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public decimal PTO { get; set; }
    }
}
