﻿using System.Collections.Generic;

namespace TimeKeeper.DTO.Models.DasboardModels
{
    public class AdminDashboardModel
    {
        public AdminDashboardModel()
        {
            PaidTimeOff = new List<TeamKeyDictionary>();
            Overtime = new List<TeamKeyDictionary>();
            MissingEntries = new List<TeamKeyDictionary>();
        }
        public int EmployeesCount { get; set; }
        public int ProjectsCount { get; set; }
        public decimal BaseTotalHours { get; set; }
        public decimal TotalHours { get; set; }
        public decimal WorkingHours { get; set; }
        public List<TeamKeyDictionary> MissingEntries { get; set; }
        public List<TeamKeyDictionary> PaidTimeOff { get; set; }
        public List<TeamKeyDictionary> Overtime { get; set; }
    }
}