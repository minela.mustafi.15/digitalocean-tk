﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeKeeper.DTO.Models.DasboardModels
{
    public class AdminDashboardRolesModel
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
    }
}
