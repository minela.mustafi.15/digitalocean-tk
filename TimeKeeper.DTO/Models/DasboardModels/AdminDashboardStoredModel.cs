﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeKeeper.DTO.Models.DasboardModels
{
    public class AdminDashboardStoredModel
    {
        public AdminDashboardStoredModel(List<string> roles)
        {
            PaidTimeOff = new List<AdminRawPTOModel>();
            Roles = new List<AdminDashboardRolesModel>();
            Roles.AddRange(roles.Select(x => new AdminDashboardRolesModel
            {
                RoleName = x
            }));
        }
        public int EmployeesCount { get; set; }
        public int ProjectsCount { get; set; }
        public decimal TotalHours { get; set; }
        public decimal WorkingHours { get; set; }
        public List<AdminRawPTOModel> PaidTimeOff { get; set; }
        public List<AdminDashboardRolesModel> Roles { get; set; }
        public ProjectModel Projects { get; set; }
    }
}
