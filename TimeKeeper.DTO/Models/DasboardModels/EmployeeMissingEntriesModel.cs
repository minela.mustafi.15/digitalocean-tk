﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeKeeper.DTO.Models.DasboardModels
{
    public class EmployeeMissingEntriesModel
    {
        //public int TeamId { get; set; }
        public MasterModel Employee { get; set; }
        public decimal MissingEntries { get; set; }
    }
}
