﻿namespace TimeKeeper.DTO.Models
{
    public class PersonalDashboardModel
    {
        public MasterModel Employee;
        public decimal TotalHours;
        public decimal WorkingHours;
        public decimal BradfordFactor;
    }
}