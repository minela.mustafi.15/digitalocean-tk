﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeKeeper.DTO.Models.DasboardModels
{
    public class TeamRawCountModel
    {
        public int ProjectId { get; set; }
    }
    public class TeamRawModel
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public decimal Value { get; set; }
    }
    public class TeamRawNonWorkingHoursModel
    {
        public int MemberId { get; set; }
        public decimal Value { get; set; }
    }
}
