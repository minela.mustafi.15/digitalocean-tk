﻿using System.Collections.Generic;

namespace TimeKeeper.DTO.Models.DasboardModels
{
    public class TeamDashboardModel
    {
        public TeamDashboardModel()
        {
            PaidTimeOff = new List<EmployeeKeyDictionary>();
            Overtime = new List<EmployeeKeyDictionary>();
            MissingEntries = new List<EmployeeKeyDictionary>();
        }
        public int EmployeesCount { get; set; }
        public int ProjectsCount { get; set; }
        public decimal BaseTotalHours { get; set; }
        public decimal TotalHours { get; set; }
        public decimal WorkingHours { get; set; }
        public List<EmployeeKeyDictionary> MissingEntries { get; set; }
        public List<EmployeeKeyDictionary> PaidTimeOff { get; set; }
        public List<EmployeeKeyDictionary> Overtime { get; set; }
    }
}