﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeKeeper.DTO.Models.DasboardModels
{
    public class AdminRawCountModel
    {
        public int EmployeeId { get; set; }
        public int ProjectId { get; set; }
        public decimal WorkingHours { get; set; }
    }
}
