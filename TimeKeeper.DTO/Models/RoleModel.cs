﻿using System.Collections.Generic;

namespace TimeKeeper.DTO.Models
{
    public class RoleModel
    {
        public RoleModel()
        {
            Members = new List<MasterModel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public decimal HourlyPrice { get; set; }
        public decimal MonthlyPrice { get; set; }

        public List<MasterModel> Members { get; set; }
    }
}