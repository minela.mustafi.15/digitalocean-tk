﻿namespace TimeKeeper.DTO.Models
{
    public class DetailModel
    {
        public int Id { get; set; }
        public MasterModel Day { get; set; }
        public MasterModel Project { get; set; }
        public string Description { get; set; }
        public decimal Hours { get; set; }
    }
}