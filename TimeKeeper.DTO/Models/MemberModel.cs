﻿namespace TimeKeeper.DTO.Models
{
    public class MemberModel
    {
        public int Id { get; set; }
        public MasterModel Employee { get; set; }
        public MasterModel Team { get; set; }
        public MasterModel Role { get; set; }
        public decimal HoursWeekly { get; set; }
        public MasterModel Status { get; set; }
    }
}