# containers

## app.js
Get services, get teams, state, lifecycles

# parent components

/* HTML elements in parantheses, not components */

- header (nav) (wrapper, logo, links, burger, login btn)
- hero (title, description, background-image)
- about (title, icon, paragraph)

- services (grid material ui)
* service-card (icon, title, description)

- team (carousel (slick))
* team-card (picture, name, title, social media buttons)

- contact (name, email, phone, message)
- footer (conditional rendering for sitemap)
- login modal (username, password, button, g-button)

# reusable components

- Button, Rounded-button, social media button (material ui)
- Input field (all types that we use) (validation, formic forms, yup)
- Textarea (validation, formic forms, yup)

