import {
  LOAD_CALENDAR_MONTH,
  LOAD_CALENDAR_MONTH_SUCCESS,
  LOAD_CALENDAR_MONTH_FAIL,
  TASK_EDITED_SUCCESS,
  TASK_EDITED_FAIL,
  TASK_ADD_FAIL,
  TASK_ADD_SUCCESS,
  ADD_DAY_SUCCESS,
  ADD_DAY_FAIL,
  ADD_DAY_WITH_TASK_SUCCESS,
  ADD_DAY_WITH_TASK_FAIL,
  TASK_DELETE_SUCCESS,
  TASK_DELETE_FAIL,
  RELOAD_CALENDAR,
  DAY_EDITED_FAIL,
  DAY_EDITED_SUCCESS
} from "./types";
// import {
//   getCalendar,
//   calendarUrl,
//   tasksUrl,
//   apiPutRequest,
//   apiPostRequest,
//   apiDeleteRequest
// } from "./../../utils/api";
import axios from "axios";
import Config from "../../Config";
const employeeCalendarUrl = Config.apiUrl + "mobile/employee/";
const calendarUrl = Config.apiUrl + "calendar/";
const tasksUrl = Config.apiUrl + "details/";

export const loadCalendar = (id, year, month) => {
  return async dispatch => {
    dispatch({ type: LOAD_CALENDAR_MONTH });
    axios
      .get(employeeCalendarUrl + `${id}/${year}/${month}`, Config.authHeader)
      .then(res => {
        console.log("employeeCalendar", res.data);
        dispatch({ type: LOAD_CALENDAR_MONTH_SUCCESS, data: res.data });
      })
      .catch(err => {
        dispatch({ type: LOAD_CALENDAR_MONTH_FAIL });
        console.log("err", err);
      });
  };
};

export const addDay = body => {
  return async dispatch => {
    dispatch({ type: LOAD_CALENDAR_MONTH });
    axios
      .post(calendarUrl, body, Config.authHeader)
      .then(res => {
        dispatch({ type: ADD_DAY_SUCCESS, data: res.data });
      })
      .catch(err => {
        dispatch({ type: ADD_DAY_FAIL });
        console.log("err", err);
      });
  };
};

export const editTask = (id, body) => {
  return dispatch => {
    axios
      .put(tasksUrl + "/" + id, body, Config.authHeader)
      .then(res => {
        dispatch({ type: TASK_EDITED_SUCCESS });
      })
      .catch(err => {
        dispatch({ type: TASK_EDITED_FAIL, error: err });
      });
  };
};

export const editDay = (id, body) => {
  console.log("edit day: ", body);
  return dispatch => {
    axios
      .put(calendarUrl + "/" + id, body, Config.authHeader)
      .then(res => {
        console.log("EDITED DAY RES", res);
        dispatch({ type: DAY_EDITED_SUCCESS });
      })
      .catch(err => {
        dispatch({ type: DAY_EDITED_FAIL, error: err });
      });
  };
};

export const addTask = body => {
  return dispatch => {
    console.log("addTask: ", body);
    // body.project.name = "project";
    axios
      .post(tasksUrl, body, Config.authHeader)
      .then(res => {
        dispatch({ type: TASK_ADD_SUCCESS });
      })
      .catch(err => {
        console.log("TASK_ADD_FAIL");
        dispatch({ type: TASK_ADD_FAIL, error: err });
      });
  };
};

export const deleteTask = id => {
  return dispatch => {
    console.log("delete by id: ", id);
    axios
      .delete(tasksUrl + "/" + id, Config.authHeader)
      .then(res => {
        console.log(res);
        dispatch({ type: TASK_DELETE_SUCCESS });
      })
      .catch(err => {
        console.log(err);
        dispatch({ type: TASK_DELETE_FAIL });
      });
  };
};

export const addDayWithTask = (day, task) => {
  console.log("Adding  day with task. Day", day);
  day.employee.firstName = "gggg";
  day.employee.lastName = "gggg";
  day.type = day.dayType;
  return async dispatch => {
    axios
      .post(calendarUrl, day, Config.authHeader)
      .then(res => {
        console.log("Added day: ", res);
        console.log("Task: ", task);
        dispatch({ type: ADD_DAY_SUCCESS, data: res.data });
        let data = {
          day: {
            id: res.data.data.id
          },
          project: {
            id: task.project.id
          },
          description: task.description,
          hours: task.hours
        };
        axios
          .post(tasksUrl, data)
          .then(res => {
            console.log("Added task after adding day", res);
            dispatch({ type: ADD_DAY_WITH_TASK_SUCCESS, data: res.data });
          })
          .catch(err => {
            dispatch({ type: ADD_DAY_WITH_TASK_FAIL });
            console.log("err", err);
          });
      })
      .catch(err => {
        dispatch({ type: ADD_DAY_WITH_TASK_FAIL });
        console.log("err", err);
      });
  };
};

export const rldCal = value => {
  return {
    type: RELOAD_CALENDAR,
    value
  };
};
