import Axios from "axios";
import Config from "../../Config";
import {
  FETCH_MONTHLY_REPORT,
  FETCH_MONTHLY_REPORT_SUCCESS,
  FETCH_MONTHLY_REPORT_FAILURE
} from "./types";

export const getMonthlyReport = (selectedYear, selectedMonth) => {
  return dispatch => {
    dispatch({ type: FETCH_MONTHLY_REPORT });
    Axios.get(
      `${Config.apiUrl}reports/monthly-overview-stored/${selectedYear}/${selectedMonth}`,
      Config.authHeader
    )
      .then(res => {
        // Map columns that will be used in table
        // NORMAL METHOD
        // let data = res.data.employeeProjectHours.map(x => {
        //   return {
        //     employee: x.employee.name,
        //     "total hours": x.totalHours,
        //     ...x.hoursByProject,
        //     "paid time off": x.paidTimeOff
        //   };
        // });
        // // ADD TOTALS TO THE TABLE
        // data.push({
        //   employee: "TOTAL",
        //   "total hours": res.data.totalHours,
        //   ...res.data.hoursByProject,
        //   "paid time off": res.data.employeeProjectHours
        //     .map(x => x.paidTimeOff)
        //     .reduce((accumulator, currentValue) => accumulator + currentValue, 0)
        // });

        // FOR STORED PROCEDURE
        let data = res.data.result.employees.map(x => {
          return {
            "-2": x.employee.name,
            "-1": x.totalHours,
            ...x.hours
            // "paid time off": x.paidTimeOff
          };
        });
        console.log("monthly report data: ", data);
        let head = ["Employee", "Total Hours", ...res.data.result.projects.map(x => x.name)];
        // data.push({
        //   "-2": "TOTAL",
        //   "-1": res.data.result.employees.map(x=>x.totalHours).reduce((a, b) => a + b, 0),
        //   ...res.data.result.employees.hours
        //   // "paid time off": res.data.employeeProjectHours
        //   //   .map(x => x.paidTimeOff)
        //   //   .reduce((accumulator, currentValue) => accumulator + currentValue, 0)
        // });
        console.log("head: ", head);
        dispatch({
          type: FETCH_MONTHLY_REPORT_SUCCESS,
          payload: {
            data: data,
            head: head,
            totalDays: res.data.totalWorkingDays,
            totalHours: res.data.totalPossibleWorkingHours
          }
        });
      })
      .catch(err => {
        console.log(err);
        dispatch({ type: FETCH_MONTHLY_REPORT_FAILURE, payload: err });
      });
  };
};

export const startLoading = () => {
  return dispatch => {
    dispatch({ type: FETCH_MONTHLY_REPORT });
  };
};
