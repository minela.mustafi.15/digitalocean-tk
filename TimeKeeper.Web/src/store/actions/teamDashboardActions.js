import Axios from "axios";
import Config from "../../Config";
import {
  FETCH_TEAM_DASHBOARD,
  FETCH_TEAM_DASHBOARD_SUCCESS,
  FETCH_TEAM_DASHBOARD_FAILURE
} from "./types";

export const getTeamDashboard = (selectedTeam, selectedYear, selectedMonth) => {
  return dispatch => {
    dispatch({ type: FETCH_TEAM_DASHBOARD });
    // console.log("team action", selectedTeam.id, selectedMonth, selectedYear);
    if (selectedTeam) {
      Axios.get(
        Config.apiUrl +
          `dashboards/team-dashboard-stored/${selectedTeam.id}/${selectedYear}/${selectedMonth}`,
        Config.authHeader
      )
        .then(res => {
          dispatch({ type: FETCH_TEAM_DASHBOARD_SUCCESS, payload: res.data });
        })
        .catch(err => {
          console.log(err);
          dispatch({ type: FETCH_TEAM_DASHBOARD_FAILURE, payload: err });
        });
    } else {
      dispatch({ type: FETCH_TEAM_DASHBOARD_FAILURE, payload: "team is null" });
    }
  };
};
