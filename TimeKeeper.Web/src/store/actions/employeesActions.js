import Axios from "axios";
import Config from "../../Config";
import { FETCH_EMPLOYEES, FETCH_EMPLOYEES_SUCCESS, FETCH_EMPLOYEES_FAILURE } from "./types";

const sortById = (a, b) => {
  return a.id - b.id;
};
export const getEmployees = () => {
  return dispatch => {
    // Config.authHeader.headers.Authorization = JSON.parse(localStorage.getItem("token"));
    dispatch({ type: FETCH_EMPLOYEES });
    Axios.get(Config.apiUrl + "employees", Config.authHeader)
      .then(res => {
        // Map columns that will be used in table
        let data = res.data.map(row => {
          return {
            id: row.id,
            firstName: row.firstName,
            lastName: row.lastName,
            age: row.age ? row.age : row.birthDate,
            email: row.email,
            phone: row.phone
          };
        });
        dispatch({ type: FETCH_EMPLOYEES_SUCCESS, payload: data.sort(sortById) });
      })
      .catch(err => {
        console.log(err);
        dispatch({ type: FETCH_EMPLOYEES_FAILURE, payload: err });
      });
  };
};
