import {
  AUTH_START,
  AUTH_SUCCESS,
  AUTH_FAIL,
  AUTH_LOGOUT,
  AUTH_PERSIST,
  AUTH_FINISH
} from "./types";
import Config from "../../Config";
import axios from "axios";

export const logout = () => {
  return {
    type: AUTH_LOGOUT
  };
};

const authStart = () => {
  return {
    type: AUTH_START
  };
};

const authSuccess = payload => {
  return {
    type: AUTH_SUCCESS,
    payload
  };
};

const authFail = error => {
  return {
    type: AUTH_FAIL,
    error
  };
};

export const authPersist = () => {
  return {
    type: AUTH_PERSIST
  };
};

export const authLogout = () => {
  return {
    type: AUTH_LOGOUT
  };
};
export const authFinish = () => {
  return {
    type: AUTH_FINISH
  };
};
export const auth = credentials => {
  return dispatch => {
    dispatch(authStart());
    axios
      .post(Config.loginUrl, credentials)
      .then(res => {
        dispatch(authSuccess(res.data));
      })
      .catch(err => dispatch(authFail(err)));
  };
};
