import { SIGNIN, SIGNIN_SUCCESS, SIGNIN_FAILURE } from "./types";

export const preserveUser = userProfile => {
  return dispatch => {
    dispatch({ type: SIGNIN_SUCCESS, payload: userProfile });
  };
};
