import Axios from "axios";
import Config from "../../Config";
import {
  FETCH_COMPANY_DASHBOARD,
  FETCH_COMPANY_DASHBOARD_SUCCESS,
  FETCH_COMPANY_DASHBOARD_FAILURE
} from "./types";

export const getCompanyDashboard = (selectedYear, selectedMonth) => {
  return dispatch => {
    dispatch({ type: FETCH_COMPANY_DASHBOARD });
    Axios.get(
      Config.apiUrl + `dashboards/company/${selectedYear}/${selectedMonth}`,
      Config.authHeader
    )
      .then(res => {
        dispatch({ type: FETCH_COMPANY_DASHBOARD_SUCCESS, payload: res.data });
      })
      .catch(err => {
        console.log(err);
        dispatch({ type: FETCH_COMPANY_DASHBOARD_FAILURE, payload: err });
      });
  };
};

export const startLoading = () => {
  return dispatch => {
    dispatch({ type: FETCH_COMPANY_DASHBOARD });
  };
};
