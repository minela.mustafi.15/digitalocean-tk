import employeesReducer from "./employeesReducer";
import signinReducer from "./signinReducer";
import authReducer from "./authReducer";
import annualReportReducer from "./annualReportReducer";
import monthlyReportReducer from "./monthlyReportReducer";
import companyDashboardReducer from "./companyDashboardReducer";
import teamDashboardReducer from "./teamDashboardReducer";
import calendarReducer from "./calendarReducer";

import { combineReducers } from "redux";

export default combineReducers({
  // currentUser: signinReducer,
  employees: employeesReducer,
  authData: authReducer,
  annualReport: annualReportReducer,
  monthlyReport: monthlyReportReducer,
  companyDashboard: companyDashboardReducer,
  teamDashboard: teamDashboardReducer,
  calendarMonth: calendarReducer
});
