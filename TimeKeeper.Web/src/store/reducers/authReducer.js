import {
  AUTH_START,
  AUTH_SUCCESS,
  AUTH_FAIL,
  AUTH_LOGOUT,
  AUTH_PERSIST,
  AUTH_FINISH
} from "../actions/types";
import Config from "../../Config";

const initialUserState = {
  user: null,
  token: null,
  loading: false,
  error: false,
  hasLoggedIn: false
};

export default (state = initialUserState, action) => {
  // console.log(action.type);
  switch (action.type) {
    case AUTH_START:
      return {
        ...state,
        loading: true
      };
    case AUTH_SUCCESS:
      Config.token = "Bearer " + action.payload.token;
      Config.authHeader.headers.Authorization = Config.token;
      console.log("authsuccess config", Config);
      localStorage.setItem("token", JSON.stringify(Config.token));
      localStorage.setItem("user", JSON.stringify(action.payload.user));
      return {
        ...state,
        user: action.payload.user,
        token: action.payload.token,
        loading: false,
        hasLoggedIn: true
      };
    case AUTH_FAIL:
      return {
        ...state,
        error: action.error,
        loading: false
      };
    case AUTH_PERSIST:
      const storageUser = JSON.parse(localStorage.getItem("user"));
      const storageToken = JSON.parse(localStorage.getItem("token"));
      Config.token = storageToken;
      Config.authHeader.headers.Authorization = storageToken;
      return {
        ...state,
        user: storageUser,
        token: storageToken
      };
    case AUTH_LOGOUT: {
      ["user", "token"].forEach(x => localStorage.removeItem(x));
      return {
        ...state,
        user: null,
        token: null
      };
    }
    case AUTH_FINISH: {
      return {
        ...state,
        hasLoggedIn: false
      };
    }
    default:
      return state;
  }
};
