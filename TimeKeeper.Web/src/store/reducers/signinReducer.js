import { SIGNIN, SIGNIN_SUCCESS, SIGNIN_FAILURE } from "../actions/types";

export default (state = null, action) => {
  switch (action.type) {
    case SIGNIN:
      return state;
    case SIGNIN_SUCCESS:
      return action.payload;
    case SIGNIN_FAILURE:
      return state;
    default:
      return state;
  }
};
