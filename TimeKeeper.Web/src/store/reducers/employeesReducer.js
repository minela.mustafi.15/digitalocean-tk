import {
  FETCH_EMPLOYEES,
  FETCH_EMPLOYEES_SUCCESS,
  FETCH_EMPLOYEES_FAILURE
} from "../actions/types";

const initialState = {
  table: {
    head: [],
    rows: [],
    actions: true
  },
  error: null,
  isLoading: true
};
export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_EMPLOYEES:
      return state;
    case FETCH_EMPLOYEES_SUCCESS:
      return Object.assign({}, state, {
        table: {
          head: Object.keys(action.payload[0]),
          rows: action.payload,
          actions: true
        },
        isLoading: false
      });
    case FETCH_EMPLOYEES_FAILURE:
      return Object.assign({}, state, {
        error: action.payload
      });
    default:
      return state;
  }
};
