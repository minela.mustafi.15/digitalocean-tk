/* /src/components/auth/logoutCallback.jsx */

import React from "react";
import { AuthConsumer } from "./AuthProvider";
import BlockElementSpinner from "../components/BlockElementSpinner/BlockElementSpinner";

export const LogoutCallback = () => (
  <AuthConsumer>
    {({ signoutRedirectCallback }) => {
      signoutRedirectCallback();
      return <BlockElementSpinner />;
    }}
  </AuthConsumer>
);
