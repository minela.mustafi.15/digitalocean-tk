/* /src/components/auth/callback.jsx */
import React, { Fragment, Component } from "react";
import { AuthConsumer } from "./AuthProvider";
import { connect } from "react-redux";
import { preserveUser } from "../store/actions/signinActions";

import BlockElementSpinner from "../components/BlockElementSpinner/BlockElementSpinner";

class Callback extends Component {
  render() {
    return (
      <AuthConsumer>
        {({ signinRedirectCallback, getUser }) => {
          signinRedirectCallback()
            .then(res => {
              //   getUser()
              //     .then(user => {
              //       // REDUX user.profile
              //       console.log("start redux process");
              //       this.props.preserveUser(user.profile);
              //     })
              //     .catch(err => console.log(err));
              console.log("get current user from local storage");
              // this.props.preserveUser(JSON.parse(localStorage.getItem("currentUser")));
            })
            .catch(err => console.log(err));
          return <BlockElementSpinner />;
        }}
      </AuthConsumer>
    );
  }
}
const mapStateToProps = state => {
  return {
    currentUser: state.currentUser
  };
};
export default connect(mapStateToProps, { preserveUser })(Callback);
