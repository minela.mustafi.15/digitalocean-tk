/* /src/routes/privateRoute.jsx */
import React from "react";
import { Route } from "react-router-dom";
import { AuthConsumer } from "./AuthProvider";
import BlockElementSpinner from "../components/BlockElementSpinner/BlockElementSpinner";

export const LeadRoute = ({ component, ...rest }) => {
  const renderFn = Component => props => (
    <AuthConsumer>
      {({ isAuthenticated, signinRedirect }) => {
        let currentUser = JSON.parse(localStorage.getItem("currentUser"));
        if (
          !!Component &&
          isAuthenticated() &&
          (currentUser.role === "admin" || currentUser.role === "lead")
        ) {
          return <Component {...props} />;
        } else {
          signinRedirect();
          return <BlockElementSpinner />;
        }
      }}
    </AuthConsumer>
  );

  return <Route {...rest} render={renderFn(component)} />;
};
