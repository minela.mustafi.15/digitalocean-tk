/* /src/components/auth/logout.jsx */

import React from "react";
import { AuthConsumer } from "./AuthProvider";
import BlockElementSpinner from "../components/BlockElementSpinner/BlockElementSpinner";

export const Logout = () => (
  <AuthConsumer>
    {({ logout }) => {
      logout();
      return <BlockElementSpinner />;
    }}
  </AuthConsumer>
);
