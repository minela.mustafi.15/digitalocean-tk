import React from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  Button,
  MenuItem,
  TextField,
  Grid
} from "@material-ui/core";
import "./Table.css";

function TableTK(props) {
  const {
    title,
    head,
    rows,
    actions,
    handleClickOpen,
    handleClickDelete,
    selectedYear,
    handleSelectedYear,
    selectedMonth,
    handleSelectedMonth,
    optionSubmit,
    hasOptions,
    sumTotals
  } = props;
  // console.log("table.js", props);

  const YearDropdown = () => (
    <TextField
      variant="outlined"
      id="selected-year"
      select
      label="Selected Year"
      value={selectedYear}
      onChange={handleSelectedYear}
      margin="normal"
      fullWidth
    >
      {[2019, 2018, 2017].map(x => {
        return (
          <MenuItem value={x} key={x}>
            {x}
          </MenuItem>
        );
      })}
    </TextField>
  );

  const MonthDropdown = () => (
    <TextField
      variant="outlined"
      id="selected-month"
      select
      label="Selected Month"
      value={selectedMonth}
      onChange={handleSelectedMonth}
      margin="normal"
      fullWidth
    >
      {[
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
      ].map((x, i) => {
        return (
          <MenuItem value={i + 1} key={x}>
            {x}
          </MenuItem>
        );
      })}
    </TextField>
  );

  return (
    <Paper elevation={3}>
      <Table className="table-tk">
        <TableHead>
          {hasOptions && (
            <TableRow>
              <TableCell colSpan={head.length} className="pr-2 pl-2">
                <Grid container spacing={4} alignItems="center" justify="space-between">
                  <Grid item xs={8}>
                    <h3>{title}</h3>
                  </Grid>
                  {selectedYear && (
                    <Grid item xs={2}>
                      <YearDropdown />
                    </Grid>
                  )}
                  {selectedMonth && (
                    <Grid item xs={2}>
                      <MonthDropdown />
                    </Grid>
                  )}
                </Grid>
              </TableCell>
            </TableRow>
          )}
          <TableRow className="table-tk-row-dark">
            {head.map((cell, i) => (
              <TableCell key={i}>{cell}</TableCell>
            ))}
            {actions && <TableCell>Actions</TableCell>}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row, i) => (
            <TableRow
              key={i}
              className={sumTotals && i === rows.length - 1 ? "table-tk-row-dark" : ""}
            >
              {Object.keys(row)
                .sort((a, b) => parseInt(a) - parseInt(b))
                .map((cell, i) => {
                  // console.log("tablejs object keys row", Object.keys(row));
                  return <TableCell key={i}>{row[cell]}</TableCell>;
                })}
              {actions && (
                <TableCell>
                  <Button onClick={() => handleClickOpen(row.id)} title="Edit">
                    <i className="far fa-edit"></i>
                  </Button>
                  <Button
                    onClick={() => handleClickDelete(row.id)}
                    color="secondary"
                    title="Delete"
                  >
                    <i className="fas fa-ban"></i>
                  </Button>
                </TableCell>
              )}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
}

export default TableTK;
