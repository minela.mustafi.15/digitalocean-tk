import React, { Component } from "react";
import "./footer.css";

class Footer extends Component {
  render() {
    return (
      <footer>
        {/* {this.props.isLoggedIn && (
          <ul>
            <li className="nav-item" onClick={this.closeNav}>
              <a href="/#top">About</a>
            </li>
            <li className="nav-item" onClick={this.closeNav}>
              <a href="/#services">Services</a>
            </li>
            <li className="nav-item" onClick={this.closeNav}>
              <a href="/#team">Our Team</a>
            </li>
            <li className="nav-item" onClick={this.closeNav}>
              <a href="/#contact">Contact</a>
            </li>
          </ul>
        )} */}
        <p className="text-light text-center mb-0">Copyright © 2019 Gigi School</p>
      </footer>
    );
  }
}

export default Footer;
