import React, { Fragment } from "react";
import { Formik, Field, Form, ErrorMessage, FastField } from "formik";
import * as Yup from "yup";
import { Container, Grid, Dialog, DialogContent, Button, TextField } from "@material-ui/core";
// import FormikTextField from "../MuiFormik/FormikTextField";
import FormikTextField from "../MuiFormik/FormikTextField";
import FormikSelect from "../MuiFormik/FormikSelect";
import Config from "../../Config";
import axios from "axios";

class EditTeam extends React.PureComponent {
  setInitialValues = currentItem => {
    return {
      name: currentItem.name ? currentItem.name : "",
      description: currentItem.description ? currentItem.description : "",
      status: currentItem.status ? currentItem.status : true
    };
  };
  componentDidUpdate() {
    // console.log("update");
    const { currentItem, selectedItemId } = this.props;
    const editForm = {
      initialValues: this.setInitialValues(currentItem)
    };
  }

  render() {
    // const CustomFieldComponent = props => {
    //   // console.log("errors", props.error, "touched", props.touched, "value", props.value);
    //   console.log("rendered");
    //   return (
    //     <TextField
    //       {...props}
    //       margin="normal"
    //       variant="outlined"
    //       fullWidth={true}
    //       label={props.label}
    //       type={props.type}
    //       rows={props.rows}
    //       multiline={props.multiline}
    //       InputLabelProps={props.type === "date" ? { shrink: true } : {}}
    //       // floatingLabelFixed={props.type === "date" ? true : false}
    //       // error={Boolean(props.touched && props.error)}
    //       // helperText={props.touched && props.error ? props.error : " "}
    //     />
    //   );
    // };
    const { open, handleClickClose, currentItem, selectedItemId } = this.props;
    const editForm = {
      initialValues: this.setInitialValues(currentItem),
      validation: Yup.object({
        name: Yup.string().required("Field is Required"),
        description: Yup.string().required("Field is Required")
      }),
      submit: (formValues, { setSubmitting }) => {
        console.log("in submit");
        console.log(Config, selectedItemId);
        let values = {};
        values = Object.assign(values, formValues);
        let putUrl = Config.apiUrl + "teams/" + selectedItemId;
        let postUrl = Config.apiUrl + "teams";
        const that = this;
        if (selectedItemId) {
          values.id = selectedItemId;
          console.log("in put");
          axios
            .put(putUrl, values, Config.authHeader)
            .then(res => {
              console.log(res.data);
              that.props.updateRow(res.data);
              handleClickClose();
            })
            .catch(err => {
              console.log("error", err);
              alert("Something went wrong");
            })
            .finally(() => {
              setSubmitting(false);
            });
        } else {
          console.log("in post", values);
          values.status = true;
          axios
            .post(postUrl, values, Config.authHeader)
            .then(res => {
              console.log(res);
              that.props.addRow(res.data);
              handleClickClose();
            })
            .catch(err => {
              console.log("error", err);
              alert("Something went wrong");
            })
            .finally(() => {
              setSubmitting(false);
            });
        }
      }
    };
    return (
      <React.Fragment>
        <Dialog
          open={open}
          onClose={handleClickClose}
          aria-labelledby="form-dialog-title"
          maxWidth="md"
        >
          <h4 id="form-dialog-title" className="text-center">
            {selectedItemId ? "Team ID: " + selectedItemId : "Add Team"}
          </h4>
          <DialogContent className="m-1">
            <Formik
              initialValues={editForm.initialValues}
              validationSchema={editForm.validation}
              onSubmit={editForm.submit}
            >
              {props => {
                return (
                  <Form>
                    <Container>
                      {/* <FastField name="name" label="Name" type="text" as={CustomFieldComponent} />
                      <div>{props.errors.name && props.touched.name ? props.errors.name : ""}</div>
                      <FastField
                        name="description"
                        label="Description"
                        type="text"
                        multiline={true}
                        rows={6}
                        as={CustomFieldComponent}
                      />
                      <div>
                        {props.errors.description && props.touched.description
                          ? props.errors.description
                          : ""}
                      </div> */}
                      <FormikTextField id="name" name="name" label="Name" />
                      <FormikTextField
                        id="description"
                        name="description"
                        label="Description"
                        multiline={true}
                        rows={6}
                      />
                      <div className="text-center">
                        {selectedItemId && (
                          <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            className="mr-1"
                            id="team-update-btn"
                          >
                            Update
                          </Button>
                        )}
                        {!selectedItemId && (
                          <Button
                            type="submit"
                            variant="contained"
                            className="mr-1 bg-success text-light"
                            id="team-add-btn"
                          >
                            Add
                          </Button>
                        )}
                        <Button
                          type="button"
                          variant="contained"
                          color="default"
                          onClick={handleClickClose}
                          id="team-cancel-btn"
                        >
                          Cancel
                        </Button>
                      </div>
                    </Container>
                  </Form>
                );
              }}
            </Formik>
          </DialogContent>
        </Dialog>
      </React.Fragment>
    );
  }
}
export default EditTeam;
