import React from "react";
import LoginDialog from "../Static/Login-Dialog";
import { NavLink } from "react-router-dom";
import { Button, Menu, MenuItem } from "@material-ui/core";
import { HashLink } from "react-router-hash-link";
import "./header.css";
import axios from "axios";
import Config from "../../Config";
import AuthService from "../../auth/AuthService";
import { authLogout } from "../../store/actions/authActions";
import { connect } from "react-redux";

// react-router-hash-link issue workaround: https://github.com/mui-org/material-ui/issues/15903
const ForwardHashLink = React.forwardRef((props, ref) => <HashLink {...props} innerRef={ref} />);

class Header extends React.Component {
  state = {
    open: false /* Dialog closed */,
    homeMenuAnchor: null,
    databaseMenuAnchor: null,
    reportsMenuAnchor: null,
    dashboardsMenuAnchor: null
  };

  handleClickOpen = () => {
    this.setState({ open: true });
    this.closeNav();
  };
  handleClickClose = () => {
    this.setState({ open: false });
  };
  openIdp = () => {
    const authService = new AuthService();
    authService
      .signinRedirect()
      .then(res => console.log(res))
      .catch(err => console.log(err));
  };
  handleHomeMenuClick = event => {
    this.setState({ homeMenuAnchor: event.currentTarget });
  };

  handleHomeMenuClose = () => {
    this.setState({ homeMenuAnchor: null });
    this.closeNav();
  };
  handleReportsMenuClick = event => {
    this.setState({ reportsMenuAnchor: event.currentTarget });
  };

  handleReportsMenuClose = () => {
    this.setState({ reportsMenuAnchor: null });
    this.closeNav();
  };
  handleDashboardsMenuClick = event => {
    this.setState({ dashboardsMenuAnchor: event.currentTarget });
  };

  handleDashboardsMenuClose = () => {
    this.setState({ dashboardsMenuAnchor: null });
    this.closeNav();
  };
  handleDatabaseMenuClick = event => {
    this.setState({ databaseMenuAnchor: event.currentTarget });
  };

  handleDatabaseMenuClose = () => {
    this.setState({ databaseMenuAnchor: null });
    this.closeNav();
  };
  toggleNav = () => {
    let listWrap = document.querySelector(".nav-items");
    listWrap.classList.toggle("h-xs-0");
    listWrap.classList.toggle("h-xs-auto");
  };
  closeNav = () => {
    let listWrap = document.querySelector(".nav-items");
    listWrap.classList.add("h-xs-0");
    listWrap.classList.remove("h-xs-auto");
  };
  render() {
    const { currentUser, setLoginState, unsetLoginState, basicLogin, basicLogout } = this.props;
    return (
      <React.Fragment>
        <nav className="flex flex-wrap justify-space-between align-items-center">
          <h4 className="nav-logo m-0">TK</h4>
          <button className="nav-toggle" onClick={this.toggleNav}>
            &#9776;
          </button>
          <ul className="nav-items h-xs-0">
            {!currentUser && (
              <React.Fragment>
                <li className="nav-item" onClick={this.closeNav}>
                  <HashLink to="/#about">About</HashLink>
                </li>
                <li className="nav-item" onClick={this.closeNav}>
                  <HashLink to="/#services">Services</HashLink>
                </li>
                <li className="nav-item" onClick={this.closeNav}>
                  <HashLink to="/#team">Our Team</HashLink>
                </li>
                <li className="nav-item" onClick={this.closeNav}>
                  <HashLink to="/#contact">Contact</HashLink>
                </li>
                <li className="nav-item">
                  <Button variant="contained" color="primary" onClick={this.handleClickOpen}>
                    Log In
                  </Button>
                </li>
                {/* <li className="nav-item">
                  <Button variant="contained" color="primary" onClick={unsetLoginState}>
                    Log Out
                  </Button>
                </li> */}
              </React.Fragment>
            )}
            {currentUser && (
              <React.Fragment>
                <li className="nav-item" onClick={this.closeNav}>
                  <NavLink exact to="/dashboard">
                    Dashboard
                  </NavLink>
                </li>
                <li className="nav-item">
                  <a
                    aria-controls="home-menu"
                    aria-haspopup="true"
                    onClick={this.handleHomeMenuClick}
                    href="#"
                  >
                    Home <i className="fas fa-caret-down"></i>
                  </a>
                  <Menu
                    id="home-menu"
                    anchorEl={this.state.homeMenuAnchor}
                    keepMounted
                    open={Boolean(this.state.homeMenuAnchor)}
                    onClose={this.handleHomeMenuClose}
                  >
                    <ForwardHashLink to="/#about">
                      <MenuItem onClick={this.handleHomeMenuClose}>About </MenuItem>
                    </ForwardHashLink>
                    <ForwardHashLink to="/#services">
                      <MenuItem onClick={this.handleHomeMenuClose}>Services </MenuItem>
                    </ForwardHashLink>

                    <ForwardHashLink to="/#team">
                      <MenuItem onClick={this.handleHomeMenuClose}>Team</MenuItem>
                    </ForwardHashLink>

                    <ForwardHashLink to="/#contact">
                      <MenuItem onClick={this.handleHomeMenuClose}>Contact</MenuItem>
                    </ForwardHashLink>
                  </Menu>
                </li>
                <li className="nav-item">
                  <a
                    aria-controls="database-menu"
                    aria-haspopup="true"
                    onClick={this.handleDatabaseMenuClick}
                    href="#"
                  >
                    Database <i className="fas fa-caret-down"></i>
                  </a>
                  <Menu
                    id="database-menu"
                    anchorEl={this.state.databaseMenuAnchor}
                    keepMounted
                    open={Boolean(this.state.databaseMenuAnchor)}
                    onClose={this.handleDatabaseMenuClose}
                  >
                    <NavLink exact to="/employees">
                      <MenuItem onClick={this.handleDatabaseMenuClose}>Employees</MenuItem>
                    </NavLink>

                    <NavLink exact to="/customers">
                      <MenuItem onClick={this.handleDatabaseMenuClose}>Customers</MenuItem>
                    </NavLink>

                    <NavLink exact to="/projects">
                      <MenuItem onClick={this.handleDatabaseMenuClose}>Projects</MenuItem>
                    </NavLink>

                    <NavLink exact to="/teams">
                      <MenuItem onClick={this.handleDatabaseMenuClose}>Teams</MenuItem>
                    </NavLink>
                  </Menu>
                </li>
                <li className="nav-item">
                  <a
                    aria-controls="reports-menu"
                    aria-haspopup="true"
                    onClick={this.handleReportsMenuClick}
                    href="#"
                  >
                    Reports <i className="fas fa-caret-down"></i>
                  </a>
                  <Menu
                    id="reports-menu"
                    anchorEl={this.state.reportsMenuAnchor}
                    keepMounted
                    open={Boolean(this.state.reportsMenuAnchor)}
                    onClose={this.handleReportsMenuClose}
                  >
                    {/* <NavLink exact to="/time-tracking">
                      <MenuItem onClick={this.handleReportsMenuClose}>Time Tracking</MenuItem>
                    </NavLink>

                    <NavLink exact to="/personal-report">
                      <MenuItem onClick={this.handleReportsMenuClose}>Personal Report</MenuItem>
                    </NavLink> */}

                    <NavLink exact to="/reports/annual">
                      <MenuItem onClick={this.handleReportsMenuClose}>Annual</MenuItem>
                    </NavLink>

                    <NavLink exact to="/reports/monthly">
                      <MenuItem onClick={this.handleReportsMenuClose}>Monthly</MenuItem>
                    </NavLink>
                  </Menu>
                </li>
                <li className="nav-item">
                  <a
                    aria-controls="dashboards-menu"
                    aria-haspopup="true"
                    onClick={this.handleDashboardsMenuClick}
                    href="#"
                  >
                    Statistics <i className="fas fa-caret-down"></i>
                  </a>
                  <Menu
                    id="dashboards-menu"
                    anchorEl={this.state.dashboardsMenuAnchor}
                    keepMounted
                    open={Boolean(this.state.dashboardsMenuAnchor)}
                    onClose={this.handleDashboardsMenuClose}
                  >
                    <NavLink exact to="/dashboards/company">
                      <MenuItem onClick={this.handleDashboardsMenuClose}>Company</MenuItem>
                    </NavLink>
                    <NavLink exact to="/dashboards/team">
                      <MenuItem onClick={this.handleDashboardsMenuClose}>Teams</MenuItem>
                    </NavLink>
                  </Menu>
                </li>
                <li className="nav-item">
                  <Button variant="contained" color="primary" onClick={this.props.authLogout}>
                    Log Out
                  </Button>
                </li>
              </React.Fragment>
            )}
          </ul>
        </nav>
        <LoginDialog
          open={this.state.open}
          handleClickClose={this.handleClickClose}
          // setLoginState={setLoginState}
        />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.authData.user
  };
};
export default connect(mapStateToProps, { authLogout })(Header);
