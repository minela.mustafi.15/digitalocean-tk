import React, { useEffect } from "react";
import { Select, MenuItem, IconButton } from "@material-ui/core";
import { Formik, Form } from "formik";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { addDay, editDay } from "../../store/actions/calendarActions";

function CalendarAbsent(props) {
  useEffect(() => {}, [props.value, props.day]);
  return (
    <div>
      {console.log("ABSENT DAY props.day", props.day)}
      <Formik
        enableReinitialize
        initialValues={{
          dayType: props.value
        }}
        onSubmit={values => {
          console.log("ON SUBMIT", props.value, props.user.id, props.day.date);
          let data = {
            dayType: {
              id: props.value
            },
            employee: {
              id: props.user.id
            },
            date: props.day.date,
            totalHours: 0
          };
          console.log("ADD DAY DATA", data);
          if (props.day.id === 0) {
            props.addDay(data);
          } else {
            data.id = props.day.id;
            console.log("DATA BEFORE EDITING DAY", data);
            props.editDay(props.day.id, data);
          }
        }}
      >
        <Form>
          <IconButton color="primary" type="submit">
            {props.day.id ? <EditIcon /> : <AddIcon />}
          </IconButton>
        </Form>
      </Formik>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    user: state.authData.user
  };
};

export default connect(mapStateToProps, { addDay, editDay })(withRouter(CalendarAbsent));
