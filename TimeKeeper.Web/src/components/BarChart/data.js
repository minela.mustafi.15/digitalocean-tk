export default {
  employeesCount: 9,
  projectsCount: 0,
  baseTotalHours: 168,
  totalHours: 1512,
  workingHours: 0,
  missingEntries: [
    {
      team: {
        id: 2,
        name: "Bravo"
      },
      value: 0
    },
    {
      team: {
        id: 3,
        name: "Charlie"
      },
      value: 672
    },
    {
      team: {
        id: 4,
        name: "Delta"
      },
      value: 0
    },
    {
      team: {
        id: 5,
        name: "Foxtrot"
      },
      value: 0
    },
    {
      team: {
        id: 6,
        name: "Oscar"
      },
      value: 0
    },
    {
      team: {
        id: 7,
        name: "Sierra"
      },
      value: 504
    },
    {
      team: {
        id: 8,
        name: "Tango"
      },
      value: 0
    },
    {
      team: {
        id: 9,
        name: "Yankee"
      },
      value: 352
    },
    {
      team: {
        id: 1,
        name: "Alpha"
      },
      value: 168
    }
  ],
  paidTimeOff: [
    {
      team: {
        id: 2,
        name: "Bravo"
      },
      value: 0
    },
    {
      team: {
        id: 3,
        name: "Charlie"
      },
      value: 0
    },
    {
      team: {
        id: 4,
        name: "Delta"
      },
      value: 0
    },
    {
      team: {
        id: 5,
        name: "Foxtrot"
      },
      value: 0
    },
    {
      team: {
        id: 6,
        name: "Oscar"
      },
      value: 0
    },
    {
      team: {
        id: 7,
        name: "Sierra"
      },
      value: 0
    },
    {
      team: {
        id: 8,
        name: "Tango"
      },
      value: 0
    },
    {
      team: {
        id: 9,
        name: "Yankee"
      },
      value: 0
    },
    {
      team: {
        id: 1,
        name: "Alpha"
      },
      value: 0
    }
  ],
  overtime: [
    {
      team: {
        id: 2,
        name: "Bravo"
      },
      value: 0
    },
    {
      team: {
        id: 3,
        name: "Charlie"
      },
      value: 0
    },
    {
      team: {
        id: 4,
        name: "Delta"
      },
      value: 0
    },
    {
      team: {
        id: 5,
        name: "Foxtrot"
      },
      value: 0
    },
    {
      team: {
        id: 6,
        name: "Oscar"
      },
      value: 0
    },
    {
      team: {
        id: 7,
        name: "Sierra"
      },
      value: 0
    },
    {
      team: {
        id: 8,
        name: "Tango"
      },
      value: 0
    },
    {
      team: {
        id: 9,
        name: "Yankee"
      },
      value: 0
    },
    {
      team: {
        id: 1,
        name: "Alpha"
      },
      value: 0
    }
  ]
};
