import React from "react";
import Slider from "react-slick";
import team from "../../data/team.json";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import TeamItem from "./Team-Item";

class Team extends React.Component {
  render() {
    let settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            arrows: true
          }
        },
        {
          breakpoint: 1000,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
            arrows: true
          }
        },
        {
          breakpoint: 750,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    return (
      <section id="team" className="team">
        <div className="section-spacer"></div>
        <h3 className="text-center mt-0 mb-2 text-light">Our Team</h3>
        <div className="container">
          <div className="flex justify-content-center">
            <Slider {...settings}>
              {team.map((teamItem, i) => (
                <TeamItem {...teamItem} key={i} />
              ))}
            </Slider>
          </div>
        </div>
        <div className="section-spacer"></div>
      </section>
    );
  }
}

export default Team;
