import React, { Component } from "react";

function Hero(props) {
  const { id, backgroundImage, title, description } = props;
  return (
    <section id={id} className="hero" style={{ backgroundImage: `url(${backgroundImage})` }}>
      <div className="hero-content">
        <div className="container">
          <h1 className="hero-title text-light mt-0">{title}</h1>
          <p className="hero-description">{description}</p>
        </div>
      </div>
    </section>
  );
}

export default Hero;
