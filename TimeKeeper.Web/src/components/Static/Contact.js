import React, { Component } from "react";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { Container, Grid, Button } from "@material-ui/core";
import Config from "../../Config";
import FormikTextField from "../MuiFormik/FormikTextField";

class Contact extends Component {
  state = {
    contactView: null,
    contactHeight: null
  };
  handleContactNull = () => {
    this.setState({ contactView: null });
  };
  componentDidMount() {
    // SET A FIXED HEIGHT OF THE FORM
    // SO IT STAYS THE SAME WHEN THE STATE CHANGES
    let contact = document.querySelector("#contact-inputs");
    let contactTitle = document.querySelector("#contact-title");
    let contactHeight =
      parseFloat(window.getComputedStyle(contact).getPropertyValue("height")) +
      parseFloat(window.getComputedStyle(contactTitle).getPropertyValue("height"));
    // console.log(contactHeight);
    this.setState({ contactHeight: contactHeight + "px" });
  }
  render() {
    const renderContact = [null, "inProgress", "submitted", "failed"];
    const contactForm = {
      initialValues: {
        name: "",
        email: "",
        phone: "",
        message: ""
      },
      validation: Yup.object({
        name: Yup.string().required("Name is Required "),
        phone: Yup.string()
          .min(9, "Phone must be 9 characters or more ")
          .required("Phone is Required "),
        email: Yup.string()
          .email("Invalid email addresss")
          .required("Email is Required "),
        message: Yup.string()
          .min(9, "Message must be 10 characters or more ")
          .required("Message is Required ")
      }),
      submit: (values, { setSubmitting }) => {
        this.setState({ contactView: renderContact[1] });
        axios
          .post(Config.apiUrl + "contact", values)
          .then(res => {
            console.log(res);
            this.setState({ contactView: renderContact[2] });
          })
          .catch(err => {
            console.log(err);
            console.log(values);
            alert("Something went wrong");
            this.setState({ contactView: renderContact[3] });
          })
          .finally(() => {
            setSubmitting(false);
          });
      }
    };
    return (
      <section id="contact" className="bg-light">
        <div className="section-spacer"></div>
        {/* STARTS WITH THIS STATE */}
        {this.state.contactView === renderContact[0] && (
          <React.Fragment>
            <h3 id="contact-title" className="text-center mt-0 mb-2 text-dark">
              Contact Us
            </h3>
            <Formik
              initialValues={contactForm.initialValues}
              validationSchema={contactForm.validation}
              onSubmit={contactForm.submit}
            >
              <Form>
                {/* <div
                  id="contact-inputs"
                  className="input-group flex flex-column align-items-center justify-content-center pl-3 pr-3"
                > */}
                <Container id="contact-inputs">
                  <Grid container spacing={4}>
                    <Grid item sm={6}>
                      <FormikTextField type="text" id="name" name="name" label="Name Surname*" />
                      <FormikTextField type="email" id="email" name="email" label="Email*" />
                      <FormikTextField type="text" id="phone" name="phone" label="Phone*" />
                    </Grid>
                    <Grid item sm={6}>
                      <FormikTextField
                        type="text"
                        id="message"
                        name="message"
                        label="Message*"
                        multiline={true}
                        rows="6"
                      />
                      <div className="text-right">
                        <Button
                          className="formik-input-btn"
                          variant="outlined"
                          color="primary"
                          fullWidth={true}
                          size="large"
                          type="submit"
                        >
                          Send Message
                        </Button>
                      </div>
                    </Grid>
                  </Grid>
                </Container>

                {/* </div> */}
              </Form>
            </Formik>
          </React.Fragment>
        )}
        {/* IN PROGRESS */}
        {this.state.contactView === renderContact[1] && (
          <div
            className="input-group flex flex-column align-items-center justify-content-center pl-3 pr-3"
            style={{ minHeight: this.state.contactHeight }}
          >
            <h2>Request in Progress...</h2>
          </div>
        )}
        {/* SUBMITTED */}
        {this.state.contactView === renderContact[2] && (
          <div
            className="input-group flex flex-column align-items-center justify-content-center pl-3 pr-3"
            style={{ minHeight: this.state.contactHeight }}
          >
            <h2>Thank you for contacting us!</h2>
            <p>Your message has been received</p>
            <button
              className="input-button shadow-1"
              type="button"
              onClick={this.handleContactNull}
            >
              Back
            </button>
          </div>
        )}
        {/* FAILED */}
        {this.state.contactView === renderContact[3] && (
          <div
            className="input-group flex flex-column align-items-center justify-content-center pl-3 pr-3"
            style={{ minHeight: this.state.contactHeight }}
          >
            <h2>Something went Wrong</h2>
            <button
              className="input-button shadow-1"
              type="button"
              onClick={this.handleContactNull}
            >
              Back
            </button>
          </div>
        )}
        <div className="section-spacer"></div>
      </section>
    );
  }
}

export default Contact;
