import services from "../../data/services.json";
import React, { Component, Fragment } from "react";
import ServiceItem from "./Service-Item";
import { Container, Grid } from "@material-ui/core";

function Services() {
  return (
    <Fragment>
      <section id="services" className="services bg-dark text-light">
        <div className="section-spacer bg-dark"></div>
        <Container>
          <Grid container spacing={4} id="service-grid">
            {services.map((service, i) => (
              <Grid item sm={4} key={i}>
                <ServiceItem {...service} />
              </Grid>
            ))}
          </Grid>
        </Container>
        <div className="section-spacer bg-dark"></div>
      </section>
    </Fragment>
  );
}

export default Services;
