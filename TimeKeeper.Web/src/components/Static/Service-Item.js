import React from "react";
function ServiceItem(props) {
  const { icon, title, description } = props;
  return (
    <div className="card text-center p-1">
      <div className="card-icon">
        <i className={icon}></i>
      </div>
      <h5 className="card-title">{title}</h5>
      <p className="card-description">{description}</p>
    </div>
  );
}

export default ServiceItem;
