import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Formik, Field, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { withRouter } from "react-router-dom";
import Config from "../../Config";
import { connect } from "react-redux";
import { auth } from "../../store/actions/authActions";

class LoginDialog extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { open, handleClickClose, setLoginState } = this.props;
    const loginForm = {
      initialValues: {
        username: "",
        password: ""
      },
      validation: Yup.object({
        username: Yup.string().required("Username is Required"),
        password: Yup.string()
          // .min(9, "Must be 9 characters or more")
          .required("Password is Required")
      }),
      submit: (values, { setSubmitting }) => {
        axios
          .post(Config.apiUrl + "users", values)
          .then(res => {
            // console.log(res);
            Config.token = `Basic ${res.data.base64}`;
            Config.authHeader = {
              headers: {
                "Content-Type": "application/json",
                Authorization: Config.token
              }
            };
            console.log(Config);
            setLoginState();
            handleClickClose();
            this.props.history.push("/dashboard");
          })
          .catch(err => {
            console.log(err);
            alert("Something went wrong");
          })
          .finally(() => setSubmitting(false));
      }
      // submit: (values, { setSubmitting }) => {
      //     // console.log(values);
      //     setSubmitting(false);
      //     setLoginState();
      //     handleClickClose();
      //     this.props.history.push('/dashboard')
      // }
    };
    return (
      <React.Fragment>
        <Dialog open={open} onClose={handleClickClose} aria-labelledby="form-dialog-title">
          <div className="m-1">
            <DialogTitle id="form-dialog-title" className="text-center">
              Time Keeper Login
            </DialogTitle>
            <DialogContent>
              <Formik
                initialValues={loginForm.initialValues}
                validationSchema={loginForm.validation}
                onSubmit={values => {
                  this.props.auth(values);
                  handleClickClose();
                }}
              >
                <Form>
                  <div className="input-group flex flex-column align-items-center justify-content-center login-form">
                    <div className="input-alerts">
                      <ErrorMessage name="username" component="div" className="input-alert" />
                      <ErrorMessage name="password" component="div" className="input-alert" />
                    </div>
                    <Field
                      className="input-control shadow-1"
                      type="text"
                      id="username"
                      name="username"
                      placeholder="Username*"
                    />
                    <Field
                      className="input-control shadow-1"
                      type="password"
                      id="password"
                      name="password"
                      placeholder="Password*"
                    />

                    {/* <div className="flex justify-space-between mb-1-5"> */}
                    <button
                      className="input-button login-btn shadow-1"
                      type="submit"
                      placeholder="Message..."
                    >
                      Login
                    </button>
                    {/* <button className="input-button shadow-1 bg-g" type="button"  placeholder="Message..."><i className="fab fa-google"></i> Login with Google</button> */}
                    {/* </div> */}
                    {/* <div className="flex justify-space-between">
                          <div><label htmlFor="rememberMe">Remember Me</label><input type="checkbox" name="rememberMe" id="rememberMe"/></div>
                          <a href="#">Forgot Password?</a>
                          </div> */}
                  </div>
                </Form>
              </Formik>
            </DialogContent>
          </div>
        </Dialog>
      </React.Fragment>
    );
  }
}

export default connect(null, { auth })(withRouter(LoginDialog));
