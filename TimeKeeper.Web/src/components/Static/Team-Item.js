import React, { Component } from "react";

function TeamItem(props) {
  const { picture, fullname, title, facebook, linkedin, twitter } = props;
  return (
    <div className="card text-center">
      <div className="profile shadow-1">
        <img src={picture} className="profile-picture img-responsive" alt="" />
        <h5 className="profile-fullname">{fullname}</h5>
        <p className="profile-title">{title}</p>
        <ul className="profile-social-media">
          <li className="social-media-link bg-fb">
            <a href={facebook}>
              <i className="fab fa-facebook-f"></i>
            </a>
          </li>
          <li className="social-media-link bg-ln">
            <a href={linkedin}>
              <i className="fab fa-linkedin-in"></i>
            </a>
          </li>
          <li className="social-media-link bg-tw">
            <a href={twitter}>
              <i className="fab fa-twitter"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default TeamItem;
