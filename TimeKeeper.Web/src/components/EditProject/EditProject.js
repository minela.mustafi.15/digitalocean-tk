import React, { Fragment } from "react";
import { Formik, Field, Form, ErrorMessage, FastField } from "formik";
import * as Yup from "yup";
import {
  Container,
  Grid,
  Paper,
  Dialog,
  DialogContent,
  DialogTitle,
  Button,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText
} from "@material-ui/core";
import FormikTextField from "../MuiFormik/FormikTextField";
import FormikSelect from "../MuiFormik/FormikSelect";
import TableTK from "../Table/Table";
import Config from "../../Config";
import axios from "axios";
import projectStatuses from "../../data/projectStatuses.json";
import pricingTypes from "../../data/pricingTypes.json";

class EditProject extends React.PureComponent {
  state = {
    selectedProjectStatus: "",
    selectedPricingType: "",
    teams: [],
    selectedTeam: "",
    customers: [],
    selectedCustomer: ""
  };
  changeProjectStatus = e => {
    this.setState({ selectedProjectStatus: e.target.value });
  };
  changePricingType = e => {
    this.setState({ selectedPricingType: e.target.value });
  };
  changeTeam = e => {
    this.setState({ selectedTeam: e.target.value });
  };
  changeCustomer = e => {
    this.setState({ selectedCustomer: e.target.value });
  };
  setInitialValues = (
    currentItem,
    selectedProjectStatus,
    selectedPricingType,
    teams,
    selectedTeam,
    customers,
    selectedCustomer
  ) => {
    return {
      name: currentItem.name ? currentItem.name : "",
      description: currentItem.description ? currentItem.description : "",
      begindate: currentItem.begindate ? currentItem.begindate : "",
      endDate: currentItem.endDate ? currentItem.endDate : "",
      status: currentItem.status
        ? projectStatuses.find(({ id }) => id === currentItem.status.id)
        : selectedProjectStatus,
      team: currentItem.team ? teams.find(({ id }) => id === currentItem.team.id) : selectedTeam,
      customer: currentItem.customer
        ? customers.find(({ id }) => id === currentItem.customer.id)
        : selectedCustomer,
      pricing: currentItem.pricing
        ? pricingTypes.find(({ id }) => id === currentItem.pricing.id)
        : selectedPricingType
    };
  };
  componentDidMount() {
    axios(Config.apiUrl + "teams", Config.authHeader)
      .then(res => {
        console.log("teams", res);
        this.setState({
          teams: res.data.map(x => {
            return {
              id: x.id,
              name: x.name
            };
          })
        });
        console.log("teams state", this.state.teams);
      })
      .catch(err => {
        console.log("teams err", err);
        alert(err);
      });
    axios(Config.apiUrl + "customers", Config.authHeader)
      .then(res => {
        console.log("customers", res);
        this.setState({
          customers: res.data.map(x => {
            return {
              id: x.id,
              name: x.name
            };
          })
        });
        console.log("customers state", this.state.customers);
      })
      .catch(err => {
        console.log("customers err", err);
        alert(err);
      });
  }
  componentDidUpdate() {
    console.log("update");
    const { currentItem, selectedItemId } = this.props;

    // console.log(currentItem);
    const editForm = {
      initialValues: this.setInitialValues(
        currentItem,
        this.state.selectedProjectStatus,
        this.state.selectedPricingType,
        this.state.teams,
        this.state.selectedTeam,
        this.state.customers,
        this.state.selectedCustomer
      )
    };
  }

  render() {
    const { open, handleClickClose, currentItem, selectedItemId } = this.props;
    const editForm = {
      initialValues: this.setInitialValues(
        currentItem,
        this.state.selectedProjectStatus,
        this.state.selectedPricingType,
        this.state.teams,
        this.state.selectedTeam,
        this.state.customers,
        this.state.selectedCustomer
      ),
      validation: Yup.object({
        name: Yup.string().required("Field is Required"),
        description: Yup.string().required("Field is Required"),
        begindate: Yup.string().required("Field is Required"),
        endDate: Yup.string().required("Field is Required"),
        status: Yup.string().required("Field is Required"),
        team: Yup.string().required("Field is Required"),
        customer: Yup.string().required("Field is Required"),
        pricing: Yup.string().required("Field is Required")
        // amount: this.state.selectedPricingType
        //   ? this.state.selectedPricingType.name === "Fixed Bid"
        //     ? Yup.string().required("Field is Required")
        //     : Yup.string()
        //   : Yup.string()
      }),
      submit: (formValues, { setSubmitting }) => {
        console.log("in submit");
        console.log(Config, selectedItemId);
        let values = {};
        values = Object.assign(values, formValues);
        let putUrl = Config.apiUrl + "projects/" + selectedItemId;
        let postUrl = Config.apiUrl + "projects";
        values.customer = {
          id: values.customer.id,
          name: "fix this on backend",
          email: "placeholder@gmail.com",
          contact: "these fields have no use but are required"
        };
        values.team = {
          id: values.team.id,
          name: "fix this on backend"
        };
        values.status = { id: values.status.id };
        values.pricing = { id: values.pricing.id };
        const that = this;
        if (selectedItemId) {
          values.id = selectedItemId;
          console.log("in put");
          axios
            .put(putUrl, values, Config.authHeader)
            .then(res => {
              console.log(res.data);
              that.props.updateRow(res.data);
              handleClickClose();
            })
            .catch(err => {
              console.log("error", err);
              alert("Something went wrong");
            })
            .finally(() => {
              this.setState({
                selectedProjectStatus: "",
                selectedTeam: "",
                selectedCustomer: "",
                selectedPricingType: ""
              });
              setSubmitting(false);
            });
        } else {
          console.log("in post", values);
          axios
            .post(postUrl, values, Config.authHeader)
            .then(res => {
              console.log(res);
              that.props.addRow(res.data);
              handleClickClose();
            })
            .catch(err => {
              console.log("error", err);
              alert("Something went wrong");
            })
            .finally(() => {
              this.setState({
                selectedProjectStatus: "",
                selectedTeam: "",
                selectedCustomer: "",
                selectedPricingType: ""
              });
              setSubmitting(false);
            });
        }
      }
    };
    return (
      <React.Fragment>
        <Dialog
          open={open}
          onClose={handleClickClose}
          aria-labelledby="form-dialog-title"
          maxWidth="lg"
        >
          <h4 id="form-dialog-title" className="text-center">
            Project: {selectedItemId ? selectedItemId : "ID"}
          </h4>
          <DialogContent className="m-1">
            <Formik
              initialValues={editForm.initialValues}
              validationSchema={editForm.validation}
              onSubmit={editForm.submit}
            >
              <Form id="edit-project">
                <Container>
                  <Grid container spacing={4}>
                    <Grid item xs={12} sm={4}>
                      <FormikTextField id="name" name="name" label="Project Name" />
                      <FormikTextField
                        id="description"
                        name="description"
                        label="Description"
                        multiline={true}
                        rows={6}
                      />
                      <div>
                        {selectedItemId && (
                          <Button
                            type="submit"
                            variant="contained"
                            color="primary"
                            className="mr-1"
                            id="project-update-btn"
                          >
                            Update
                          </Button>
                        )}
                        {!selectedItemId && (
                          <Button
                            type="submit"
                            variant="contained"
                            className="mr-1 bg-success text-light"
                            id="project-add-btn"
                          >
                            Add
                          </Button>
                        )}
                        <Button
                          type="button"
                          variant="contained"
                          color="default"
                          onClick={handleClickClose}
                          id="project-cancel-btn"
                        >
                          Cancel
                        </Button>
                      </div>
                    </Grid>
                    <Grid item xs={12} sm={8}>
                      <Grid container spacing={4}>
                        <Grid item sm={12} md={6}>
                          <FormikTextField
                            id="begin-date"
                            type="date"
                            name="begindate"
                            label="Start Date"
                          />
                          <FormikTextField
                            id="end-date"
                            type="date"
                            name="endDate"
                            label="End Date"
                          />
                          <FormikSelect
                            name="status"
                            id="project-status-select"
                            label="Status"
                            items={projectStatuses}
                            setSelectedValue={this.changeProjectStatus}
                          ></FormikSelect>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                          <FormikSelect
                            name="team"
                            id="team-status-select"
                            label="Team"
                            items={this.state.teams}
                            setSelectedValue={this.changeTeam}
                          ></FormikSelect>
                          <FormikSelect
                            name="customer"
                            id="customer-status-select"
                            label="Customer"
                            value=""
                            items={this.state.customers}
                            setSelectedValue={this.changeCustomer}
                          ></FormikSelect>
                          <FormikSelect
                            name="pricing"
                            id="pricing-status-select"
                            label="Pricing"
                            items={pricingTypes}
                            setSelectedValue={this.changePricingType}
                          ></FormikSelect>
                          {this.state.selectedPricingType.name === "Fixed Bid" && (
                            <FormikTextField id="amount" name="amount" label="Amount" />
                          )}
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Container>
              </Form>
            </Formik>
          </DialogContent>
        </Dialog>
      </React.Fragment>
    );
  }
}
export default EditProject;