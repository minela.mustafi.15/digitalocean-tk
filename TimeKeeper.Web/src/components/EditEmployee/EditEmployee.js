import React, { Fragment } from "react";
import { Formik, Field, Form, ErrorMessage, FastField } from "formik";
import * as Yup from "yup";
import {
  Container,
  Grid,
  Paper,
  Dialog,
  DialogContent,
  Button,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText
} from "@material-ui/core";
import FormikTextField from "../MuiFormik/FormikTextField";
import FormikSelect from "../MuiFormik/FormikSelect";
import TableTK from "../Table/Table";
import Config from "../../Config";
import axios from "axios";
import Moment from "moment";
import employeeStatuses from "../../data/employeeStatuses.json";
import employeePositions from "../../data/employeePositions.json";

class EditEmployee extends React.PureComponent {
  state = {
    selectedEmployeeStatus: "",
    selectedEmployeePosition: "",
    rolesTable: {
      head: ["Team", "Role"],
      rows: [
        {
          id: 1,
          team: { id: 0, name: "First Team" },
          role: { id: 0, name: "FE" }
        },
        {
          id: 2,
          team: { id: 1, name: "Sec Team" },
          role: { id: 1, name: "FE" }
        },
        {
          id: 3,
          team: { id: 2, name: "Third Team" },
          role: { id: 2, name: "QA" }
        }
      ]
    }
  };
  changeEmployeeStatus = e => {
    this.setState({ selectedEmployeeStatus: e.target.value });
  };
  changeEmployeePosition = e => {
    this.setState({ selectedEmployeePosition: e.target.value });
  };
  setInitialValues = (
    currentItem,
    employeePositions,
    employeeStatuses,
    selectedEmployeeStatus,
    selectedEmployeePosition
  ) => {
    return {
      firstName: currentItem.firstName ? currentItem.firstName : "",
      lastName: currentItem.lastName ? currentItem.lastName : "",
      email: currentItem.email ? currentItem.email : "",
      phone: currentItem.phone ? currentItem.phone : "",
      birthDate: currentItem.birthDate ? currentItem.birthDate : "",
      beginDate: currentItem.beginDate ? currentItem.beginDate : "",
      endDate: currentItem.endDate ? currentItem.endDate : "",
      status: currentItem.status
        ? employeeStatuses.find(({ id }) => id === currentItem.status.id)
        : selectedEmployeeStatus,
      position: currentItem.position
        ? employeePositions.find(({ id }) => id === currentItem.position.id)
        : selectedEmployeePosition
    };
  };
  // console.log('mapped', data);
  componentDidUpdate() {
    // console.log("update");
    const { currentItem, selectedItemId } = this.props;
    // console.log(currentItem);
    const loginForm = {
      initialValues: this.setInitialValues(
        currentItem,
        employeePositions,
        employeeStatuses,
        this.state.selectedEmployeePosition,
        this.state.selectedEmployeeStatus
      )
    };
  }

  render() {
    const { open, handleClickClose, currentItem, selectedItemId } = this.props;
    const loginForm = {
      initialValues: this.setInitialValues(
        currentItem,
        employeePositions,
        employeeStatuses,
        this.state.selectedEmployeePosition,
        this.state.selectedEmployeeStatus
      ),
      validation: Yup.object({
        firstName: Yup.string().required("Field is Required"),
        lastName: Yup.string().required("Field is Required"),
        email: Yup.string().required("Field is Required"),
        phone: Yup.string().required("Field is Required"),
        birthDate: Yup.string().required("Field is Required"),
        beginDate: Yup.string().required("Field is Required"),
        endDate: Yup.string(),
        status: Yup.string().required("Field is Required"),
        position: Yup.string().required("Field is Required")
      }),
      submit: (formValues, { setSubmitting }) => {
        let values = {};
        values = Object.assign(values, formValues);
        values.image = "";
        let putUrl = Config.apiUrl + "employees/" + selectedItemId;
        let postUrl = Config.apiUrl + "employees";
        console.log(Config, selectedItemId);
        const that = this;
        delete values.endDate;
        if (selectedItemId) {
          values.id = selectedItemId;
          console.log("in put:", values);
          axios
            .put(putUrl, values, Config.authHeader)
            .then(res => {
              console.log(res.data);
              that.props.updateRow(res.data);
              handleClickClose();
            })
            .catch(err => {
              console.log("error", err);
              alert("Something went wrong");
            })
            .finally(() => setSubmitting(false));
        } else {
          console.log(values);
          values.position = { id: values.position.id };
          values.status = { id: values.status.id };
          // handleClickClose();
          axios
            .post(postUrl, values, Config.authHeader)
            .then(res => {
              console.log(res);
              that.props.addRow(res.data);
              handleClickClose();
            })
            .catch(err => {
              console.log("error", err);
              alert("Something went wrong");
            })
            .finally(() => setSubmitting(false));
        }
      }
      // submit: (values, { setSubmitting }) => {
      //   console.log(values);
      //   setSubmitting(false);
      //   handleClickClose();
      // }
    };
    return (
      <React.Fragment>
        <Dialog
          open={open}
          onClose={handleClickClose}
          aria-labelledby="form-dialog-title"
          maxWidth="lg"
        >
          {/* <DialogTitle id="form-dialog-title" className="text-center">
              {selectedItemId ? selectedItemId : "ID"}
            </DialogTitle> */}
          <DialogContent className="m-1">
            <Formik
              initialValues={loginForm.initialValues}
              validationSchema={loginForm.validation}
              onSubmit={loginForm.submit}
            >
              <Form id="edit-employee">
                <Container>
                  <Grid container spacing={4}>
                    {/* <Grid item xs={12} sm={4}>
                      <img
                        src="/images/team/Amir Bunjo.jpg"
                        alt=""
                        className="profile-picture-fit mb-1-5"
                      />
                      <Button
                        className="mb-1-5"
                        type="button"
                        variant="contained"
                        color="default"
                        fullWidth={true}
                      >
                        Upload Photo
                      </Button>
                      <TableTK
                        head={this.state.rolesTable.head}
                        rows={this.state.rolesTable.rows.map(row => {
                          return {
                            team: row.team.name,
                            role: row.role.name
                          };
                        })}
                      />
                    </Grid> */}
                    <Grid item xs={12} sm={12}>
                      <Grid container spacing={4}>
                        <Grid item sm={12} md={12}>
                          <h4 className="m-0">Employee Profile (id: {selectedItemId})</h4>
                        </Grid>
                        <Grid item sm={12} md={6}>
                          <FormikTextField id="first-name" name="firstName" label="First Name" />
                          <FormikTextField id="last-name" name="lastName" label="Last Name" />
                          <FormikTextField id="email" name="email" label="email" />
                          <FormikTextField id="phone" name="phone" label="phone" />
                          <FormikTextField
                            id="birth-date"
                            type="date"
                            name="birthDate"
                            label="Birth Date"
                          />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                          <FormikTextField
                            type="date"
                            name="beginDate"
                            label="Employment Begin Date"
                            id="begin-date"
                          />

                          {/* <FormikTextField type="date" name="endDate" label="Employment End Date" /> */}
                          <FormikSelect
                            name="status"
                            id="status-select"
                            label="Status"
                            items={employeeStatuses}
                            setSelectedValue={this.changeEmployeeStatus}
                          ></FormikSelect>
                          <FormikSelect
                            name="position"
                            id="position-select"
                            label="Position"
                            items={employeePositions}
                            setSelectedValue={this.changeEmployeePosition}
                          ></FormikSelect>

                          <div className="text-right">
                            {selectedItemId && (
                              <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                className="mr-1"
                                id="employee-update-btn"
                              >
                                Update
                              </Button>
                            )}
                            {!selectedItemId && (
                              <Button
                                type="submit"
                                variant="contained"
                                className="mr-1 bg-success text-light"
                                id="employee-add-btn"
                              >
                                Add
                              </Button>
                            )}
                            <Button
                              type="button"
                              variant="contained"
                              color="default"
                              onClick={handleClickClose}
                              id="employee-cancel-btn"
                            >
                              Cancel
                            </Button>
                          </div>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Container>
              </Form>
            </Formik>
          </DialogContent>
        </Dialog>
      </React.Fragment>
    );
  }
}
export default EditEmployee;
