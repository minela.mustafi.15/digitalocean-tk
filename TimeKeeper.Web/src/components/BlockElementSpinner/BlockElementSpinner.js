import React from "react";

export default function BlockElementSpinner() {
  return (
    <h1 className="m-5 flex align-items-center justify-content-center" style={{ height: "100%" }}>
      <i className="fas fa-clock fa-spin"></i>
    </h1>
  );
}
