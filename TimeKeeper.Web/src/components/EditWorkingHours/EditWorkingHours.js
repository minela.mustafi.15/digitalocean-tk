import React, { Component, Fragment } from "react";
import { Container, Grid, AppBar, Tabs, Tab, Typography, Box } from "@material-ui/core";
import { Formik, Form } from "formik";
import FormikSelect from "../MuiFormik/FormikSelect";
import FormikTextField from "../MuiFormik/FormikTextField";
import axios from "axios";
import Config from "../../Config";
import { IconButton, Button, Paper } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import TodayIcon from "@material-ui/icons/Today";
import "./EditWorkingHours.css";
import Moment from "moment";
import Calendar from "react-calendar";

class EditWorkingHours extends Component {
  state = {
    selectedTab: 0,
    workingHours: [
      {
        selectedProject: "",
        description: "",
        hours: 0
      }
    ],
    projects: [],
    calendarDate: new Date(),
    selectedDate: Moment(Date.now()).format("YYYY-MM-DD")
  };
  // add item to working hours array
  addWorkingHours = () => {
    let newWorkingHours = [...this.state.workingHours];
    newWorkingHours.push({
      selectedProject: "",
      description: "",
      hours: 0
    });
    this.setState({
      workingHours: newWorkingHours
    });
  };
  // delete item from working hours array
  deleteWorkingHours = index => {
    let newWorkingHours = [...this.state.workingHours];
    newWorkingHours.splice(index, 1);
    this.setState({
      workingHours: newWorkingHours
    });
  };
  handleSelectedTab = (event, newValue) => {
    this.setState({ selectedTab: newValue });
  };
  // generic method for an array of select components
  handleSelectedProject = (e, index) => {
    let newWorkingHours = [...this.state.workingHours];
    newWorkingHours[index] = e.target.value;
    this.setState({
      workingHours: newWorkingHours
    });
  };
  handleDateChange = calendarDate => this.setState({ calendarDate });
  handleDayClick = selectedDate =>
    this.setState({ selectedDate: Moment(selectedDate).format("YYYY-MM-DD") });
  componentDidMount() {
    // get projects so all selects can be populated
    axios
      .get(Config.apiUrl + "projects")
      .then(res => {
        // console.log('mapped', data);
        this.setState({
          projects: res.data
        });
      })
      .catch(err => {
        alert(err);
        console.log(err);
      });
  }
  render() {
    return (
      <Fragment>
        <Container>
          <Grid container spacing={4}>
            <Grid item sm={6}>
              <Calendar
                activeStartDate={this.state.date}
                onChange={this.handleDateChange}
                value={this.state.date}
                onClickDay={this.handleDayClick}
              />
            </Grid>
            <Grid item sm={6}>
              <AppBar position="static">
                <Tabs
                  variant="fullWidth"
                  value={this.state.selectedTab}
                  onChange={this.handleSelectedTab}
                  aria-label="Working Hours Entry"
                >
                  <Tab label="Working Hours" {...a11yProps(0)} />
                  <Tab label="Absent Days" {...a11yProps(1)} />
                </Tabs>
              </AppBar>
              <Paper elevation={4}>
                <TabPanel value={this.state.selectedTab} index={0}>
                  <Formik>
                    <Form>
                      <Grid container alignItems="center" className="mb-1-5">
                        <span>{this.state.selectedDate} </span>
                        <TodayIcon />
                      </Grid>
                      {this.state.workingHours.map((x, index, array) => {
                        return (
                          <Fragment key={index}>
                            <h6>Entry: {index + 1}</h6>
                            <Grid container spacing={2} alignItems="center">
                              <Grid item xs={6}>
                                <FormikSelect
                                  name={"project-select-" + index}
                                  id={"project-select-" + index}
                                  label="Project"
                                  items={this.state.projects}
                                  setSelectedValue={e => this.handleSelectedProject(e, index)}
                                  value={x.selectedProject}
                                />
                              </Grid>
                              <Grid item xs={4}>
                                <FormikTextField
                                  name={"hours-worked-" + index}
                                  label="Hours Worked"
                                />
                              </Grid>
                              <Grid item xs={2} className="text-right">
                                {/* <span onClick={() => this.deleteWorkingHours(index)}>X</span> */}
                                <IconButton
                                  className="align-adjust-margin"
                                  aria-label="delete-working-hours"
                                  onClick={() => this.deleteWorkingHours(index)}
                                  color="secondary"
                                  disabled={array.length === 1}
                                >
                                  <DeleteIcon />
                                </IconButton>
                              </Grid>
                              <Grid item xs={12}>
                                <FormikTextField
                                  name={"description-" + index}
                                  label="Description"
                                  multiline={true}
                                  rows={2}
                                />
                              </Grid>
                            </Grid>
                          </Fragment>
                        );
                      })}

                      <Grid container spacing={2} justify="flex-end">
                        <Grid item>
                          <Button
                            aria-label="add-working-hours"
                            onClick={() => this.addWorkingHours()}
                            color="primary"
                            type="button"
                            variant="outlined"
                            className="mr-1"
                          >
                            <AddIcon /> Working Hours
                          </Button>
                        </Grid>
                        <Grid item>
                          <Button type="submit" color="primary" variant="contained">
                            Update
                          </Button>
                        </Grid>
                      </Grid>
                    </Form>
                  </Formik>
                </TabPanel>
              </Paper>
              <Paper elevation={4}>
                <TabPanel value={this.state.selectedTab} index={1}>
                  Item Two
                </TabPanel>
              </Paper>
            </Grid>
          </Grid>
        </Container>
      </Fragment>
    );
  }
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

function a11yProps(index) {
  return {
    id: `tab-${index}`,
    "aria-controls": `tabpanel-${index}`
  };
}

function dayTile(props) {
  return (
    <Fragment>
      <div>{props.hours}</div>
    </Fragment>
  );
}

export default EditWorkingHours;
