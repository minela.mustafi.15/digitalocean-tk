import React, { Fragment } from "react";
import { Formik, Field, Form, ErrorMessage, FastField } from "formik";
import * as Yup from "yup";
import {
  Container,
  Grid,
  Paper,
  Dialog,
  DialogContent,
  Button,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText
} from "@material-ui/core";
import FormikTextField from "../MuiFormik/FormikTextField";
import FormikSelect from "../MuiFormik/FormikSelect";
import TableTK from "../Table/Table";
import Config from "../../Config";
import axios from "axios";
import customerStatuses from "../../data/customerStatuses.json";

class EditCustomer extends React.PureComponent {
  state = {
    selectedCustomerStatus: ""
  };
  changeCustomerStatus = e => {
    this.setState({ selectedCustomerStatus: e.target.value });
  };
  setInitialValues = (currentItem, selectedCustomerStatus) => {
    return {
      name: currentItem.name ? currentItem.name : "",
      contact: currentItem.contact ? currentItem.contact : "",
      email: currentItem.email ? currentItem.email : "",
      phone: currentItem.phone ? currentItem.phone : "",
      road: currentItem.address ? currentItem.address.road : "",
      zipCode: currentItem.address ? currentItem.address.zipCode : "",
      city: currentItem.address ? currentItem.address.city : "",
      status: currentItem.status
        ? customerStatuses.find(({ id }) => id === currentItem.status.id)
        : selectedCustomerStatus
    };
  };
  componentDidUpdate() {
    // console.log("update");
    const { currentItem, selectedItemId } = this.props;
    // console.log(currentItem);
    const editForm = {
      initialValues: this.setInitialValues(currentItem, this.state.selectedCustomerStatus)
    };
  }

  render() {
    const { open, handleClickClose, currentItem, selectedItemId } = this.props;
    const editForm = {
      initialValues: this.setInitialValues(currentItem, this.state.selectedCustomerStatus),
      validation: Yup.object({
        name: Yup.string().required("Field is Required"),
        contact: Yup.string().required("Field is Required"),
        email: Yup.string().required("Field is Required"),
        phone: Yup.string().required("Field is Required"),
        road: Yup.string().required("Field is Required"),
        zipCode: Yup.string().required("Field is Required"),
        city: Yup.string().required("Field is Required"),
        status: Yup.string().required("Field is Required")
      }),
      submit: (formValues, { setSubmitting }) => {
        console.log("in submit");
        let values = {};
        values = Object.assign(values, formValues);
        values.image = "";

        values.address = {
          road: values.road,
          city: values.city,
          zipCode: values.zipCode
        };
        delete values.road;
        delete values.city;
        delete values.zipCode;

        let putUrl = Config.apiUrl + "customers/" + selectedItemId;
        let postUrl = Config.apiUrl + "customers";
        console.log(Config, selectedItemId);
        const that = this;
        if (selectedItemId) {
          values.id = selectedItemId;
          values.status = { id: values.status.id };
          console.log("in put");
          axios
            .put(putUrl, values, Config.authHeader)
            .then(res => {
              console.log(res.data);
              that.props.updateRow(res.data);
              handleClickClose();
            })
            .catch(err => {
              console.log("error", err);
              alert("Something went wrong");
            })
            .finally(() => {
              this.setState({ selectedCustomerStatus: "" });
              setSubmitting(false);
            });
        } else {
          console.log(values);
          values.status = { id: values.status.id };
          axios
            .post(postUrl, values, Config.authHeader)
            .then(res => {
              console.log(res);
              that.props.addRow(res.data);
              handleClickClose();
            })
            .catch(err => {
              console.log("error", err);
              alert("Something went wrong");
            })
            .finally(() => {
              this.setState({ selectedCustomerStatus: "" });
              setSubmitting(false);
            });
        }
      }
      // submit: (values, { setSubmitting }) => {
      //   console.log(values);
      //   setSubmitting(false);
      //   handleClickClose();
      // }
    };
    return (
      <React.Fragment>
        <Dialog
          open={open}
          onClose={handleClickClose}
          aria-labelledby="form-dialog-title"
          maxWidth="lg"
        >
          {/* <DialogTitle id="form-dialog-title" className="text-center">
              {selectedItemId ? selectedItemId : "ID"}
            </DialogTitle> */}
          <DialogContent className="m-1">
            <Formik
              initialValues={editForm.initialValues}
              validationSchema={editForm.validation}
              onSubmit={editForm.submit}
            >
              <Form id="edit-customer">
                <Container>
                  <Grid container spacing={4}>
                    {/* <Grid item xs={12} sm={4}></Grid> */}
                    <Grid item xs={12} sm={12}>
                      <Grid container spacing={4}>
                        <Grid item sm={12} md={12}>
                          <h4 className="m-0">Customer Profile (id: {selectedItemId})</h4>
                        </Grid>
                        <Grid item sm={12} md={6}>
                          <FormikTextField id="name" name="name" label="Business Name" />
                          <FormikTextField id="contact" name="contact" label="Contact Name" />
                          <FormikTextField id="email" name="email" label="Email" />
                          <FormikTextField id="phone" name="phone" label="Phone" />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                          <FormikTextField id="road" name="road" label="Home Address" />
                          <FormikTextField id="city" name="city" label="City" />
                          <FormikTextField id="zipCode" name="zipCode" label="Zip code" />
                          <FormikSelect
                            name="status"
                            id="customer-status-select"
                            label="Status"
                            items={customerStatuses}
                            setSelectedValue={this.changeCustomerStatus}
                          ></FormikSelect>
                          <div className="text-right">
                            {selectedItemId && (
                              <Button
                                type="submit"
                                variant="contained"
                                color="primary"
                                className="mr-1"
                                id="update-customer-btn"
                              >
                                Update
                              </Button>
                            )}
                            {!selectedItemId && (
                              <Button
                                type="submit"
                                variant="contained"
                                className="mr-1 bg-success text-light"
                                id="add-customer-btn"
                              >
                                Add
                              </Button>
                            )}
                            <Button
                              type="button"
                              variant="contained"
                              color="default"
                              onClick={handleClickClose}
                              id="customer-cancel-btn"
                            >
                              Cancel
                            </Button>
                          </div>
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Container>
              </Form>
            </Formik>
          </DialogContent>
        </Dialog>
      </React.Fragment>
    );
  }
}
export default EditCustomer;
