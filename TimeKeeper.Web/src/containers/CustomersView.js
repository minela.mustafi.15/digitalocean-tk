import React, { Component, Fragment } from "react";
import TableView from "../components/TableView/TableView";
import axios from "axios";
import Config from "../Config";
import EditCustomer from "../components/EditCustomer/EditCustomer";
import BlockElementSpinner from "../components/BlockElementSpinner/BlockElementSpinner";
import WithCrud from "../hocs/WithCrud";

class CustomersView extends Component {
  state = {
    title: "Customers",
    backgroundImage: "/images/customers.jpg"
  };
  render() {
    return (
      <Fragment>
        {!this.props.isLoading && (
          <Fragment>
            <TableView {...this.state} {...this.props} fab />
            <EditCustomer open={this.props.isEditOpen} {...this.props} />
          </Fragment>
        )}
        {this.props.isLoading && <BlockElementSpinner />}
      </Fragment>
    );
  }
}
const adjustTableItem = item => {
  return {
    id: item.id,
    name: item.name,
    contact: item.contact,
    email: item.email,
    status: item.status.name
  };
};
// Adjusts item to fill input fields in edit form
const adjustSelectedItem = item => {
  let adjustedItem = Object.assign({}, item);
  return adjustedItem;
};

// Adjusts resource items to fit table columns
const adjustRows = rows => {
  return rows.map(row => {
    return {
      id: row.id,
      "Business Name": row.name,
      "Contact Name": row.contact,
      Email: row.email,
      Status: row.status.name
    };
  });
};

export default WithCrud(
  CustomersView,
  "customers",
  adjustTableItem,
  adjustSelectedItem,
  adjustRows
);
