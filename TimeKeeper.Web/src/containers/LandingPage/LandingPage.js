import React, { Fragment, Component } from "react";
import Hero from "../../components/Static/Hero";
import Team from "../../components/Static/Team";
import Services from "../../components/Static/Services";
import Contact from "../../components/Static/Contact";
import Footer from "../../components/Footer/Footer";
import "./LandingPage.css";

class LandingPage extends Component {
  render() {
    let hero = {
      id: "about",
      title: "Time Tracker",
      description: `popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker includingversions of Lorem Ipsum.`,
      backgroundImage: "/images/clockwork.jpg"
    };
    return (
      <Fragment>
        <Hero {...hero} />
        <Services />
        <Team />
        <Contact />
      </Fragment>
    );
  }
}

export default LandingPage;
