import React, { Fragment } from "react";
import "./TeamsView.css";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import axios from "axios";
import Config from "../../Config";
import { Container, Grid, Paper, Button, Fab } from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import EditTeam from "../../components/EditTeam/EditTeam";
import BlockElementSpinner from "../../components/BlockElementSpinner/BlockElementSpinner";

class TeamsView extends React.Component {
  state = {
    teams: [],
    isTeamSelected: false,
    selectedTeam: {
      members: []
    },
    isEditOpen: false,
    selectedItemId: null,
    currentItem: {},
    isLoading: true,
    backgroundImage: "/images/teams.jpg"
  };
  addRow = item => {
    let teams = this.state.teams;
    teams.push(item);
    this.setState({
      teams: teams
    });
  };
  updateRow = item => {
    let teams = this.state.teams;
    let index = teams.findIndex(x => x.id === item.id);
    teams.splice(index, 1, item);
    this.setState({
      teams: teams
    });
  };
  deleteRow = itemId => {
    let teams = this.state.teams;
    let index = teams.findIndex(x => x.id === itemId);
    teams.splice(index, 1);
    this.setState({
      teams: teams,
      currentItem: {},
      selectedItemId: null
    });
  };
  showTeamDetails = team => {
    this.setState({ selectedTeam: team, isTeamSelected: true });
  };
  handleClickDelete = itemId => {
    if (window.confirm("Are you sure?")) {
      axios
        .delete(Config.apiUrl + "teams/" + itemId, Config.authHeader)
        .then(res => {
          console.log(res);
          this.deleteRow(itemId);
          alert("Deleted");
        })
        .catch(err => {
          alert(err);
          console.log(err);
        });
    }
  };
  handleClickOpen = itemId => {
    if (itemId !== null) {
      axios
        .get(Config.apiUrl + "teams/" + itemId, {
          headers: {
            "Content-Type": "application/json",
            Authorization: Config.token
          }
        })
        .then(res => {
          let item = {};
          item = Object.assign(item, res.data);
          console.log("item", item);
          this.setState({
            isEditOpen: true,
            selectedItemId: itemId,
            currentItem: item
          });
        })
        .catch(err => {
          alert(err);
          console.log(err);
        });
    } else {
      this.setState({
        isEditOpen: true,
        selectedItemId: null,
        currentItem: {}
      });
    }
    // console.log(itemId);
  };
  handleClickClose = () => {
    this.setState({ isEditOpen: false, selectedItemId: null, currentItem: {} });
  };

  componentDidMount() {
    axios
      .get(Config.apiUrl + "teams", Config.authHeader)
      .then(res => {
        console.log("teams", res);
        const tempTeams = res.data.map(x => {
          return {
            id: x.id,
            name: x.name,
            description: x.description,
            members: x.members
          };
        });
        this.setState({
          teams: tempTeams,
          selectedTeam: tempTeams[0],
          isLoading: false
        });
        console.log("teams state", this.state.teams);
      })
      .catch(err => {
        console.log("teams err", err);
        alert(err);
      });
  }
  render() {
    let settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1
    };
    return (
      <Fragment>
        {!this.state.isLoading && (
          <Fragment>
            <div
              className="crud-top"
              style={{ backgroundImage: `url(${this.state.backgroundImage})` }}
            >
              <h2>Teams</h2>
            </div>
            <Container fixed maxWidth="xl" className="crud-table mb-3">
              <Paper className="pl-2 pr-2 pb-2">
                <Grid container justify="center">
                  <Grid item xs={12}>
                    <Slider {...settings} className="m-3">
                      {this.state.teams.map((team, i) => (
                        <div
                          key={i}
                          onClick={() => this.showTeamDetails(team)}
                          className={
                            this.state.selectedTeam.id === team.id
                              ? "pt-1 pb-1 text-center bg-dark text-light"
                              : "pt-1 pb-1 text-center"
                          }
                        >
                          <h5 className="mb-1-5">{team.name}</h5>
                          <Button
                            variant="contained"
                            color="default"
                            onClick={() => this.handleClickOpen(team.id)}
                          >
                            Edit
                          </Button>
                          <Button
                            variant="contained"
                            color="secondary"
                            onClick={() => this.handleClickDelete(team.id)}
                          >
                            Delete
                          </Button>
                        </div>
                      ))}
                    </Slider>
                  </Grid>
                  {this.state.selectedTeam !== {} && (
                    <Grid item xs={12}>
                      <Grid container spacing={4}>
                        <Grid item xs={8}>
                          <h5 className="mb-1-5">{this.state.selectedTeam.name}</h5>
                          <p className="mb-1-5">{this.state.selectedTeam.description}</p>
                        </Grid>
                        <Grid item xs={4}>
                          <h5 className="mb-1-5">Members: </h5>
                          <ul className="mb-1-5">
                            {this.state.selectedTeam.members.map((member, i) => (
                              <li key={i}>
                                <p>{member.name ? member.name : ""}</p>
                              </li>
                            ))}
                          </ul>
                        </Grid>
                      </Grid>
                    </Grid>
                  )}
                </Grid>
              </Paper>
            </Container>
            <EditTeam
              open={this.state.isEditOpen}
              handleClickClose={this.handleClickClose}
              selectedItemId={this.state.selectedItemId}
              currentItem={this.state.currentItem}
              addRow={this.addRow}
              updateRow={this.updateRow}
            />
            <Fab
              color="primary"
              aria-label="add"
              className="fab-position"
              onClick={() => this.handleClickOpen(null)}
            >
              <AddIcon />
            </Fab>
          </Fragment>
        )}
        {this.state.isLoading && <BlockElementSpinner />}
      </Fragment>
    );
  }
}
export default TeamsView;
