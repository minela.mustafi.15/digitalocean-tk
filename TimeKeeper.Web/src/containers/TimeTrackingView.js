import React, { Component, Fragment } from "react";
import TableView from "../components/TableView/TableView";
import axios from "axios";
import Config from "../Config";
import EditProject from "../components/EditProject/EditProject";
import Moment from "moment";
import BlockElementSpinner from "../components/BlockElementSpinner/BlockElementSpinner";
import WithCrud from "../hocs/WithCrud";

// unfinished component
class TimeTrackingView extends Component {
  state = {
    title: "Time Tracking",
    backgroundImage: "/images/projects.jpg"
  };

  render() {
    return (
      <Fragment>
        {!this.props.isLoading && (
          <Fragment>
            <TableView
              {...this.state}
              {...this.props}
              handleClickOpen={this.props.handleClickOpen}
              handleClickDelete={this.props.handleClickDelete}
            />
            <EditProject
              open={this.props.isEditOpen}
              handleClickClose={this.props.handleClickClose}
              selectedItemId={this.props.selectedItemId}
              currentItem={this.props.currentItem}
              updateRow={this.props.updateRow}
              addRow={this.props.addRow}
            />
          </Fragment>
        )}
        {this.props.isLoading && <BlockElementSpinner />}
      </Fragment>
    );
  }
}

// The following methods exist as an adjustment to the models received from backend
// Adjusts item to be inserted into table
const adjustTableItem = item => {
  return {
    id: item.id,
    "Project Name": item.name,
    "Customer Name": item.customer.name,
    "Team Name": item.team.name,
    Status: item.status.name
  };
};
// Adjusts item to fill input fields in edit form
const adjustSelectedItem = item => {
  let adjustedItem = Object.assign({}, item);
  adjustedItem.endDate = Moment(item.endDate).format("YYYY-MM-DD");
  adjustedItem.begindate = Moment(item.beginDate).format("YYYY-MM-DD");
  // console.log("item", item);
  return adjustedItem;
};

// Adjusts resource items to fit table columns
const adjustRows = rows => {
  return rows.map(row => {
    return {
      id: row.id,
      "Project Name": row.name,
      "Customer Name": row.customer.name,
      "Team Name": row.team.name,
      Status: row.status.name
    };
  });
};

export default WithCrud(
  TimeTrackingView,
  "projects",
  adjustTableItem,
  adjustSelectedItem,
  adjustRows
);

// componentDidMount() {
//   let head = [
//     "ID",
//     "Employee",
//     "Working Hours",
//     "Business Absence",
//     "Public Holiday",
//     "Vacation",
//     "Sick Days",
//     "Missing Entries"
//   ];
//   let id = 0;
//   function createData(
//     employee,
//     workingHours,
//     businessAbsence,
//     publicHoliday,
//     vacation,
//     sickDays,
//     missingEntries
//   ) {
//     id += 1;
//     return {
//       id,
//       employee,
//       workingHours,
//       businessAbsence,
//       publicHoliday,
//       vacation,
//       sickDays,
//       missingEntries
//     };
//   }
//   const rows = [
//     createData("Peter Severin", 40, 8, 8, 16, 0, 104),
//     createData("Henry Roberton", 32, 0, 8, 0, 0, 136),
//     createData("Olga Ford", 32, 8, 8, 0, 0, 128),
//     createData("Diana Higgins", 160, 0, 8, 0, 8, 0)
//   ];
//   this.setState({
//     table: {
//       rows: rows,
//       head: head,
//       actions: true
//     },
//     isLoading: false
//   });
// }
