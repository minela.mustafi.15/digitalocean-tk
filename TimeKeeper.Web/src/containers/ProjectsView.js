import React, { Component, Fragment } from "react";
import TableView from "../components/TableView/TableView";
import axios from "axios";
import Config from "../Config";
import EditProject from "../components/EditProject/EditProject";
import Moment from "moment";
import BlockElementSpinner from "../components/BlockElementSpinner/BlockElementSpinner";
import WithCrud from "../hocs/WithCrud";

class ProjectsView extends Component {
  state = {
    title: "Projects",
    backgroundImage: "/images/projects.jpg"
  };
  render() {
    return (
      <Fragment>
        {!this.props.isLoading && (
          <Fragment>
            <TableView {...this.state} {...this.props} fab />
            <EditProject open={this.props.isEditOpen} {...this.props} />
          </Fragment>
        )}
        {this.props.isLoading && <BlockElementSpinner />}
      </Fragment>
    );
  }
}

const adjustTableItem = item => {
  return {
    id: item.id,
    "Project Name": item.name,
    "Customer Name": item.customer.name,
    "Team Name": item.team.name,
    Status: item.status.name
  };
};
// Adjusts item to fill input fields in edit form
const adjustSelectedItem = item => {
  let adjustedItem = Object.assign({}, item);
  adjustedItem.endDate = Moment(item.endDate).format("YYYY-MM-DD");
  adjustedItem.begindate = Moment(item.beginDate).format("YYYY-MM-DD");
  // console.log("item", item);
  return adjustedItem;
};

// Adjusts resource items to fit table columns
const adjustRows = rows => {
  return rows.map(row => {
    return {
      id: row.id,
      "Project Name": row.name,
      "Customer Name": row.customer.name,
      "Team Name": row.team.name,
      Status: row.status.name
    };
  });
};

export default WithCrud(ProjectsView, "projects", adjustTableItem, adjustSelectedItem, adjustRows);
