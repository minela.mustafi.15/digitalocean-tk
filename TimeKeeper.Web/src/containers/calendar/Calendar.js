import React, { useState, useEffect, Fragment } from "react";

import Calendar from "react-calendar";
import moment from "moment";
import { loadCalendar, rldCal } from "../../store/actions/calendarActions";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import CalendarModal from "../../components/calendarModal/CalendarModal";
import BlockElementSpinner from "../../components/BlockElementSpinner/BlockElementSpinner";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import Config from "../../Config";

import "./Calendar.css";

function CalendarDisplay(props) {
  const [date, setDate] = useState(new Date(2018, 6, 6, 10, 33, 30, 0));
  const [year, setYear] = useState(moment(date).format("YYYY"));
  const [month, setMonth] = useState(moment(date).format("MM"));
  const [day, setDay] = useState(moment(date).format("DD"));
  const [employeeId] = useState(props.user.id);
  const [projects, setProjects] = useState([]);
  const [selectedTab, setSelectedTab] = useState(0);

  useEffect(() => {
    axios
      .get(Config.apiUrl + "projects", Config.authHeader)
      .then(res => {
        setProjects(res.data);
      })
      .catch(err => {
        console.log("project err", err);
        alert(err);
      });
    console.log("loadCalendar");
    props.loadCalendar(employeeId, year, month);

    if (props.calendarMonth) {
      const selectedYear = moment(date).format("YYYY");
      const selectedMonth = moment(date).format("MM");
      const selectedDay = moment(date).format("DD");
      setDate(date);
      setYear(selectedYear);
      setMonth(selectedMonth);
      setDay(selectedDay);
    }
    props.rldCal(false);
  }, [props.reload]);
  const handleSelectedTab = (event, newValue) => {
    setSelectedTab(newValue);
  };
  const changeData = selectedDate => {
    const selectedYear = moment(selectedDate).format("YYYY");
    const selectedMonth = moment(selectedDate).format("MM");
    const selectedDay = moment(selectedDate).format("DD");
    if (selectedYear !== year || selectedMonth !== month) {
      props.loadCalendar(employeeId, selectedYear, selectedMonth);
      setDate(selectedDate);
      setYear(selectedYear);
      setMonth(selectedMonth);
      setDay(selectedDay);
    } else if (selectedDay !== day) {
      setDate(selectedDate);
      setDay(selectedDay);
    }
  };
  function onChange(date) {
    changeData(date);
  }

  function a11yProps(index) {
    return {
      id: `tab-${index}`,
      "aria-controls": `tabpanel-${index}`
    };
  }

  return (
    // <Fragment>
    //   {!props.isloading && props.calendarMonth.map((x, i) => <div key={i}>{x.date}</div>)}
    // </Fragment>
    <Grid container spacing={2}>
      <Grid item xs={5}>
        <Calendar onChange={onChange} value={date} />
      </Grid>
      <Grid item xs={7}>
        {console.log("cal month", props.calendarMonth)}
        {props.calendarMonth.length > 0 &&
        moment(props.calendarMonth[day - 1].date).format("YYYY-MM-DD") ===
          moment(date).format("YYYY-MM-DD") ? (
          <div>
            {console.log("in cal modal")}
            <CalendarModal
              selectedTab={selectedTab}
              handleSelectedTab={handleSelectedTab}
              a11yProps={a11yProps}
              calendarMonth={props.calendarMonth}
              projects={projects}
              day={props.calendarMonth[day - 1]}
            />
          </div>
        ) : (
          <Fragment>
            {props.calendarMonth.length > 0
              ? console.log(
                  moment(props.calendarMonth[day - 1].date).format("YYYY-MM-DD") ===
                    moment(date).format("YYYY-MM-DD")
                )
              : console.log("nema")}
            <BlockElementSpinner />
          </Fragment>
        )}
      </Grid>
    </Grid>
  );
}

const mapStateToProps = state => {
  return {
    loading: state.calendarMonth.loading,
    calendarMonth: state.calendarMonth.data,
    reload: state.calendarMonth.reload,
    user: state.authData.user
  };
};

export default connect(mapStateToProps, { loadCalendar, rldCal })(withRouter(CalendarDisplay));
