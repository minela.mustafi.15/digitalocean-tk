import React, { Component, Fragment } from "react";
import TableView from "../components/TableView/TableView";
import EditEmployee from "../components/EditEmployee/EditEmployee";
import axios from "axios";
import Config from "../Config";
import Moment from "moment";
import { connect } from "react-redux";
import { compose } from "redux";
import { getEmployees } from "../store/actions/employeesActions";
import BlockElementSpinner from "../components/BlockElementSpinner/BlockElementSpinner";
import WithCrud from "../hocs/WithCrud";

class EmployeesView extends Component {
  state = {
    title: "Employees",
    backgroundImage: "/images/employees.jpg"
  };
  componentDidMount() {
    // if (this.props.employees && this.props.employees.table.rows.length === 0) {
    //   // console.log("fetching employees");
    //   this.props.getEmployees();
    // }
  }
  componentDidUpdate() {
    console.log("rows: ", this.props.table.rows);
  }
  render() {
    // console.log(this.props);
    return (
      <Fragment>
        {!this.props.isLoading && (
          <Fragment>
            <TableView {...this.state} {...this.props} fab />
            <EditEmployee open={this.props.isEditOpen} {...this.props} />
          </Fragment>
        )}
        {this.props.isLoading && <BlockElementSpinner />}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    employees: state.employees
  };
};
const adjustTableItem = item => {
  return {
    id: item.id,
    firstName: item.firstName,
    lastName: item.lastName,
    age: item.age ? item.age : item.birthDate,
    email: item.email,
    phone: item.phone
  };
};
// Adjusts item to fill input fields in edit form
const adjustSelectedItem = item => {
  let adjustedItem = Object.assign({}, item);
  adjustedItem.birthDate = Moment(item.birthDate).format("YYYY-MM-DD");
  adjustedItem.beginDate = Moment(item.beginDate).format("YYYY-MM-DD");
  adjustedItem.endDate = Moment(item.endDate).format("YYYY-MM-DD");
  adjustedItem.position = {
    id: item.position.id,
    name: item.position.name
  };
  adjustedItem.status = {
    id: item.status.id,
    name: item.status.name
  };
  // console.log("item", item);
  return adjustedItem;
};

// Adjusts resource items to fit table columns
const adjustRows = rows => {
  return rows.map(row => {
    return {
      id: row.id,
      firstName: row.firstName,
      lastName: row.lastName,
      age: row.age ? row.age : row.birthDate,
      email: row.email,
      phone: row.phone
    };
  });
};

export default WithCrud(
  // first argument in connect should be null if no state
  connect(mapStateToProps, { getEmployees })(EmployeesView),
  "employees",
  adjustTableItem,
  adjustSelectedItem,
  adjustRows
);
