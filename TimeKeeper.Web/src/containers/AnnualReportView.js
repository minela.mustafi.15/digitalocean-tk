import React, { Fragment, useState, useEffect } from "react";
import TableView from "../components/TableView/TableView";
import axios from "axios";
import Config from "../Config";
import EditCustomer from "../components/EditCustomer/EditCustomer";
import BlockElementSpinner from "../components/BlockElementSpinner/BlockElementSpinner";
import WithCrud from "../hocs/WithCrud";
import { MenuItem, TextField } from "@material-ui/core";
import { getAnnualReport, startLoading } from "../store/actions/annualReportActions";
import { connect } from "react-redux";

function AnnualReport(props) {
  const [selectedYear, setSelectedYear] = useState(2019);
  const title = "Annual Overview";
  const backgroundImage = "/images/customers.jpg";

  useEffect(() => {
    props.getAnnualReport(selectedYear);
  }, [selectedYear]);

  const handleSelectedYear = e => {
    props.startLoading();
    setSelectedYear(e.target.value);
  };

  return (
    <Fragment>
      {!props.annualReport.isLoading && (
        <Fragment>
          <TableView
            title={title}
            backgroundImage={backgroundImage}
            table={props.annualReport.table}
            selectedYear={selectedYear}
            handleSelectedYear={handleSelectedYear}
            sumTotals={true}
            hasOptions
          />
        </Fragment>
      )}
      {props.annualReport.isLoading && <BlockElementSpinner />}
    </Fragment>
  );
}

const mapStateToProps = state => {
  return {
    annualReport: state.annualReport
  };
};

export default connect(mapStateToProps, { getAnnualReport, startLoading })(AnnualReport);
