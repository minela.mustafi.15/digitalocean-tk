import React from "react";
import Hero from "../components/Static/Hero";
import TableTK from "../components/Table/Table";
import EditWorkingHours from "../components/EditWorkingHours/EditWorkingHours";

class Dashboard extends React.Component {
  render() {
    let hero = {
      id: "dashboard",
      title: "Welcome",
      description: "Lorem ipsum text has been used since the last millenium",
      backgroundImage: "/images/bricks.jpg"
    };
    // let head = ["ID", "Dessert (100g serving)", "Calories", "Fat", "Carbs", "Protein"];
    // let id = 0;
    // function createData(name, calories, fat, carbs, protein) {
    //   id += 1;
    //   return { id, name, calories, fat, carbs, protein };
    // }
    // const rows = [
    //   createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
    //   createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
    //   createData("Eclair", 262, 16.0, 24, 6.0),
    //   createData("Cupcake", 305, 3.7, 67, 4.3),
    //   createData("Gingerbread", 356, 16.0, 49, 3.9)
    // ];
    return (
      <React.Fragment>
        <Hero {...hero} />
        {/* <TableTK head={head} rows={rows} /> */}
      </React.Fragment>
    );
  }
}
export default Dashboard;
