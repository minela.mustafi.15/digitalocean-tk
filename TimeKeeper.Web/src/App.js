import React, { Fragment, Component } from "react";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import AuthService from "./auth/AuthService";
import { AuthProvider } from "./auth/AuthProvider";
import { PrivateRoute } from "./auth/PrivateRoute";
import { authPersist, authFinish } from "./store/actions/authActions";
import routes from "./routes";
import Config from "./Config";

class App extends Component {
  componentWillMount() {
    // this.handleLogin();
    if (!this.props.token && localStorage.getItem("token")) this.props.authPersist();
    // console.log("history", this.props.history);
    console.log("app.js config", Config.authHeader);
  }
  componentDidUpdate(prevProps) {
    const { user, history, hasLoggedIn, authFinish } = this.props;
    // console.log("diduptade app");
    // console.log("hasloggedin", hasLoggedIn);
    if (!user) history.push("/");

    // When there is no flag, user will be redirected to dashboard on page reload
    if (hasLoggedIn) {
      authFinish();
      history.push("/dashboard");
    }
  }
  shouldComponentUpdate(nextProps) {
    return this.props.user !== nextProps.user;
  }

  render() {
    return (
      // <AuthProvider>
      <div className="flex flex-column h-100">
        <Header setLoginState={this.setLoginState} unsetLoginState={this.unsetLoginState} />
        <main className="flex-grow-1 flex flex-column">
          <Switch>
            {routes.map(({ path, privateRoute, exact, component }, i) => {
              return privateRoute ? (
                <PrivateRoute exact={exact} path={path} component={component} key={i} />
              ) : (
                <Route exact={exact} path={path} component={component} key={i} />
              );
            })}
          </Switch>
        </main>
        <Footer />
      </div>
      //  </AuthProvider>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.authData.user,
    token: state.authData.token,
    hasLoggedIn: state.authData.hasLoggedIn
  };
};

export default connect(mapStateToProps, { authPersist, authFinish })(withRouter(App));
{
  /* <Route exact path="/pie">
              <PieChart />
            </Route>
            <Route exact path="/employees">
              <EmployeesView />
            </Route>
            <Route exact path="/customers">
              <CustomersView />
            </Route>
            <Route exact path="/projects">
              <ProjectsView />
            </Route>
            <Route exact path="/teams">
              <TeamsView />
            </Route>
            <Route exact path="/time-tracking">
              <TimeTrackingView />
            </Route>
            <Route exact path="/personal-report">
              <EditWorkingHours />
            </Route>
            <Route path="/auth-callback" component={Callback} />
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/logout/callback" component={LogoutCallback} />
            <Route exact path="/silentrenew" component={SilentRenew} />
            <PrivateRoute path="/dashboard" component={Dashboard} />
            <Route path="/">
              <LandingPage />
            </Route> */
}
// state = {
// isLoggedIn: false,
// currentUser: localStorage.getItem("currentUser")
//   ? JSON.parse(localStorage.getItem("currentUser"))
//   : null,
// token: localStorage.getItem("access_token")
//   ? "Bearer " + localStorage.getItem("access_token")
//   : null
// };
// basicLogin = credentials => {
//   axios
//     .post(Config.loginUrl, credentials)
//     .then(res => {
//       Config.authHeader.headers.Authorization = "Bearer " + res.data.token;
//       Config.token = res.data.token;
//       localStorage.setItem("currentUser", JSON.stringify(res.data.user));
//       localStorage.setItem("access_token", JSON.stringify(res.data.token));
//       window.location.replace("/dashboard");
//     })
//     .catch(err => {
//       console.log(err);
//     });
// };
// basicLogout = () => {
//   localStorage.removeItem("currentUser");
//   localStorage.removeItem("access_token");
//   window.location.replace("/");
// };
// setLoginState = () => {
//   this.setState({ isLoggedIn: true });
// };
// unsetLoginState = () => {
//   this.setState({ isLoggedIn: false });
//   const authService = new AuthService();
//   authService.logout();
// };
// componentDidMount() {
//   if (localStorage.getItem("access_token")) {
//     Config.token = "Bearer " + localStorage.getItem("access_token");
//     Config.authHeader.headers.Authorization = "Bearer " + localStorage.getItem("access_token");
//   }
//   console.log("Config", Config);
// }
{
  /* <Route exact path="/employees">
              <EmployeesView />
            </Route>
            <Route exact path="/customers">
              <CustomersView />
            </Route>
            <Route exact path="/projects">
              <ProjectsView />
            </Route>
            <Route exact path="/teams">
              <TeamsView />
            </Route>
            <Route exact path="/time-tracking">
              <TimeTrackingView />
            </Route>
            <Route exact path="/personal-report">
              <EditWorkingHours />
            </Route>
            <Route path="/auth-callback" component={Callback} />
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/logout/callback" component={LogoutCallback} />
            <Route exact path="/silentrenew" component={SilentRenew} />
            <PrivateRoute path="/dashboard" component={Dashboard} />
            <Route path="/">
              <LandingPage />
            </Route> */
}
