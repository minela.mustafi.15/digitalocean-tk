//const authUrl = "https://localhost:44300";
const authUrl = "http://localhost:8000";

export const IDENTITY_CONFIG = {
  authority: authUrl,
  client_id: "tk2019",
  redirect_uri: "http://localhost:3000/auth-callback",
  post_logout_redirect_uri: "http://localhost:3000/logout/callback",
  response_type: "id_token token",
  scope: "openid profile roles address teams timekeeper",
  filterProtocolClaims: true,
  loadUserInfo: true,
  automaticSilentRenew: true,
  silent_redirect_uri: "http://localhost:3000/silentrenew"
};

export const METADATA_OIDC = {
  issuer: authUrl,
  jwks_uri: authUrl + "/.well-known/openid-configuration/jwks",
  authorization_endpoint: authUrl + "/connect/authorize",
  token_endpoint: authUrl + "/connect/token",
  userinfo_endpoint: authUrl + "/connect/userinfo",
  end_session_endpoint: authUrl + "/connect/endsession",
  check_session_iframe: authUrl + "/connect/checksession",
  revocation_endpoint: authUrl + "/connect/revocation",
  introspection_endpoint: authUrl + "/connect/introspect"
};
