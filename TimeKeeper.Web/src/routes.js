import EmployeesView from "./containers/EmployeesView";
import CustomersView from "./containers/CustomersView";
import ProjectsView from "./containers/ProjectsView";
import LandingPage from "./containers/LandingPage/LandingPage";
import Dashboard from "./containers/Dashboard";
import TeamsView from "./containers/TeamsView/TeamsView";
import TimeTrackingView from "./containers/TimeTrackingView";
import EditWorkingHours from "./components/EditWorkingHours/EditWorkingHours";
import PieChart from "./components/PieChart/PieChart";
import BarChart from "./components/BarChart/BarChart";
import AnnualReportView from "./containers/AnnualReportView";
import MonthlyReportView from "./containers/MonthlyReportView";
import CompanyDashboard from "./containers/CompanyDashboard/CompanyDashboard";
import TeamDashboard from "./containers/TeamDashboard/TeamDashboard";
import Calendar from "./containers/calendar/Calendar";

// auth

import Callback from "./auth/Callback";
import { Logout } from "./auth/Logout";
import { LogoutCallback } from "./auth/LogoutCallback";
import { SilentRenew } from "./auth/SilentRenew";

export default [
  {
    path: "/employees",
    privateRoute: false,
    exact: true,
    component: EmployeesView
  },
  {
    path: "/customers",
    privateRoute: false,
    exact: true,
    component: CustomersView
  },
  {
    path: "/projects",
    privateRoute: false,
    exact: true,
    component: ProjectsView
  },
  {
    path: "/teams",
    privateRoute: false,
    exact: true,
    component: TeamsView
  },
  {
    path: "/time-tracking",
    privateRoute: false,
    exact: true,
    component: TimeTrackingView
  },
  {
    path: "/personal-report",
    privateRoute: false,
    exact: true,
    component: EditWorkingHours
  },
  {
    path: "/auth-callback",
    privateRoute: false,
    exact: true,
    component: Callback
  },
  {
    path: "/logout",
    privateRoute: false,
    exact: true,
    component: Logout
  },
  {
    path: "/logout/callback",
    privateRoute: false,
    exact: true,
    component: LogoutCallback
  },
  {
    path: "/silentrenew",
    privateRoute: false,
    exact: true,
    component: SilentRenew
  },
  {
    path: "/dashboard",
    privateRoute: false,
    exact: true,
    component: Dashboard
  },
  {
    path: "/calendar",
    privateRoute: false,
    exact: true,
    component: Calendar
  },
  {
    path: "/dashboards/company",
    privateRoute: false,
    exact: true,
    component: CompanyDashboard
  },
  {
    path: "/dashboards/team",
    privateRoute: false,
    exact: true,
    component: TeamDashboard
  },
  {
    path: "/bar",
    privateRoute: false,
    exact: true,
    component: BarChart
  },
  {
    path: "/reports/annual",
    privateRoute: false,
    exact: true,
    component: AnnualReportView
  },
  {
    path: "/reports/monthly",
    privateRoute: false,
    exact: true,
    component: MonthlyReportView
  },
  {
    path: "/",
    privateRoute: false,
    exact: true,
    component: LandingPage
  }
];
