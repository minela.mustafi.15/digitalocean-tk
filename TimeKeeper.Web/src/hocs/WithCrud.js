import React, { Component, Fragment } from "react";
import axios from "axios";
import Config from "../Config";
// import { connect } from "react-redux";
// import { getEmployees } from "../store/actions/employeesActions";

let resourceName = null;

function WithCrud(PassedComponent, resource, adjustTableItem, adjustSelectedItem, adjustRows) {
  // resourceName = resource;
  // resource: endpoint path
  // adjustItem: get item by id then adjust it for inputs
  // adjustList: get list of items then adjust it for table
  return class extends Component {
    state = {
      table: {
        head: [],
        rows: [],
        actions: true
      },
      isEditOpen: false,
      selectedItemId: null,
      currentItem: {},
      isLoading: true
    };
    sortById = (a, b) => {
      return a.id - b.id;
    };
    methods = {
      addRow: item => {
        let rows = this.state.table.rows;
        let itemToInsert = adjustTableItem(item);
        // itemToInsert: passed item is adjusted for table
        rows.push(itemToInsert);
        console.log("addRow: ", rows);
        this.setState({
          table: {
            ...this.state.table,
            rows: rows.sort(this.sortById)
          }
        });
      },
      updateRow: item => {
        let rows = this.state.table.rows;
        let index = rows.findIndex(x => x.id === item.id);
        let itemToInsert = adjustTableItem(item);
        // itemToInsert: passed item is adjusted for table
        rows.splice(index, 1, itemToInsert);
        console.log("updateRow: ", rows);
        this.setState({
          table: {
            ...this.state.table,
            rows: rows.sort(this.sortById)
          }
        });
      },
      deleteRow: itemId => {
        let rows = this.state.table.rows;
        let index = rows.findIndex(x => x.id === itemId);
        rows.splice(index, 1);
        console.log("deleteRow: ", rows);
        this.setState({
          table: {
            ...this.state.table,
            rows: rows.sort(this.sortById)
          }
        });
      },
      handleClickDelete: itemId => {
        // console.log("delete: ", Config.apiUrl + resource + "/" + itemId);
        axios
          .delete(Config.apiUrl + resource + "/" + itemId, Config.authHeader)
          .then(res => {
            console.log(res);
            this.methods.deleteRow(itemId);
            alert("Deleted");
          })
          .catch(err => {
            console.log(err);
          });
      },
      // console.log(itemId);
      handleClickOpen: itemId => {
        if (itemId !== null) {
          axios
            .get(Config.apiUrl + resource + "/" + itemId, Config.authHeader)
            .then(res => {
              let item = adjustSelectedItem(res.data);
              // console.log('adjusted selected item: ', item);
              this.setState({
                isEditOpen: true,
                selectedItemId: itemId,
                currentItem: item
              });
            })
            .catch(err => {
              alert(err);
              console.log(err);
            });
        } else {
          this.setState({
            isEditOpen: true,
            selectedItemId: null,
            currentItem: {}
          });
        }
        // console.log(itemId);
      },
      handleClickClose: () => {
        this.setState({ isEditOpen: false, selectedItemId: null, currentItem: {} });
      }
    };

    componentDidMount() {
      // Config.authHeader.headers.Authorization = JSON.parse(localStorage.getItem("token"));
      axios
        .get(Config.apiUrl + resource, Config.authHeader)
        .then(res => {
          let data = adjustRows(res.data);
          // console.log('mapped', data);
          this.setState({
            table: {
              rows: data.sort(this.sortById),
              head: Object.keys(data[0]),
              actions: true
            },
            isLoading: false
          });
        })
        .catch(err => {
          alert(err);
          console.log(err);
        });
    }
    render() {
      return <PassedComponent {...this.state} {...this.methods} />;
    }
  };
}

// const mapStateToProps = state => {
//   return {
//     [resourceName]: state[resourceName]
//   };
// };

// const mapDispatchToProps = () => {
//   let getResource = "get" + resourceName.charAt(0).toUpperCase() + resourceName.substring(1);
//   console.log("getResource", window[getResource]);
//   return {
//     [getResource]: window[getResource]
//   };
// };

export default WithCrud;
