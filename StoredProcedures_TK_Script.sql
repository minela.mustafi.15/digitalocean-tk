---------------------Admin Dashboard----------------------------------------

CREATE OR REPLACE FUNCTION admindashboard(y integer, m integer)
 RETURNS TABLE(employeeid integer, projectid integer, workinghours numeric)
 LANGUAGE sql
AS 'select e."Id", t."ProjectId", sum(t."Hours") as "WorkingHours"
from "Employees" as e join "Calendar" as c on e."Id"=c."EmployeeId"
	join "Tasks" as t on c."Id"=t."DayId"
where extract(year from c."Date") = y
	and extract(month from c."Date") = m
group by e."Id", t."ProjectId"'
;

--------------------------------Admin PTOs-------------------------------

CREATE OR REPLACE FUNCTION adminptohours(y integer, m integer)
 RETURNS TABLE(teamid integer, teamname text, ptohours numeric)
 LANGUAGE sql
AS 'select t."Id", t."Name", (count(c."Id") * 8)::dec as "PTO"
from "Calendar" as c join "Employees" as e on c."EmployeeId"=e."Id"
	join "Members" as me on e."Id"=me."EmployeeId"
	join "Teams" as t on me."TeamId"=t."Id"
where extract(year from c."Date") = y
	and extract(month from c."Date") = m
	and c."TypeId" <> 1
group by t."Id"'
;

----------------------------Annual Report-------------------------------------------

CREATE OR REPLACE FUNCTION annualreport(y integer)
 RETURNS TABLE(id integer, name text, month integer, hours numeric)
 LANGUAGE sql
AS 'select p."Id", p."Name", extract(month from c."Date")::int, sum(t."Hours")::dec
from "Calendar" as c join "Tasks" as t on t."DayId" = c."Id" join "Projects" as p on t."ProjectId" = p."Id"
where extract(year from c."Date")=y and c."Deleted"=false and p."Deleted"=false and t."Deleted"=false
group by p."Id", p."Name", extract(month from c."Date")
order by p."Id"'
;

---------------------------------------------Company Overtime---------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.companyovertimehours(y integer, m integer)
 RETURNS TABLE(employeeid integer, employeefirstname text, employeelastname text, overtimehours numeric)
 LANGUAGE sql
AS 'select  res."EmployeeId", res."FirstName", res."LastName", sum("Overtime") as "Overtime" from
(select e."Id" as "EmployeeId", e."FirstName", e."LastName", c."Id" as "DayId", c."Date", coalesce(sum(t."Hours"), 0) as "WorkingHours",
case 	when (c."TypeId" = 1) and (extract(isodow from c."Date") >= 6) then (coalesce(sum(t."Hours"), 0))
		when (c."TypeId" = 1) and (extract(isodow from c."Date") < 6) and (coalesce(sum(t."Hours"), 0) > 8) then (coalesce(sum(t."Hours"), 0) - 8)
		else 0 end
		as "Overtime"
from	"Employees" as e join "Calendar" as c on e."Id" = c."EmployeeId"
		join "Tasks" as t on c."Id" = t."DayId"
where	extract(year from c."Date") = y
		and extract(month from c."Date") = m		
group by e."Id", c."Id") as res
group by res."EmployeeId", res."FirstName", res."LastName"
order by res."EmployeeId"'
;

-------------------------------------------------Company working hours----------------------------------------------------

CREATE OR REPLACE FUNCTION public.companyworkinghoursdata(y integer, m integer)
 RETURNS TABLE(employeeid integer, teamid integer, teamname text, roleid integer, rolename text, rolehourlyprice numeric, rolemonthlyprice numeric, projectid integer, projectname text, projectamount numeric, projectpricingid integer, projectpricingname text, workinghours numeric)
 LANGUAGE sql
AS 'select 	res."EmployeeId", res."TeamId", res."TeamName",
		res."RoleId", res."RoleName", res."RoleHourlyPrice" , res."RoleMonthlyPrice",
		res."ProjectId", res."ProjectName", res."ProjectAmount",
		res."ProjectPricingId",
		res."ProjectPricingName",
		sum(res."Hours") as "WorkingHours" from
(select distinct on (t."Id") 
		r."Id" as "RoleId", r."Name" as "RoleName", r."HourlyPrice" as "RoleHourlyPrice", r."MonthlyPrice" as "RoleMonthlyPrice",
		e."Id" as "EmployeeId", te."Id" as "TeamId", te."Name" as "TeamName",
		p."Id" as "ProjectId", p."Name" as "ProjectName", p."Ammount" as "ProjectAmount",
		p."PricingId" as "ProjectPricingId", ps."Value" as "ProjectPricingName",
		t."Hours" as "Hours"
from	
		"Roles" as r join "Members" as me on r."Id" = me."RoleId" 
		join "Employees" as e on e."Id" = me."EmployeeId"
		join "Calendar" as c on e."Id" = c."EmployeeId"
		join "Tasks" as t on c."Id" = t."DayId"
		join "Projects" as p on t."ProjectId" = p."Id"
		join "PricingTypes" as ps on ps."Id" = p."PricingId"
		join "Teams" as te on te."Id" = p."TeamId"
where	extract(year from c."Date") = y
		and extract(month from c."Date") = m) as res
group by 	res."RoleId", res."RoleName", res."RoleHourlyPrice", res."RoleMonthlyPrice", 
			res."EmployeeId", res."TeamId", res."TeamName",
			res."ProjectId", res."ProjectName", res."ProjectAmount",
			res."ProjectPricingId", res."ProjectPricingName"
order by res."EmployeeId"'
;

-------------------------------------Count Projects--------------------------------------

CREATE OR REPLACE FUNCTION public.countprojects(tid integer, y integer, m integer)
 RETURNS TABLE(projectid integer)
 LANGUAGE sql
AS 'select p."Id"
from    "Projects" as p 
        join "Teams" as te on p."TeamId" = te."Id"
        join "Members" as me on te."Id" = me."TeamId" 
        join "Employees" as e on e."Id" = me."EmployeeId"
        join "Calendar" as c on e."Id" = c."EmployeeId"
        join "Tasks" as a on c."Id" = a."DayId"
where   extract(year from c."Date") = y
        and extract(month from c."Date") = m
        and te."Id" = tId
group by p."Id"'
;

---------------------------------------Date Month---------------------------------------------

CREATE OR REPLACE FUNCTION datemonth(tid integer, y integer, m integer)
 RETURNS TABLE(eid integer, ename text, n numeric)
 LANGUAGE sql
AS 'select me."Id", e."FirstName" || '' '' || e."LastName" as "EmployeeName", count(c."Date")::dec
from "Calendar" as c
      join "Employees" as e on c."EmployeeId" = e."Id"
      join "Members" as me on me."EmployeeId" = e."Id"
      join "Teams" as t on t."Id" = me."TeamId"
where extract(year from c."Date") = y
        and extract(month from c."Date") = m
        and t."Id" = tId
        and 
        EXTRACT(ISODOW FROM c."Date") IN (1, 2, 3, 4, 5)
 group by me."Id", e."FirstName", e."LastName"'
;

-----------------------------------Employee Hours By Type of Day------------------------

CREATE OR REPLACE FUNCTION employeehoursbydaytype(y integer, m integer)
 RETURNS TABLE(teamid integer, employeeid integer, employeefirstname text, employeelastname text, daytypeid integer, daytypename text, dayhours numeric)
 LANGUAGE sql
AS 'select  res."TeamId", res."EmployeeId", res."EmployeeFirstName", res."EmployeeLastName", 
        res."DayTypeId", res."DayTypeName", coalesce(sum(res."DayHours"), 0) as "DayHours"
from
(select  coalesce(p."TeamId", 0) as "TeamId", 
        e."Id" as "EmployeeId", e."FirstName" as "EmployeeFirstName", e."LastName" as "EmployeeLastName",
        dt."Id" as "DayTypeId", dt."Value" as "DayTypeName",
        c."Id" as "DayId",
        ta."Id" as "TaskId",
case    when dt."Id" = 1 then ta."Hours"
        when dt."Id" <> 1 then 8
        else 0 end
        as "DayHours"
from    "Employees" as e
        join "Calendar" as c on e."Id" = c."EmployeeId"
        join "DayTypes" as dt on c."TypeId" = dt."Id"
        left join "Tasks" as ta on c."Id" = ta."DayId"
        left join "Projects" as p on p."Id" = ta."ProjectId"
where   extract(year from c."Date") = y
        and extract(month from c."Date") = m
group by p."TeamId", dt."Id", e."Id", c."Id", ta."Id") as res
group by res."TeamId", res."EmployeeId", res."EmployeeFirstName", res."EmployeeLastName", 
        res."DayTypeId", res."DayTypeName"
order by res."EmployeeId"'
;

------------------------------Employee Projects Count------------------------------------

CREATE OR REPLACE FUNCTION public.employeeprojectscount(y integer, m integer)
 RETURNS TABLE(employeeid integer, projectid integer)
 LANGUAGE sql
AS 'select e."Id", t."ProjectId"
from "Employees" as e join "Calendar" as c on e."Id"=c."EmployeeId"
	join "Tasks" as t on c."Id"=t."DayId"
where extract(year from c."Date") = 2017
	and extract(month from c."Date") = 12
group by e."Id", t."ProjectId"'
;

------------------------------------Get Member Overtime-------------------------------------

CREATE OR REPLACE FUNCTION public.getmemberovertimehours(tid integer, y integer, m integer)
 RETURNS TABLE(memberid integer, overtime numeric)
 LANGUAGE sql
AS 'select  res."MemberId", sum(res."Overtime") as "Overtime"
from (select  me."Id" as "MemberId", sum(a."Hours" - 8) as "Overtime"
from    "Teams" as te join "Members" as me on te."Id" = me."TeamId"
        join "Employees" as e on me."EmployeeId" = e."Id"
        join "Calendar" as c on e."Id" = c."EmployeeId"
        join "Tasks" as a on c."Id" = a."DayId"
where   extract(year from c."Date") = y
        and extract(month from c."Date") = m
        and extract(isodow from c."Date") < 6  
        and a."Hours" > 8
        and te."Id" = tId
group by me."Id"
union 
select  me."Id" as "MemberId", sum(a."Hours") as "Overtime"
from    "Teams" as te join "Members" as me on te."Id" = me."TeamId"
        join "Employees" as e on me."EmployeeId" = e."Id"
        join "Calendar" as c on e."Id" = c."EmployeeId"
        join "Tasks" as a on c."Id" = a."DayId"
where   extract(year from c."Date") = y
        and extract(month from c."Date") = m
        and extract(isodow from c."Date") >= 6
        and te."Id" = tId
group by me."Id") as res
group by res."MemberId"'
;

---------------------------------Monthly Report---------------------------------------------------------

CREATE OR REPLACE FUNCTION monthlyreport(y integer, m integer)
 RETURNS TABLE(empid integer, empname text, projid integer, projname text, hours numeric)
 LANGUAGE sql
AS 'select e."Id", e."FirstName" || '' '' || e."LastName", p."Id", p."Name", sum(t."Hours")::dec
from "Employees" as e join "Calendar" as c on e."Id"=c."EmployeeId" 
	join "Tasks" as t on c."Id"=t."DayId" 
	join "Projects" as p on p."Id"=t."ProjectId"
where e."Deleted"=false and p."Deleted"=false and c."Deleted"=false and t."Deleted"=false
	and extract(year from c."Date")=y and extract(month from c."Date")=m
group by e."Id", e."FirstName", e."LastName", p."Id", p."Name"
order by e."Id"'
;

------------------------------------------Monthly Total--------------------------------------------

CREATE OR REPLACE FUNCTION monthlytotal(empid integer, y integer, m integer)
 RETURNS numeric
 LANGUAGE sql
AS 'select sum(t."Hours")::decimal as monthlyTotal
from "Employees" as e
    join "Calendar" as c on c."EmployeeId"=e."Id"
    join "Tasks" as t on t."DayId"=c."Id"
where
extract(year from c."Date") = y
    and extract(month from c."Date") = m
    and e."Id"=empId'
;

-----------------------------------Personal Dashboard---------------------------------------

CREATE OR REPLACE FUNCTION personaldashboard(empid integer, y integer, m integer)
 RETURNS TABLE(empid integer, empname text, workingmonthly numeric, workingyearly numeric, sickmonthly integer, sickyearly integer)
 LANGUAGE sql
AS 'select distinct e."Id", e."FirstName" || '' '' || e."LastName" as "Name",
coalesce(workingMonthly(empId, y, m),0)::decimal as "WorkingMonthly",
coalesce(workingYearly(empId, y),0)::decimal as "WorkingYearly",
coalesce(sickMonthly(empId, y, m),0)::int as "SickMonthly",
coalesce(sickYearly(empId, y),0)::int as "SickYearly"
from "Employees" as e
left outer join "Calendar" as c on c."EmployeeId"=e."Id"
where e."Id"=empId'
;
--------------------------------------Personal utilization-----------------------------------------------

CREATE OR REPLACE FUNCTION personalutilization(empid integer, y integer, m integer)
 RETURNS TABLE(empid integer, empname text, workingmonthly numeric, workingyearly numeric, sickmonthly integer, sickyearly integer)
 LANGUAGE sql
AS 'select distinct e."Id", e."FirstName" || '' '' || e."LastName" as "Name",
workingMonthly(empId, y, m)::decimal as "WorkingMonthly",
workingYearly(empId, y)::decimal as "WorkingYearly",
sickMonthly(empId, y, m)::int as "SickMonthly",
sickYearly(empId, y)::int as "SickYearly"
from "Employees" as e
join "Calendar" as c on c."EmployeeId"=e."Id"
where e."Id"=empId'
;

-------------------------------------Project History---------------------------------

CREATE OR REPLACE FUNCTION projecthistory(y integer)
 RETURNS TABLE(empid integer, empname text, hours numeric, year integer)
 LANGUAGE sql
AS 'select e."Id", e."FirstName" || '' '' || e."LastName", sum(t."Hours")::decimal, extract(year from c."Date")::integer 
from "Employees" as e join "Calendar" as c on e."Id"=c."EmployeeId" join "Tasks" as t on c."Id"=t."DayId" join "Projects" as p on p."Id"=t."ProjectId"
where p."Id"=y and e."Deleted"=false and c."Deleted"=false and t."Deleted"=false and p."Deleted"=false
group by e."Id", e."FirstName", e."LastName", extract(year from c."Date")
order by e."Id"'
;

-------------------------------Sick By Months------------------------------------------

CREATE OR REPLACE FUNCTION sickbymonths(empid integer, y integer)
 RETURNS TABLE(m integer)
 LANGUAGE sql
AS 'select extract(month from c."Date")::int
from "Calendar" as c
join "Employees" as e on c."EmployeeId"=e."Id"
where extract(year from c."Date") = y
      and e."Id"=empId and c."TypeId"=5
group by extract(month from c."Date")'
;

------------------------------------------Sick Monthly-----------------------------------

CREATE OR REPLACE FUNCTION sickmonthly(empid integer, y integer, m integer)
 RETURNS integer
 LANGUAGE sql
AS 'select count(c."Id")::int
from "Calendar" as c
join "Employees" as e on c."EmployeeId"=e."Id"
where extract(year from c."Date") = y
      and extract(month from c."Date") = m
      and e."Id"=empId and c."TypeId"=5'
;

---------------------------------Sick yearly------------------------------------

CREATE OR REPLACE FUNCTION sickyearly(empid integer, y integer)
 RETURNS integer
 LANGUAGE sql
AS 'select count(c."Id")::int
from "Calendar" as c
join "Employees" as e on c."EmployeeId"=e."Id"
where extract(year from c."Date") = y
      and e."Id"=empId and c."TypeId"=5'
;

--------------------------------Team Dashboard-------------------------------------

CREATE OR REPLACE FUNCTION public.teamdashboard(tid integer, y integer, m integer)
 RETURNS TABLE(employeeid integer, membername text, workinghours numeric)
 LANGUAGE sql
AS 'select res."EmployeeId", res."EmployeeName", sum(res."Hours")::dec from
(select distinct on (a."Id") 
        me."Id" as "EmployeeId",
        e."FirstName" || '' '' || e."LastName" as "EmployeeName",
        a."Hours" as "Hours"
from    "Projects" as p 
        join "Teams" as te on p."TeamId" = te."Id"
        join "Members" as me on te."Id" = me."TeamId" 
        join "Employees" as e on e."Id" = me."EmployeeId"
        join "Calendar" as c on e."Id" = c."EmployeeId"
        join "Tasks" as a on c."Id" = a."DayId"
where   extract(year from c."Date") = y
        and extract(month from c."Date") = m
        and te."Id" = tId) as res
group by res."EmployeeId", res."EmployeeName"'
;

----------------------------------Working Monthly--------------------------------

CREATE OR REPLACE FUNCTION workingmonthly(empid integer, y integer, m integer)
 RETURNS numeric
 LANGUAGE sql
AS 'select sum(t."Hours")::decimal as workingMonthly
from "Employees" as e
    join "Calendar" as c on c."EmployeeId"=e."Id"
    join "Tasks" as t on t."DayId"=c."Id"
where
extract(year from c."Date") = y
    and extract(month from c."Date") = m
    and e."Id"=empId'
;

---------------------------Working Yearly----------------------------------------

CREATE OR REPLACE FUNCTION workingyearly(empid integer, y integer)
 RETURNS numeric
 LANGUAGE sql
AS 'select sum(t."Hours")::decimal as workingYearly
from "Employees" as e
    join "Calendar" as c on c."EmployeeId"=e."Id"
    join "Tasks" as t on t."DayId"=c."Id"
where extract(year from c."Date") = y
      and e."Id"=empId'
;

---------------------------Yearly Total-----------------------------------------

CREATE OR REPLACE FUNCTION public.yearlytotal(empid integer, y integer)
 RETURNS numeric
 LANGUAGE sql
AS 'select sum(t."Hours") as yearlyTotal
from "Employees" as e
    join "Calendar" as c on c."EmployeeId"=e."Id"
    join "Tasks" as t on t."DayId"=c."Id"
where extract(year from c."Date") = y
      and e."Id"=empId'
;


--------------Testing Ground-----------------------------------------------------------

select * from personaldashboard(1, 2018, 6);

select distinct e."Id", e."FirstName" || ' ' || e."LastName" as "Name",
coalesce( workingMonthly(1, 2018, 6), 0)::decimal as "WorkingMonthly",
workingYearly(1, 2018)::decimal as "WorkingYearly",
sickMonthly(1, 2018, 6)::int as "SickMonthly",
sickYearly(1, 2018)::int as "SickYearly"
from "Employees" as e
join "Calendar" as c on c."EmployeeId"=e."Id"
where e."Id"=1
