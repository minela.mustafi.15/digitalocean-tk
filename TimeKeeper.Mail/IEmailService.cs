﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeKeeper.Mail
{
    public interface IEmailService
    {
        public void Send(string mailTo, string subject, string body);
    }
}
