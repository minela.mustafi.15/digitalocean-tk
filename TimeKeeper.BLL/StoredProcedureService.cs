﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using TimeKeeper.DAL;
using TimeKeeper.DTO.Models.DasboardModels;

namespace TimeKeeper.BLL
{
    public class StoredProcedureService
    {
        protected UnitOfWork _unit;
        public StoredProcedureService(UnitOfWork unit)
        {
            _unit = unit;
        }

        public List<Entity> GetStoredProcedure<Entity>(string procedureName, int[] args)
        {
            var arguments = string.Join(", ", args);
            var cmd = _unit.Context.Database.GetDbConnection().CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"select * from {procedureName}({arguments})";
            if (cmd.Connection.State == ConnectionState.Closed) cmd.Connection.Open();
            DbDataReader sql = cmd.ExecuteReader();

            if (!sql.HasRows) cmd.Connection.Close();

            else if (sql.HasRows)
            {
                var list = SQLFactory.BuildSQL<Entity>(sql);
                cmd.Connection.Close();
                return list;
            }
            return new List<string>() as List<Entity>;
        }
    }
}
