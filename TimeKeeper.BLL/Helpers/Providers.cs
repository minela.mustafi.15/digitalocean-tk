﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Factories;
using TimeKeeper.DTO.Models;

namespace TimeKeeper.BLL.Helpers
{
    public static class Providers
    {
        public static int GetYearlyWorkingDays(this int year)
        {
            int workingDays = 0;
            for (int i = 1; i <= 12; i++)
            {
                workingDays += GetMonthlyWorkingDays(year, i);
            }
            return workingDays;
        }

        public static int GetMonthlyWorkingDays(this int year, int month)
        {
            int daysInMonth = DateTime.DaysInMonth(year, month);
            int workingDays = 0;
            for (int i = 1; i <= daysInMonth; i++)
            {
                DateTime thisDay = new DateTime(year, month, i);
                if (thisDay.DayOfWeek != DayOfWeek.Saturday && thisDay.DayOfWeek != DayOfWeek.Sunday)
                {
                    workingDays += 1;
                }
            }
            return workingDays;
        }

        public static int GetMonthWeekDays(this int month, int year)
        {
            DateTime day = new DateTime(year, month, 1);
            int monthWeekDays = 0;
            while (day.Month == month)
            {
                if (Validators.IsWeekend(day)) { day = day.AddDays(1); continue; }
                monthWeekDays++;
                day = day.AddDays(1);
            }
            return monthWeekDays;
        }

        public static int GetOvertimeHours(this List<DayModel> workdays)
        {
            int overtime = 0;
            foreach (DayModel workday in workdays)
            {
                if (Validators.IsWeekend(workday.Date))
                { overtime += (int)workday.TotalHours; continue; }
                if (workday.TotalHours > 8)
                    overtime += (int)workday.TotalHours - 8;
            }
            return overtime;
        }

        public static int GetEmployeesPTOFromReport(this EmployeeTrackerModel report)
        {
            return (int)report.HourTypes.
                    Where(x =>
                   x.Key == "Holiday" ||
                   x.Key == "Busines" ||
                   x.Key == "Religious" ||
                   x.Key == "Sick" ||
                   x.Key == "Vacation" ||
                   x.Key == "Other").Select(x => x.Value).Sum();
        }

        public static Dictionary<int, decimal> SetMonths()
        {
            Dictionary<int, decimal> HoursPerMonth = new Dictionary<int, decimal>();

            for (int i = 1; i <= 12; i++)
            {
                HoursPerMonth.Add(i, 0);
            }

            return HoursPerMonth;
        }

        public static Dictionary<string, decimal> SetMonthlyOverviewColumns(this List<Project> projects)
        {
            Dictionary<string, decimal> projectColumns = new Dictionary<string, decimal>();
            foreach (Project project in projects)
            {
                projectColumns.Add(project.Name, 0);
            }
            return projectColumns;
        }

    }
}