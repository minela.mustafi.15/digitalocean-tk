﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.BLL.Helpers;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Factories;
using TimeKeeper.DTO.Models;

namespace TimeKeeper.BLL
{
    public class TimeTrackingService
    {
        public UnitOfWork Unit;

        public TimeTrackingService(UnitOfWork unit)
        {
            Unit = unit;
        }

        public async Task<List<EmployeeTrackerModel>> TeamTimeTracking(int teamId, int year, int month)
        {
            var teamTask = await Unit.Teams.Get(teamId);
            Team team = teamTask;
            List<EmployeeTrackerModel> employeeTimeModels = new List<EmployeeTrackerModel>();

            foreach (Member member in team.Members)
            {
                EmployeeTrackerModel empTimeModel = await EmployeeTimeTracking(member.Employee.Id, year, month);
                employeeTimeModels.Add(empTimeModel);
            }

            return employeeTimeModels.ToList();
        }

        public async Task<EmployeeTrackerModel> EmployeeTimeTracking(int employeeId, int year, int month)
        {
            var employeeTask = await Unit.Employees.Get(employeeId);
            var calendarTask = await GetEmployeeMonth(employeeId, year, month);
            var dayTypesTask = await Unit.DayTypes.Get();

            Employee employee = employeeTask;
            List<DayModel> calendar = calendarTask;
            List<DayType> dayTypes = dayTypesTask.ToList();

            EmployeeTrackerModel employeePersonalReport = employee.CreateTimeModel();
            decimal totalHours = 0;
            decimal overtime = 0;
            foreach (DayType day in dayTypes)
            {
                int sumHoursPerDay = (int)calendar.FindAll(x => x.DayType.Id == day.Id).Sum(x => x.TotalHours);
                if (day.Value == "Workday" || day.Value == "Weekend")
                {
                    overtime = Providers.GetOvertimeHours(calendar.FindAll(x => x.DayType.Name == "Workday").ToList());
                }
                employeePersonalReport.HourTypes.Add(day.Value, sumHoursPerDay); //
                totalHours += sumHoursPerDay;
            }

            int missingEntries = calendar.FindAll(x => x.DayType.Name == "Empty").Count() * 8;
            employeePersonalReport.HourTypes.Add("TotalHours", totalHours + missingEntries);
            employeePersonalReport.HourTypes.Add("MissingEntries", missingEntries);
            employeePersonalReport.HourTypes.Add("Overtime", overtime);
            employeePersonalReport.HourTypes.Add("PaidTimeOff", employeePersonalReport.HourTypes["TotalHours"] - employeePersonalReport.HourTypes["MissingEntries"] - employeePersonalReport.HourTypes["Workday"]);

            return employeePersonalReport;
        }

        public async Task<List<DayModel>> GetEmployeeMonth(int empId, int year, int month)
        {
            List<DayModel> calendar = new List<DayModel>();
            if (!Validators.ValidateMonth(year, month)) throw new Exception("Invalid data! Check year and month");
            DayType future = new DayType { Id = 10, Value = "Future" };
            DayType empty = new DayType { Id = 11, Value = "Empty" };
            DayType weekend = new DayType { Id = 12, Value = "Weekend" };
            DayType na = new DayType { Id = 13, Value = "N/A" };
            DateTime day = new DateTime(year, month, 1);

            var empTask = Unit.Employees.Get(empId);
            Employee emp = await empTask;
            while (day.Month == month)
            {
                DayModel newDay = new DayModel
                {
                    Employee = emp.Master(),
                    Date = day,
                    DayType = empty.Master()
                };
                if (Validators.IsWeekend(day)) newDay.DayType = weekend.Master();
                if (day > DateTime.Today) newDay.DayType = future.Master();
                if (day < emp.BeginDate || (emp.EndDate != new DateTime(1, 1, 1) && emp.EndDate != null && day > emp.EndDate)) newDay.DayType = na.Master();
                calendar.Add(newDay);
                day = day.AddDays(1);
            }
            var employeeDaysTask = await Unit.Calendar.Get(x => x.Employee.Id == empId && x.Date.Year == year && x.Date.Month == month);
            List<DayModel> employeeDays = employeeDaysTask.Select(x => x.Create()).ToList();
            foreach (var d in employeeDays)
            {
                calendar[d.Date.Day - 1] = d;
            }
            return calendar;
        }

        public async Task<List<DayModel>> CreateMonth(int employeeId, int year, int month)
        {
            List<DayModel> calendar = new List<DayModel>();
            var empTask = await Unit.Employees.Get(employeeId);
            Employee employee = empTask;
            DateTime day = new DateTime(year, month, 1);
            while (day.Month == month)
            {
                DayModel newDay = new DayModel
                {
                    Employee = employee.Master(),
                    Date = day
                };

                calendar.Add(newDay);
                day.AddDays(1);
            }
            return calendar;
        }
    }
}