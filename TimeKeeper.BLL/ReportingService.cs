﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.BLL.Helpers;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Factories;
using TimeKeeper.DTO.Models;
using TimeKeeper.DTO.Models.ReportModels;

namespace TimeKeeper.BLL
{
    public class ReportingService
    {
        public UnitOfWork Unit;

        public ReportingService(UnitOfWork unit)
        {
            Unit = unit;
        }

        public async Task<List<AnnualTimeModel>> GetAnnual(int year)
        {
            List<AnnualTimeModel> result = new List<AnnualTimeModel>();
            AnnualTimeModel total = new AnnualTimeModel { Project = new MasterModel { Id = 0, Name = "TOTAL" } };
            var projTask = await Unit.Projects.Get();
            List<Project> projects = projTask.ToList().OrderBy(p => p.Name).ToList();
            foreach (Project p in projects)
            {
                List<Detail> query = p.Details.Where(d => d.Day.Date.Year == year).ToList();
                if (query.Count != 0)
                {
                    var list = query.GroupBy(d => d.Day.Date.Month)
                                    .Select(x => new { month = x.Key, hours = x.Sum(y => y.Hours) });
                    AnnualTimeModel atm = new AnnualTimeModel { Project = p.Master() };
                    foreach (var item in list)
                    {
                        atm.Hours[item.month - 1] = item.hours;
                        atm.Total += item.hours;

                        total.Hours[item.month - 1] += item.hours;
                        total.Total += item.hours;
                    }
                    total.Project.Id++;
                    result.Add(atm);
                }
            }
            result.Add(total);
            return result;
        }

        public async Task<MonthlyOverviewModel> GetMonthlyOverview(int year, int month)
        {
            MonthlyOverviewModel monthlyOverview = new MonthlyOverviewModel();
            List<EmployeeModel> employees = new List<EmployeeModel>();
            var tasksTask = await Unit.Details.Get(x => x.Day.Date.Year == year && x.Day.Date.Month == month);
            List<Detail> tasks = tasksTask.ToList();

            List<Project> projects = new List<Project>();
            foreach (Detail task in tasks)
            {
                if (!Validators.IsDuplicate(projects, task.Project))
                {
                    projects.Add(task.Project);
                }
            }
            Dictionary<string, decimal> projectColumns = Providers.SetMonthlyOverviewColumns(projects);
            monthlyOverview.HoursByProject = projectColumns;
            monthlyOverview.TotalHours = 0;
            monthlyOverview.EmployeeProjectHours = new List<EmployeeMonthlyProjectModel>();
            foreach (Detail task in tasks)
            {
                if (!Validators.IsDuplicate(employees, task.Day.Employee))
                {
                    employees.Add(task.Day.Employee.Create());
                }
            }
            foreach (EmployeeModel emp in employees)
            {
                List<Detail> employeesTasks = tasks.Where(x => x.Day.Employee.Id == emp.Id).ToList();
                EmployeeMonthlyProjectModel employeeProjectModel = await GetEmployeeMonthlyOverview(projects, emp, employeesTasks);
                foreach (KeyValuePair<string, decimal> keyValuePair in employeeProjectModel.HoursByProject)
                {
                    monthlyOverview.HoursByProject[keyValuePair.Key] += keyValuePair.Value;
                }
                monthlyOverview.EmployeeProjectHours.Add(employeeProjectModel);
                monthlyOverview.TotalHours += employeeProjectModel.TotalHours;
            }
            monthlyOverview.TotalWorkingDays = Providers.GetMonthlyWorkingDays(year, month);
            monthlyOverview.TotalPossibleWorkingHours = monthlyOverview.TotalWorkingDays * 8;
            return monthlyOverview;
        }

        public async Task<EmployeeMonthlyProjectModel> GetEmployeeMonthlyOverview(List<Project> projects, EmployeeModel employee, List<Detail> tasks)
        {
            Dictionary<string, decimal> projectColumns = Providers.SetMonthlyOverviewColumns(projects);
            EmployeeMonthlyProjectModel employeeProject = new EmployeeMonthlyProjectModel();
            var empTask = await Unit.Employees.Get(employee.Id);
            employeeProject.Employee = empTask.Master();
            employeeProject.HoursByProject = projectColumns;
            employeeProject.TotalHours = 0;
            employeeProject.PaidTimeOff = 0;
            foreach (Detail task in tasks)
            {
                employeeProject.HoursByProject[task.Project.Name] += task.Hours;
                employeeProject.TotalHours += task.Hours;
                if (Validators.IsAbsence(task.Day)) employeeProject.PaidTimeOff += 8;
            }
            return employeeProject;
        }

        public async Task<ProjectHistoryModel> GetProjectHistoryModel(int projectId)
        {
            ProjectHistoryModel projectHistory = new ProjectHistoryModel();

            var tasksTask = await Unit.Projects.Get();
            var employeesTask = await Unit.Employees.Get();

            List<Detail> tasks = tasksTask.ToList().FirstOrDefault(x => x.Id == projectId).Details.ToList();
            List<Employee> employees = new List<Employee>();

            foreach (Detail d in tasks)
            {
                if (employeesTask.ToList().FirstOrDefault(x => x.Id == d.Day.Employee.Id) != null && !Validators.IsDuplicateEmployee(employees, d.Day.Employee))
                {
                    var empByDayTask = await Unit.Employees.Get(d.Day.Employee.Id);
                    employees.Add(empByDayTask);
                }
            }

            foreach (Employee emp in employees)
            {
                EmployeeProjectModel e = new EmployeeProjectModel
                {
                    EmployeeName = emp.FullName,
                    HoursPerYears = await SetYearsColumns(projectId),
                    TotalHoursPerProjectPerEmployee = 0
                };
                foreach (Detail d in tasks)
                {
                    if (d.Day.Employee.Id == emp.Id && e.HoursPerYears.ContainsKey(d.Day.Date.Year))
                    {
                        e.HoursPerYears[d.Day.Date.Year] += d.Hours;
                    }
                }
                foreach (KeyValuePair<int, decimal> keyValuePair in e.HoursPerYears)
                {
                    e.TotalHoursPerProjectPerEmployee += keyValuePair.Value;
                }
                projectHistory.Employees.Add(e);
            }
            projectHistory.TotalYearlyProjectHours = await SetYearsColumns(projectId);

            foreach (EmployeeProjectModel empProjectModel in projectHistory.Employees)
            {
                foreach (KeyValuePair<int, decimal> keyValuePair in empProjectModel.HoursPerYears)
                {
                    projectHistory.TotalYearlyProjectHours[keyValuePair.Key] += keyValuePair.Value;
                }
                projectHistory.TotalHoursPerProject += empProjectModel.TotalHoursPerProjectPerEmployee;
            }
            return projectHistory;
        }

        public async Task<Dictionary<int, decimal>> SetYearsColumns(int projectId)
        {
            Dictionary<int, decimal> yearColumns = new Dictionary<int, decimal>();
            var tasksTask = await Unit.Details.Get(x => x.Project.Id == projectId);
            List<Detail> tasks = tasksTask.ToList();

            foreach (Detail d in tasks)
            {
                if (!Validators.IsDuplicateYear(yearColumns, d.Day.Date.Year))
                    yearColumns.Add(d.Day.Date.Year, 0);
            }
            return yearColumns;
        }


        public async Task<List<AnnualTimeModel>> GetAnnualStored(int year)
        {
            List<AnnualTimeModel> result = new List<AnnualTimeModel>();
            AnnualTimeModel total = new AnnualTimeModel { Project = new MasterModel { Id = 0, Name = "TOTL" } };

            var cmd = Unit.Context.Database.GetDbConnection().CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"select * from AnnualReport({year})";

            if (cmd.Connection.State == ConnectionState.Closed) cmd.Connection.Open();

            DbDataReader sql = cmd.ExecuteReader();
            List<AnnualRawModel> rawData = new List<AnnualRawModel>();
            if (sql.HasRows)
            {
                while (sql.Read())
                {
                    rawData.Add(new AnnualRawModel
                    {
                        Id = sql.GetInt32(0),
                        Name = sql.GetString(1),
                        Month = sql.GetInt32(2),
                        Hours = sql.GetDecimal(3)
                    });
                }

                AnnualTimeModel atm = new AnnualTimeModel { Project = new MasterModel { Id = 0 } };
                foreach (AnnualRawModel item in rawData)
                {
                    if (atm.Project.Id != item.Id)
                    {
                        if (atm.Project.Id != 0) result.Add(atm);
                        atm = new AnnualTimeModel { Project = new MasterModel { Id = item.Id, Name = item.Name } };
                        total.Project.Id++;
                    }

                    atm.Hours[item.Month - 1] = item.Hours;
                    atm.Total += item.Hours;

                    total.Hours[item.Month - 1] += item.Hours;
                    total.Total += item.Hours;
                }
                if (atm.Project.Id != 0) result.Add(atm);
            }
            result.Add(total);
            return result;
        }

        public async Task<ProjectMonthlyModelStored> GetMonthlyStored(int year, int month)
        {
            ProjectMonthlyModelStored result = new ProjectMonthlyModelStored();

            var cmd = Unit.Context.Database.GetDbConnection().CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"select * from MonthlyReport({year},{month})";
            if (cmd.Connection.State == ConnectionState.Closed) cmd.Connection.Open();
            DbDataReader sql = cmd.ExecuteReader();
            List<MonthlyRawModel> rawData = new List<MonthlyRawModel>();
            if (sql.HasRows)
            {
                while (sql.Read())
                {
                    rawData.Add(new MonthlyRawModel
                    {
                        EmpId = sql.GetInt32(0),
                        EmpName = sql.GetString(1),
                        ProjId = sql.GetInt32(2),
                        ProjName = sql.GetString(3),
                        Hours = sql.GetDecimal(4)
                    });
                }

                result.Projects = rawData.GroupBy(x => new { x.ProjId, x.ProjName })
                                         .Select(x => new MasterModel { Id = x.Key.ProjId, Name = x.Key.ProjName })
                                         .ToList();
                List<int> projList = result.Projects.Select(x => x.Id).ToList();

                EmployeeProjectModelStored emp = new EmployeeProjectModelStored(projList) { Employee = new MasterModel() { Id = 0 } };
                foreach(MonthlyRawModel item in rawData)
                {
                    if (item.EmpId != emp.Employee.Id)
                    {
                        if (emp.Employee.Id != 0) result.Employees.Add(emp);
                        emp = new EmployeeProjectModelStored(projList)
                        {
                            Employee = new MasterModel { Id = item.EmpId, Name = item.EmpName },
                        };
                    }
                    emp.Hours[item.ProjId] = item.Hours;
                    emp.TotalHours += item.Hours;
                }
                if (emp.Employee.Id != 0) result.Employees.Add(emp);
            }

            return result;
        }

        public async Task<ProjectHistoryAnnualModel> GetProjectHistoryStored(int projectId)
        {
            ProjectHistoryAnnualModel result = new ProjectHistoryAnnualModel();
            var cmd = Unit.Context.Database.GetDbConnection().CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"select * from ProjectHistory({projectId})";
            if (cmd.Connection.State == ConnectionState.Closed) cmd.Connection.Open();
            DbDataReader sql = cmd.ExecuteReader();
            List<HistoryRawModel> rawData = new List<HistoryRawModel>();
            if (sql.HasRows)
            {
                while (sql.Read())
                {
                    rawData.Add(new HistoryRawModel
                    {
                        EmployeeId = sql.GetInt32(0),
                        EmployeeName = sql.GetString(1),
                        Hours = sql.GetDecimal(2),
                        Year = sql.GetInt32(3)
                    });
                }
                HashSet<int> set = new HashSet<int>();
                result.Years = rawData.Select(x => x.Year).Distinct().ToList();
                EmployeeProjectHistoryStored eph = new EmployeeProjectHistoryStored(result.Years) { Employee = new MasterModel { Id = 0 } };
                foreach (HistoryRawModel item in rawData)
                {
                    if (item.EmployeeId != eph.Employee.Id)
                    {
                        if (eph.Employee.Id != 0) result.Employees.Add(eph);
                        eph = new EmployeeProjectHistoryStored(result.Years)
                        {
                            Employee = new MasterModel { Id = item.EmployeeId, Name = item.EmployeeName }
                        };
                    }
                    eph.TotalYearlyProjectHours[item.Year] = item.Hours;
                    eph.TotalHoursPerProject += item.Hours;
                }
                if (eph.Employee.Id != 0) result.Employees.Add(eph);
            }
            return result;
        }
    }
}