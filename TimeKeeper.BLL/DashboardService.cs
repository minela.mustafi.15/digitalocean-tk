using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeKeeper.BLL.Helpers;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Factories;
using TimeKeeper.DTO.Models;
using TimeKeeper.DTO.Models.DasboardModels;
using TimeKeeper.DTO.Models.DasboardModels.TimeKeeper.DTO.Models.DashboardModels;

namespace TimeKeeper.BLL
{
    public class DashboardService
    {
        protected UnitOfWork _unit;
        protected StoredProcedureService _storedProcedureService;

        public DashboardService(UnitOfWork unit, StoredProcedureService storedProcedureService)
        {
            _unit = unit;
            _storedProcedureService = storedProcedureService;
        }

        public async Task<TeamDashboardModel> GetTeamDashboardInfo(int teamId, int year, int month)
        {
            var Tracker = new TimeTrackingService(_unit);
            TeamDashboardModel teamDashboardModel = new TeamDashboardModel();
            List<EmployeeTrackerModel> teamMonthReport = await Tracker.TeamTimeTracking(teamId, year, month);

            var teams = await _unit.Teams.Get(teamId);

            teamDashboardModel.EmployeesCount = teams.Members.Count();
            teamDashboardModel.ProjectsCount = teams.Projects.Count();
            teamDashboardModel.BaseTotalHours = Providers.GetMonthWeekDays(month, year) * 8;
            teamDashboardModel.TotalHours = teamDashboardModel.BaseTotalHours * teamDashboardModel.EmployeesCount;

            teamDashboardModel.WorkingHours = (int)teamMonthReport.Sum(x => x.HourTypes["Workday"]);
            foreach (EmployeeTrackerModel report in teamMonthReport)
            {
                teamDashboardModel.PaidTimeOff.Add(new EmployeeKeyDictionary(report.Employee, Providers.GetEmployeesPTOFromReport(report)));
                teamDashboardModel.Overtime.Add(new EmployeeKeyDictionary(report.Employee, report.HourTypes["Overtime"]));
                teamDashboardModel.MissingEntries.Add(new EmployeeKeyDictionary(report.Employee, report.HourTypes["MissingEntries"]));
            }

            return teamDashboardModel;
        }

        public async Task<AdminDashboardModel> GetAdminDashboardInfo(int year, int month)
        {
            AdminDashboardModel adminDashboardModel = new AdminDashboardModel();
            adminDashboardModel.EmployeesCount = await GetNumberOfEmployeesForTimePeriod(month, year);
            List<ProjectModel> projects = await GetProjectsForTimePeriod(month, year);


            adminDashboardModel.BaseTotalHours = Providers.GetMonthWeekDays(month, year) * 8;
            adminDashboardModel.TotalHours = adminDashboardModel.BaseTotalHours * adminDashboardModel.EmployeesCount;
            var teamIdsTask = await _unit.Teams.Get();
            List<int> teamIds = teamIdsTask.Select(x => x.Id).ToList();
            foreach (int teamId in teamIds)
            {
                var teamTask = await _unit.Teams.Get(teamId);
                MasterModel team = teamTask.Master();
                TeamDashboardModel teamDashboardModel = await GetTeamDashboardInfo(teamId, year, month);
                adminDashboardModel.MissingEntries.Add(new TeamKeyDictionary(team, (int)teamDashboardModel.MissingEntries.Sum(x => x.Value)));
                adminDashboardModel.PaidTimeOff.Add(new TeamKeyDictionary(team, (int)teamDashboardModel.PaidTimeOff.Sum(x => x.Value)));
                adminDashboardModel.Overtime.Add(new TeamKeyDictionary(team, (int)teamDashboardModel.Overtime.Sum(x => x.Value)));
            }
            return adminDashboardModel;
        }

        public async Task<AdminDashboardStoredModel> GetAdminDashboardStored(int year, int month)
        {
            var roleGet = await _unit.Roles.Get();
            List<string> roles = roleGet.Select(x => x.Name).ToList();
            AdminDashboardStoredModel adminDashboard = new AdminDashboardStoredModel(roles);

            List<AdminRawCountModel> rawData = _storedProcedureService.GetStoredProcedure<AdminRawCountModel>("AdminDashboard", new int[] { year, month });
            adminDashboard.PaidTimeOff = _storedProcedureService.GetStoredProcedure<AdminRawPTOModel>("AdminPTOHours", new int[] { year, month });
            adminDashboard.EmployeesCount = rawData.GroupBy(x => x.EmployeeId).Count();
            adminDashboard.ProjectsCount = rawData.GroupBy(x => x.ProjectId).Count();
            adminDashboard.WorkingHours = rawData.Sum(x => x.WorkingHours);

            var teamGet = await _unit.Teams.Get();
            List<Team> teams = teamGet.ToList();
            List<Project> projects = teams.SelectMany(x => x.Projects).ToList();

            return adminDashboard;
        }

        public async Task<int> GetNumberOfEmployeesForTimePeriod(int month, int year)
        {

            var empNUmber = await _unit.Employees.Get(x => x.BeginDate < new DateTime(year, month, DateTime.DaysInMonth(year, month))
                            && (x.EndDate == null || x.EndDate == new DateTime(1, 1, 1) || x.EndDate > new DateTime(year, month, DateTime.DaysInMonth(year, month))));

            return empNUmber.Count();
        }

        public async Task<List<ProjectModel>> GetProjectsForTimePeriod(int month, int year)
        {
            List<Project> query = _unit.Projects.Get(x => x.BeginDate < new DateTime(year, month, DateTime.DaysInMonth(year, month))
                            && (x.EndDate == null || x.EndDate == new DateTime(1, 1, 1) || x.EndDate > new DateTime(year, month, DateTime.DaysInMonth(year, month)))).Result.ToList();
            List<ProjectModel> projects = query.Select(x => x.Create()).ToList();
            return projects;
        }

        public async Task<PersonalDashboardModel> GetEmployeeYearDashboard(int employeeId, int year)
        {
            List<DayModel> calendar = await GetEmployeeCalendar(employeeId, year);
            decimal totalHours = Providers.GetYearlyWorkingDays(year) * 8;
            var result = await CreatePersonalDashboard(employeeId, year, totalHours, calendar);
            return result;
        }

        public async Task<PersonalDashboardModel> GetEmployeeMonthDashboard(int employeeId, int year, int month)
        {
            List<DayModel> calendar = await GetEmployeeCalendar(employeeId, year, month);
            decimal totalHours = Providers.GetMonthlyWorkingDays(year, month) * 8;
            var result = await CreatePersonalDashboard(employeeId, year, totalHours, calendar);
            return result;
        }

        private async Task<PersonalDashboardModel> CreatePersonalDashboard(int employeeId, int year, decimal totalHours, List<DayModel> calendar)
        {
            var emp = await _unit.Employees.Get(employeeId);
            decimal workingHours = calendar.Where(x => x.DayType.Name == "Workday").Sum(x => x.TotalHours);
            return new PersonalDashboardModel
            {
                Employee = emp.Master(),
                TotalHours = totalHours,
                WorkingHours = workingHours,
                BradfordFactor = await GetBradfordFactor(employeeId, year)
            };
        }

        public async Task<List<DayModel>> GetEmployeeCalendar(int employeeId, int year)
        {
            var result = await _unit.Calendar.Get(x => x.Employee.Id == employeeId && x.Date.Year == year);
            return result.Select(x => x.Create()).ToList();
        }

        public async Task<List<DayModel>> GetEmployeeCalendar(int empId, int year, int month)
        {
            var result = await _unit.Calendar.Get(x => x.Employee.Id == empId && x.Date.Year == year && x.Date.Month == month);
            return result.Select(x => x.Create()).ToList();
        }

        public async Task<decimal> GetBradfordFactor(int employeeId, int year)
        {
            List<DayModel> calendar = await GetEmployeeCalendar(employeeId, year);
            int absenceInstances = 0;
            int absenceDays = 0;
            calendar = calendar.OrderBy(x => x.Date).ToList();

            absenceDays = calendar.Where(x => x.DayType.Name == "Sick" && x.Date < DateTime.Now).Count();
            for (int i = 0; i < calendar.Count; i++)
            {
                if (calendar[i].DayType.Name == "Sick" && calendar[i].Date < DateTime.Now)
                {
                    if (i == 0) absenceInstances++;
                    else if (calendar[i - 1].DayType.Name != "Sick")
                    {
                        absenceInstances++;
                    }
                }
            }
            return (decimal)Math.Pow(absenceInstances, 2) * absenceDays;
        }


        public async Task<PersonalDashboardStoredModel> GetPersonalDashboardStored(int empId, int year, int month)
        {
            PersonalDashboardStoredModel personalDashboard = new PersonalDashboardStoredModel();
            List<PersonalDashboardRawModel> rawData = _storedProcedureService.GetStoredProcedure<PersonalDashboardRawModel>("personalDashboard", new int[] { empId, year, month });
            decimal workingDaysInMonth = Providers.GetMonthlyWorkingDays(year, month) * 8;
            decimal workingDaysInYear = Providers.GetYearlyWorkingDays(year) * 8;

                personalDashboard.PersonalDashboardHours = rawData[0];
                personalDashboard.UtilizationMonthly = decimal.Round(((rawData[0].WorkingMonthly / workingDaysInMonth) * 100), 2, MidpointRounding.AwayFromZero);
                personalDashboard.UtilizationYearly = decimal.Round(((rawData[0].WorkingYearly / workingDaysInYear) * 100), 2, MidpointRounding.AwayFromZero);
                personalDashboard.BradfordFactor = GetBradfordFactor(rawData[0], year).Result;

            return personalDashboard;
        }

        public async Task<decimal> GetBradfordFactor(PersonalDashboardRawModel personalDashboardHours, int year)
        {
            int absenceInstances = 0;
            int absenceDays = personalDashboardHours.SickYearly;

            var cmd = _unit.Context.Database.GetDbConnection().CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = $"select * from sickByMonths({personalDashboardHours.EmployeeId}, {year})";
            if (cmd.Connection.State == ConnectionState.Closed) cmd.Connection.Open();
            DbDataReader sql = cmd.ExecuteReader();
            if (sql.HasRows)
            {
                while (sql.Read())
                {
                    absenceInstances++;
                }
            }

            return (decimal)Math.Pow(absenceInstances, 2) * absenceDays;
        }

        /*###########################################################################################################*/
        public CompanyDashboardModel GetCompanyDashboard(int year, int month)
        {
            CompanyDashboardModel companyDashboard = new CompanyDashboardModel();
            decimal baseHours = Providers.GetMonthlyWorkingDays(year, month) * 8;
            List<CompanyDashboardRawModel> rawData = _storedProcedureService.GetStoredProcedure<CompanyDashboardRawModel>("CompanyWorkingHoursData", new int[] { year, month });
            List<CompanyEmployeeHoursModel> employeeHours = _storedProcedureService.GetStoredProcedure<CompanyEmployeeHoursModel>("EmployeeHoursByDayType", new int[] { year, month });
            List<CompanyOvertimeModel> overtime = _storedProcedureService.GetStoredProcedure<CompanyOvertimeModel>("CompanyOvertimeHours", new int[] { year, month });

            List<MasterModel> activeTeams = new List<MasterModel>();
            activeTeams.AddRange(rawData.GroupBy(x => new
            {
                Id = x.TeamId,
                Name = x.TeamName
            }).ToList().Select(x => new MasterModel
            {
                Id = x.Key.Id,
                Name = x.Key.Name
            }).ToList());

            companyDashboard.EmployeesCount = rawData.GroupBy(x => x.EmployeeId).Count();
            companyDashboard.ProjectsCount = rawData.GroupBy(x => x.ProjectId).Count();
            companyDashboard.TotalHours = companyDashboard.EmployeesCount * baseHours;
            companyDashboard.TotalWorkingHours = rawData.Sum(x => x.WorkingHours);
            companyDashboard.Projects = GetCompanyProjectModels(rawData);
            companyDashboard.Roles = GetRoleUtilization(rawData, baseHours);
            companyDashboard.Teams = GetCompanyTeamModels(rawData, employeeHours, activeTeams, overtime);
            GetCompanyMissingEntries(employeeHours, companyDashboard.Teams, baseHours, overtime);

            return companyDashboard;
        }
        private decimal EmployeeRatioInTeam(List<CompanyEmployeeHoursModel> workDays, int teamId, int employeeId)
        {
            decimal empTeamWorkingHours = workDays.Where(x => x.EmployeeId == employeeId && x.TeamId == teamId).Sum(x => x.DayTypeHours);
            decimal empWorkingHours = workDays.Where(x => x.EmployeeId == employeeId).Sum(x => x.DayTypeHours);
            return empTeamWorkingHours / empWorkingHours;
        }
        private void GetCompanyMissingEntries(List<CompanyEmployeeHoursModel> employeeHours, List<CompanyTeamModel> teams, decimal baseHours, List<CompanyOvertimeModel> overtime)
        {
            List<EmployeeMissingEntriesModel> missingEntriesEmployee = employeeHours.GroupBy(x => new { x.EmployeeId, x.EmployeeName })
                .Select(x => new EmployeeMissingEntriesModel
                {
                    Employee = new MasterModel { Id = x.Key.EmployeeId, Name = x.Key.EmployeeName },
                    MissingEntries = baseHours - x.Sum(y => y.DayTypeHours) + overtime.Where(y => y.EmployeeId == x.Key.EmployeeId).Sum(y => y.OvertimeHours)
                }).Where(x => x.MissingEntries > 0).ToList();

            foreach (CompanyTeamModel team in teams)
            {
                foreach (EmployeeMissingEntriesModel employee in missingEntriesEmployee)
                {
                    if (employeeHours.Any(x => x.EmployeeId == employee.Employee.Id && x.TeamId == team.Team.Id))
                    {
                        team.MissingEntries += employee.MissingEntries;
                    }
                }
            }
        }
        private List<CompanyTeamModel> GetCompanyTeamModels(List<CompanyDashboardRawModel> rawData, List<CompanyEmployeeHoursModel> employeeHours, List<MasterModel> activeTeams, List<CompanyOvertimeModel> overtime)
        {
            List<CompanyTeamModel> teams = new List<CompanyTeamModel>();
            teams.AddRange(activeTeams.Select(x => new CompanyTeamModel
            {
                Team = new MasterModel {
                Id = x.Id,
                Name = x.Name
                },
                Overtime = 0,
                PaidTimeOff = 0
            }).OrderBy(x => x.Team.Id).ToList());

            List<CompanyOvertimeModel> overtimeNotNull = overtime.Where(x => x.OvertimeHours > 0).ToList();
            List<CompanyEmployeeHoursModel> paidTimeOff = employeeHours.Where(x => x.DayTypeName != "Workday").ToList();
            List<CompanyEmployeeHoursModel> workDays = employeeHours.Where(x => x.DayTypeName == "Workday").ToList();

            foreach (CompanyEmployeeHoursModel row in workDays)
            {
                if (row.TeamId != 0)
                {
                    if (overtimeNotNull.FirstOrDefault(x => x.EmployeeId == row.EmployeeId) != null)
                    {
                        GetCompanyOvertime(teams, workDays, overtimeNotNull, row.TeamId, row.EmployeeId);
                    }

                    if (paidTimeOff.FirstOrDefault(x => x.EmployeeId == row.EmployeeId) != null)
                    {
                        GetCompanyPaidTimeOff(teams, workDays, paidTimeOff, row.TeamId, row.EmployeeId);
                    }
                }
            }

            return teams;
        }
        private void GetCompanyOvertime(List<CompanyTeamModel> teams, List<CompanyEmployeeHoursModel> workDays, List<CompanyOvertimeModel> overtime, int teamId, int employeeId)
        {
            decimal empOvertime = overtime.Where(x => x.EmployeeId == employeeId).Sum(x => x.OvertimeHours);
            teams.FirstOrDefault(x => x.Team.Id == teamId).Overtime += empOvertime * EmployeeRatioInTeam(workDays, teamId, employeeId);
        }
        private void GetCompanyPaidTimeOff(List<CompanyTeamModel> teams, List<CompanyEmployeeHoursModel> workDays, List<CompanyEmployeeHoursModel> paidTimeOff, int teamId, int employeeId)
        {
            decimal empPaidTimeOff = paidTimeOff.Where(x => x.EmployeeId == employeeId).Sum(x => x.DayTypeHours);
            decimal empRatio = EmployeeRatioInTeam(workDays, teamId, employeeId);
            teams.FirstOrDefault(x => x.Team.Id == teamId).PaidTimeOff += empPaidTimeOff * empRatio;
        }
        private List<CompanyRolesDashboardModel> GetRoleUtilization(List<CompanyDashboardRawModel> rawData, decimal baseHours)
        {
            List<CompanyRolesDashboardModel> roles = new List<CompanyRolesDashboardModel>();

            List<CompanyRolesRawModel> rolesRaw = CreateRolesRaw(rawData);

            CompanyRolesDashboardModel role = new CompanyRolesDashboardModel { Role = new MasterModel { Id=0, Name="" } };
            foreach (CompanyRolesRawModel row in rolesRaw)
            {
                if (row.RoleName != role.Role.Name)
                {
                    if (role.Role.Name != "") roles.Add(role);
                    role = new CompanyRolesDashboardModel { Role = new MasterModel{Id=row.RoleId, Name= row.RoleName }  };
                    role.WorkingHours = rolesRaw.Where(x => x.RoleName == role.Role.Name).Sum(x => x.WorkingHours);
                }
                decimal hoursEmployeeRole = rolesRaw.Where(x => x.EmployeeId == row.EmployeeId && x.RoleName == role.Role.Name).Sum(x => x.WorkingHours);
                decimal hoursEmployee = rolesRaw.Where(x => x.EmployeeId == row.EmployeeId).Sum(x => x.WorkingHours);
                role.TotalHours += (hoursEmployeeRole / hoursEmployee) * baseHours;
            }
            if (role.Role.Name != "") roles.Add(role);

            return roles;
        }

        private List<CompanyRolesRawModel> CreateRolesRaw(List<CompanyDashboardRawModel> rawData)
        {
            List<CompanyRolesRawModel> rolesRaw = rawData.GroupBy(x => new { x.EmployeeId, x.RoleId, x.RoleName }).Select(
                x => new CompanyRolesRawModel
                {
                    EmployeeId = x.Key.EmployeeId,
                    RoleId = x.Key.RoleId,
                    RoleName = x.Key.RoleName,
                    WorkingHours = x.Sum(y => y.WorkingHours)
                }).ToList().OrderBy(x => x.RoleName).ToList();

            return rolesRaw;
        }

        private List<CompanyProjectsDashboardModel> GetCompanyProjectModels(List<CompanyDashboardRawModel> rawData)
        {
            List<CompanyProjectsDashboardModel> projects = new List<CompanyProjectsDashboardModel>();
            List<CompanyDashboardRawModel> rawProjects = rawData.OrderBy(x => x.ProjectId).ToList();

            CompanyProjectsDashboardModel project = new CompanyProjectsDashboardModel { Project = new MasterModel { Id = 0 } };
            foreach (CompanyDashboardRawModel row in rawProjects)
            {
                if (row.ProjectId != project.Project.Id)
                {
                    if (project.Project.Id != 0) projects.Add(project);
                    project = new CompanyProjectsDashboardModel
                    {
                        Project = new MasterModel { Id = row.ProjectId, Name = row.ProjectName },
                        Revenue = GetProjectRevenue(rawProjects, row.ProjectId, row.ProjectPricingName)
                    };
                }
            }
            if (project.Project.Id != 0) projects.Add(project);

            return projects;
        }

        private decimal GetProjectRevenue(List<CompanyDashboardRawModel> rawData, int projectId, string pricingType)
        {
            switch (pricingType)
            {
                case "Fixed bid":
                    return rawData.FirstOrDefault(x => x.ProjectId == projectId).ProjectAmount;
                case "Hourly":
                    return rawData.Where(x => x.ProjectId == projectId).Sum(x => x.WorkingHours * x.RoleHourlyPrice);
                case "PerCapita":
                    return rawData.Where(x => x.ProjectId == projectId)
                                  .GroupBy(x => new { x.EmployeeId, x.ProjectId, x.RoleMonthlyPrice })
                                  .ToList().Sum(x => x.Key.RoleMonthlyPrice);
                default:
                    return 0;
            }
        }

        /*########################################################################################################*/
        public TeamDashboardModelNew GetTeamDashboardStored(Team team, int year, int month)
        {
            TeamDashboardModelNew teamDashboard = new TeamDashboardModelNew();
            List<TeamRawModel> rawData = _storedProcedureService.GetStoredProcedure<TeamRawModel>("TeamDashboard", new int[] { team.Id, year, month });
            List<TeamRawNonWorkingHoursModel> rawDataPTO = _storedProcedureService.GetStoredProcedure<TeamRawNonWorkingHoursModel>("GetMemberPTOHours", new int[] { team.Id, year, month });
            List<TeamRawNonWorkingHoursModel> rawDataOvertime = _storedProcedureService.GetStoredProcedure<TeamRawNonWorkingHoursModel>("GetMemberOvertimeHours", new int[] { team.Id, year, month });

            decimal baseTotalHours = Providers.GetMonthlyWorkingDays(year, month) * 8;

            teamDashboard.Year = year;
            teamDashboard.Month = month;
            teamDashboard.Team = new MasterModel { Id = team.Id, Name = team.Name };
            teamDashboard.NumberOfEmployees = rawData.GroupBy(x => x.EmployeeId).Count();
            teamDashboard.TotalWorkingHours = rawData.Sum(x => x.Value);
            teamDashboard.TotalHours = baseTotalHours * teamDashboard.NumberOfEmployees;

            List<TeamRawCountModel> rawDataProjectsCount = _storedProcedureService.GetStoredProcedure<TeamRawCountModel>("CountProjects", new int[] { team.Id, year, month });

            teamDashboard.NumberOfProjects = rawDataProjectsCount.Count;

            List<TeamRawModel> rawDataMissingEntries = GetMembersMissingEntries(team.Id, year, month, baseTotalHours);

            foreach (TeamRawModel r in rawData)
            {
                teamDashboard.EmployeeTimes.Add(new TeamMemberDashboardModel
                {
                    Employee = new MasterModel { Id = r.EmployeeId, Name = r.EmployeeName },
                    TotalHours = baseTotalHours,
                    Overtime = (rawDataOvertime == null || rawDataOvertime.FirstOrDefault(x => x.MemberId == r.EmployeeId) == null) ? 0 : rawDataOvertime.FirstOrDefault(x => x.MemberId == r.EmployeeId).Value,
                    PaidTimeOff = (rawDataPTO == null || rawDataPTO.FirstOrDefault(x => x.MemberId == r.EmployeeId) == null) ? 0 : rawDataPTO.FirstOrDefault(x => x.MemberId == r.EmployeeId).Value,
                    WorkingHours = r.Value,
                    MissingEntries = (rawDataMissingEntries == null || rawDataMissingEntries.FirstOrDefault(x => x.EmployeeId == r.EmployeeId) == null) ? 0 : rawDataMissingEntries.FirstOrDefault(x => x.EmployeeId == r.EmployeeId).Value,
                });
            }
            return teamDashboard;
        }
        public List<TeamRawModel> GetMembersMissingEntries(int teamId, int year, int month, decimal baseTotalHours)
        {
            List<TeamRawModel> rawData = _storedProcedureService.GetStoredProcedure<TeamRawModel>("DateMonth", new int[] { teamId, year, month });

            foreach (TeamRawModel trm in rawData)
            {
                trm.Value = baseTotalHours - trm.Value * 8;   // trm su odradjeni dani;
            }
            return rawData;
        }
    }
}
