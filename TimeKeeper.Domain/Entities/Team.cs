﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Domain
{
    public class Team : BaseClass
    {
        public Team()
        {
            Members = new List<Member>();
            Projects = new List<Project>();
        }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
        public virtual IList<Member> Members { get; set; }
        public virtual IList<Project> Projects { get; set; }
        public bool Status { get; set; }
    }
}