﻿using System.Collections.Generic;

namespace TimeKeeper.Domain.Entities
{
    public class ProjectStatus : BaseClass
    {
        public ProjectStatus()
        {
            Projects = new List<Project>();
        }

        public int Type { get; set; }
        public string Value { get; set; }
        public virtual IList<Project> Projects { get; set; }
    }
}