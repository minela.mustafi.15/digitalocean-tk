﻿using System.ComponentModel.DataAnnotations;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Domain
{
    public class Member : BaseClass
    {
        public virtual Employee Employee { get; set; }
        public virtual Team Team { get; set; }
        public virtual Role Role { get; set; }

        [Required]
        public decimal HoursWeekly { get; set; }

        public virtual MemberStatus Status { get; set; }
    }
}