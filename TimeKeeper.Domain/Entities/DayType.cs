﻿using System.Collections.Generic;

namespace TimeKeeper.Domain.Entities
{
    public class DayType : BaseClass
    {
        public DayType()
        {
            Days = new List<Day>();
        }

        public int Type { get; set; }
        public string Value { get; set; }
        public virtual IList<Day> Days { get; set; }
    }
}