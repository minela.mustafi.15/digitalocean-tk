﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TimeKeeper.Domain.Entities
{
    public class Project : BaseClass
    {
        public Project()
        {
            Details = new List<Detail>();
        }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Team Team { get; set; }

        public DateTime? BeginDate { get; set; }

        public DateTime? EndDate { get; set; }
        public virtual ProjectStatus Status { get; set; }
        public virtual PricingType Pricing { get; set; }

        [Required]
        public decimal Ammount { get; set; }

        public virtual IList<Detail> Details { get; set; }
    }
}