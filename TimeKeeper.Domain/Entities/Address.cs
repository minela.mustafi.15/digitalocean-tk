﻿namespace TimeKeeper.Domain.Entities
{
    public class Address
    {
        public string Road { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}