﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace TimeKeeper.Domain.Entities
{
    [Table("Calendar")]
    public class Day : BaseClass
    {
        public Day()
        {
            Details = new List<Detail>();
        }

        [Required]
        public DateTime Date { get; set; }

        public virtual Employee Employee { get; set; }
        public virtual DayType Type { get; set; }
        public virtual IList<Detail> Details { get; set; }
    }
}