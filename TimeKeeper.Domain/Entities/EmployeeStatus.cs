﻿using System.Collections.Generic;

namespace TimeKeeper.Domain.Entities
{
    public class EmployeeStatus : BaseClass
    {
        public EmployeeStatus()
        {
            Employees = new List<Employee>();
        }

        public int Type { get; set; }
        public string Value { get; set; }
        public virtual IList<Employee> Employees { get; set; }
    }
}