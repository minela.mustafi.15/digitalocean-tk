﻿using System.Collections.Generic;

namespace TimeKeeper.Domain.Entities
{
    public class MemberStatus : BaseClass
    {
        public MemberStatus()
        {
            Members = new List<Member>();
        }

        public int Type { get; set; }
        public string Value { get; set; }
        public virtual IList<Member> Members { get; set; }
    }
}