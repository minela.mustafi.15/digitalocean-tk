﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Domain
{
    public class Employee : BaseClass
    {
        public Employee()
        {
            Calendar = new List<Day>();
            Members = new List<Member>();
        }

        [Required, MinLength(3)]
        public string FirstName { get; set; }

        [Required, MinLength(3)]
        public string LastName { get; set; }

        public string Image { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Phone { get; set; }
        public DateTime BirthDate { get; set; }
        public virtual EmployeeStatus Status { get; set; }

        //[Required]
        public DateTime BeginDate { get; set; }

        [NotMapped]
        public int Age
        {
            get
            {
                DateTime now = DateTime.Today;
                int age = now.Year - BirthDate.Year;
                if (now < BirthDate.AddYears(age)) age--;
                return age;
            }
        }

        public DateTime? EndDate { get; set; }
        public virtual PositionType Position { get; set; }
        public virtual IList<Day> Calendar { get; set; }
        public virtual IList<Member> Members { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}