﻿using System.Collections.Generic;

namespace TimeKeeper.Domain.Entities
{
    public class CustomerStatus : BaseClass
    {
        public CustomerStatus()
        {
            Customers = new List<Customer>();
        }

        public int Type { get; set; }
        public string Value { get; set; }
        public virtual List<Customer> Customers { get; set; }
    }
}