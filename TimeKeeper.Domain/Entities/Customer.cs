﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TimeKeeper.Domain.Entities
{
    public class Customer : BaseClass
    {
        public Customer()
        {
            Address = new Address();
            Projects = new List<Project>();
        }

        [Required, MinLength(3)]
        public string Name { get; set; }

        public string Image { get; set; }

        [Required]
        public string Contact { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        public string Phone { get; set; }

        [Required]
        public virtual Address Address { get; set; }

        public virtual CustomerStatus Status { get; set; }
        public virtual List<Project> Projects { get; set; }
    }
}