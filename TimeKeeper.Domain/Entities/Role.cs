﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Domain
{
    public class Role : BaseClass
    {
        public Role()
        {
            Members = new List<Member>();
        }

        [Required]
        public string Name { get; set; }

        public decimal HourlyPrice { get; set; }
        public decimal MonthlyPrice { get; set; }

        public virtual IList<Member> Members { get; set; }
    }
}