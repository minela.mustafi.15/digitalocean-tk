﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TimeKeeper.Domain.Entities
{
    [Table("Tasks")]
    public class Detail : BaseClass
    {
        public virtual Day Day { get; set; }
        public virtual Project Project { get; set; }
        public string Description { get; set; }

        [Required]
        public decimal Hours { get; set; }
    }
}