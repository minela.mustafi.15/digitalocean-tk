﻿using OfficeOpenXml;
using System;
using System.Threading.Tasks;
using TimeKeeper.DAL;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Seed
{
    public static class Calendar
    {
        public static async Task Collect(ExcelWorksheet rawData, UnitOfWork unit)
        {
            Console.Write("Calendar: ");
            int N = 0;
            for (int row = 2; row <= rawData.Dimension.Rows; row++)
            {
                Day d = new Day
                {
                    Id = rawData.ReadInteger(row, 1),
                    Date = rawData.ReadDate(row, 4),
                    Employee = await unit.Employees.Get(Utility.dicEmployees[rawData.ReadInteger(row, 2)]),
                    Type = await unit.DayTypes.Get(rawData.ReadInteger(row, 3)),
                };

                unit.Calendar.Insert(d);
                N++;
                if (N % 100 == 0)
                {
                    await unit.Save();
                    Console.Write($"{N} ");
                }
            }
            await unit.Save();
            Console.WriteLine(N);
        }
    }
}