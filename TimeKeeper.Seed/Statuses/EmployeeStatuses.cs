﻿using OfficeOpenXml;
using System;
using System.Threading.Tasks;
using TimeKeeper.DAL;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Seed.Statuses
{
    public static class EmployeeStatuses
    {
        public static async Task Collect(ExcelWorksheet rawData, UnitOfWork unit)
        {
            Console.Write("Employee status: ");
            int N = 0;
            for (int row = 2; row <= rawData.Dimension.Rows; row++)
            {
                EmployeeStatus es = new EmployeeStatus
                {
                    Id = rawData.ReadInteger(row, 1),
                    Type = rawData.ReadInteger(row, 2),
                    Value = rawData.ReadString(row, 3)
                };

                unit.EmployeeStatuses.Insert(es);
                N++;
                if (N % 100 == 0)
                {
                    await unit.Save();
                    Console.Write($"{N} ");
                }
            }
            await unit.Save();
            Console.WriteLine(N);
        }
    }
}