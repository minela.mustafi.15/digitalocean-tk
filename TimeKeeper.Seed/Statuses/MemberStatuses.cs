﻿using OfficeOpenXml;
using System;
using System.Threading.Tasks;
using TimeKeeper.DAL;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Seed.Statuses
{
    public static class MemberStatuses
    {
        public static async Task Collect(ExcelWorksheet rawData, UnitOfWork unit)
        {
            Console.Write("Member status: ");
            int N = 0;
            for (int row = 2; row <= rawData.Dimension.Rows; row++)
            {
                MemberStatus ms = new MemberStatus
                {
                    Id = rawData.ReadInteger(row, 1),
                    Type = rawData.ReadInteger(row, 2),
                    Value = rawData.ReadString(row, 3)
                };

                unit.MemberStatuses.Insert(ms);
                N++;
                if (N % 100 == 0)
                {
                    await unit.Save();
                    Console.Write($"{N} ");
                }
            }
            await unit.Save();
            Console.WriteLine(N);
        }
    }
}