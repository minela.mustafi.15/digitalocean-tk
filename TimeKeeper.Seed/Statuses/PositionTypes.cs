﻿using OfficeOpenXml;
using System;
using System.Threading.Tasks;
using TimeKeeper.DAL;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Seed.Statuses
{
    public static class PositionTypes
    {
        public static async Task Collect(ExcelWorksheet rawData, UnitOfWork unit)
        {
            Console.Write("Position type: ");
            int N = 0;
            for (int row = 2; row <= rawData.Dimension.Rows; row++)
            {
                string oldId = rawData.ReadString(row, 1);
                PositionType pt = new PositionType
                {
                    Type = rawData.ReadInteger(row, 2),
                    Value = rawData.ReadString(row, 3)
                };

                unit.PositionTypes.Insert(pt);
                await unit.Save();
                Utility.dicPositions.Add(oldId, pt.Id);
                N++;
            }
            await unit.Save();
            Console.WriteLine(N);
        }
    }
}