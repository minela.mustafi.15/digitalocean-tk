﻿using OfficeOpenXml;
using System;
using System.Threading.Tasks;
using TimeKeeper.DAL;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Seed.Statuses
{
    public static class CustomerStatuses
    {
        public static async Task Collect(ExcelWorksheet rawData, UnitOfWork unit)
        {
            Console.Write("Customer status: ");
            int N = 0;
            for (int row = 2; row <= rawData.Dimension.Rows; row++)
            {
                CustomerStatus cs = new CustomerStatus
                {
                    Id = rawData.ReadInteger(row, 1),
                    Type = rawData.ReadInteger(row, 2),
                    Value = rawData.ReadString(row, 3)
                };

                unit.CustomerStatuses.Insert(cs);

                N++;
                if (N % 100 == 0)
                {
                    await unit.Save();
                    Console.Write($"{N} ");
                }
            }
            await unit.Save();
            Console.WriteLine(N);
        }
    }
}