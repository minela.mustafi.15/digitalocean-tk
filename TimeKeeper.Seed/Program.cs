﻿using OfficeOpenXml;
using System;
using System.IO;
using TimeKeeper.DAL;
using TimeKeeper.Seed.Statuses;

namespace TimeKeeper.Seed
{
    internal class Program
    {
        private static readonly string fileLocation = @"C:\Users\nelag\Desktop\Projects\TimeKeeper\TimeKeeper.xlsx";

        private static void Main(string[] args)
        {
            FileInfo file = new FileInfo(fileLocation);
            string conStr = "User ID=postgres; Password=postgres; Server=localhost;Port=5432; Database=timekeeper; Integrated Security=true; Pooling=true;";

            ExcelPackage package = new ExcelPackage(file);
            UnitOfWork unit = new UnitOfWork(new TimeKeeperContext(conStr));
            unit.Context.Database.EnsureDeleted();
            unit.Context.Database.EnsureCreated();
            unit.Context.ChangeTracker.AutoDetectChangesEnabled = false;

            var sheets = package.Workbook.Worksheets;

            async void CollectAllSheets()
            {
                await CustomerStatuses.Collect(sheets["CustomerStatuses"], unit);
                await DayTypes.Collect(sheets["DayTypes"], unit);
                await EmployeeStatuses.Collect(sheets["EmployeeStatuses"], unit);
                await PricingTypes.Collect(sheets["PricingTypes"], unit);
                await ProjectStatuses.Collect(sheets["ProjectStatuses"], unit);
                await PositionTypes.Collect(sheets["PositionTypes"], unit);
                await MemberStatuses.Collect(sheets["MemberStatuses"], unit);

                await Teams.Collect(sheets["Teams"], unit);
                await Roles.Collect(sheets["Roles"], unit);
                await Customers.Collect(sheets["Customers"], unit);
                await Projects.Collect(sheets["Projects"], unit);
                await Employees.Collect(sheets["Employees"], unit);
                await Members.Collect(sheets["Engagement"], unit);
                await Calendar.Collect(sheets["Calendar"], unit);
                await Details.Collect(sheets["Details"], unit);
                await Users.Collect(sheets["Employees"], unit);
            }

            CollectAllSheets();

            Console.WriteLine("Seeding database is done.");
            Console.ReadKey();
        }
    }
}