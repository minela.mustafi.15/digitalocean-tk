﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;

namespace TimeKeeper.Seed
{
    public static class Utility
    {
        public static Dictionary<int, int> dicCustomers = new Dictionary<int, int>();
        public static Dictionary<int, int> dicEmployees = new Dictionary<int, int>();
        public static Dictionary<int, int> dicMembers = new Dictionary<int, int>();
        public static Dictionary<int, int> dicProjects = new Dictionary<int, int>();
        public static Dictionary<string, int> dicRoles = new Dictionary<string, int>();
        public static Dictionary<string, int> dicTeams = new Dictionary<string, int>();
        public static Dictionary<string, int> dicPositions = new Dictionary<string, int>();

        public static string ReadString(this ExcelWorksheet sheet, int row, int col)
        {
            try
            {
                return sheet.Cells[row, col].Value.ToString().Trim();
            }
            catch
            {
                return "";
            }
        }

        public static int ReadInteger(this ExcelWorksheet sheet, int row, int col)
        {
            return int.Parse(sheet.Cells[row, col].Value.ToString().Trim());
        }

        public static decimal ReadDecimal(this ExcelWorksheet sheet, int row, int col)
        {
            return decimal.Parse(sheet.Cells[row, col].Value.ToString().Trim());
        }

        public static DateTime ReadDate(this ExcelWorksheet sheet, int row, int col)
        {
            try
            {
                var data = sheet.Cells[row, col].Value;
                if (data == null) return DateTime.MinValue;
                return DateTime.FromOADate(double.Parse(data.ToString()));
            }
            catch
            {
                return new DateTime(1, 1, 1);
            }
        }

        public static string ReadCity(this ExcelWorksheet sheet, int row, int col)
        {
            string places = sheet.ReadString(row, col);
            string[] place = places.Split(", ");
            return place[0];
        }

        public static string ReadCountry(this ExcelWorksheet sheet, int row, int col)
        {
            string places = sheet.ReadString(row, col);
            string[] place = places.Split(", ");
            return place[1];
        }

        public static bool ReadBool(this ExcelWorksheet sheet, int row, int col) => sheet.ReadString(row, col) == "-1";
    }
}