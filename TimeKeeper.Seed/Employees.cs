﻿using OfficeOpenXml;
using System;
using System.Threading.Tasks;
using TimeKeeper.DAL;
using TimeKeeper.Domain;

namespace TimeKeeper.Seed
{
    public static class Employees
    {
        public static async Task Collect(ExcelWorksheet rawData, UnitOfWork unit)
        {
            Console.Write("Employees: ");
            int N = 0;
            for (int row = 2; row <= rawData.Dimension.Rows; row++)
            {
                int oldId = rawData.ReadInteger(row, 1);
                Employee e = new Employee
                {
                    FirstName = rawData.ReadString(row, 2),
                    LastName = rawData.ReadString(row, 3),
                    Image = rawData.ReadString(row, 4),
                    Email = rawData.ReadString(row, 6),
                    Phone = rawData.ReadString(row, 7),
                    BirthDate = rawData.ReadDate(row, 8),
                    Status = await unit.EmployeeStatuses.Get(rawData.ReadInteger(row, 11)),
                    BeginDate = rawData.ReadDate(row, 9),
                    EndDate = rawData.ReadDate(row, 10),
                    Position = await unit.PositionTypes.Get(Utility.dicPositions[rawData.ReadString(row, 12)])
                };

                unit.Employees.Insert(e);
                await unit.Save();
                Utility.dicEmployees.Add(oldId, e.Id);
                N++;
            }
            Console.WriteLine(N);
        }
    }
}