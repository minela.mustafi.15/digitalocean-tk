﻿using OfficeOpenXml;
using System;
using System.Threading.Tasks;
using TimeKeeper.DAL;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.Seed
{
    public static class Users
    {
        public static async Task Collect(ExcelWorksheet rawData, UnitOfWork unit)
        {
            Console.Write("Users: ");
            int N = 0;
            for (int row = 2; row <= rawData.Dimension.Rows; row++)
            {
                User u = new User
                {
                    Name = rawData.ReadString(row, 2) + " " + rawData.ReadString(row, 3),
                    Username = (rawData.ReadString(row, 2) + rawData.ReadString(row, 3).Substring(0, 2)).ToLower(),
                    Password = "$ch00l",
                    Role = "user"
                };

                unit.Users.Insert(u);
                await unit.Save();
                N++;
            }
            Console.WriteLine(N);
        }
    }
}