﻿using OfficeOpenXml;
using System;
using System.Threading.Tasks;
using TimeKeeper.DAL;
using TimeKeeper.Domain;

namespace TimeKeeper.Seed
{
    public static class Members
    {
        public static async Task Collect(ExcelWorksheet rawData, UnitOfWork unit)
        {
            Console.Write("Members: ");
            int N = 0;
            for (int row = 2; row <= rawData.Dimension.Rows; row++)
            {
                Member m = new Member
                {
                    Employee = await unit.Employees.Get(Utility.dicEmployees[rawData.ReadInteger(row, 1)]),

                    Team = await unit.Teams.Get(Utility.dicTeams[rawData.ReadString(row, 2)]),

                    Role = await unit.Roles.Get(Utility.dicRoles[rawData.ReadString(row, 3)]),

                    HoursWeekly = rawData.ReadDecimal(row, 4),

                    Status = await unit.MemberStatuses.Get(rawData.ReadInteger(row, 5))
                };

                unit.Members.Insert(m);
                N++;
            }
            await unit.Save();
            Console.WriteLine(N);
        }
    }
}