import React, { Component } from "react";
import { Provider }from "react-redux";
import configureStore from "./src/redux/configureStore";
import { StyleSheet, SafeAreaView } from "react-native";
import { createAppContainer } from "react-navigation";
import { getRootNavigator } from "./src/navigation";
import Theme from "./src/assets/Theme";
import config from './src/config';
import {retrieveData} from './src/utils/deviceStorage';
import Loader from './src/components/Loader';

export default class App extends Component {
  state = {
    isLoggedIn: false,
    isLoading: true
  }
  componentWillMount(){
    if(config.token === ''){
      retrieveData('token').then(token => {
        if (token){
          config.authHeader.headers.Authorization = 'Bearer ' + token;
          config.token = token;
          this.setState({isLoggedIn: true, isLoading:false});
        }
        else{
          this.setState({isLoggedIn: false, isLoading:false});
        }
      }).catch(err=>{
        console.log(err);
        this.setState({isLoading:false})
      });
    }
  }
  render() {
    const RootNavigator = createAppContainer(getRootNavigator(this.state.isLoggedIn));
    return (
      <SafeAreaView style={styles.container}>
        {!this.state.isLoading && <RootNavigator />}
        {this.state.isLoading && <Loader/>}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Theme.COLORS.black
  }
});
