import {AsyncStorage} from 'react-native';

export const storeData = async (itemName, item) => {
    try {
      // console.log('in store data:', itemName, item);
      await AsyncStorage.setItem(itemName, JSON.stringify(item));
      // console.log('item set');
    } catch (error) {
      // Error saving data
      console.log(error);
    }
  };

  export const retrieveData = async (itemName) => {
    try {
      const value = await AsyncStorage.getItem(itemName);
      if (value !== null) {
        // We have data!!
        // console.log(JSON.parse(value));
        return JSON.parse(value);
      }
    } catch (error) {
      // Error retrieving data
      console.log(error);
    }
  };

  