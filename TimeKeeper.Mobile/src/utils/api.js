import axios from "axios";
import config from '../config';
export const getCalendarMonth = async (id, year, month) => {
    const result = await axios.get('http://192.168.60.71/timekeeper/api/mobile/calendar/' +  id + '/' + year + '/' + month, config.authHeader);
    if (result && result.data) {       
        return result.data;
    }
    return false;
}