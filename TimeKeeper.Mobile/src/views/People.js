import React, { Component } from "react";
import { getNews } from "../server";
import { SafeAreaView, View, FlatList, Text, Button } from "react-native";
import List from "../components/List";
import Constants from 'expo-constants';

import theme from '../assets/Theme';
import { Icon, Header, Left, Body, Right } from 'native-base'
import axios from 'axios';
import config from '../config';
import Loader from '../components/Loader';

const DATA = [
  {
    id: '1',
    title: 'Berina Omerasevic',
    description: 'berkica@gmail.com',
    image: require('../../assets/profiles/BerinaOmerašević.jpg')
  },
  {
    id: '2',
    title: 'Hamza Crnovrsanin',
    description: 'hamzic@gmail.com',
    image: require('../../assets/profiles/HamzaCrnovršanin.jpg')
  },
  {
    id: '3',
    title: 'Ajdin Zorlak',
    description: 'zoka@gmail.com',
    image: require('../../assets/profiles/AjdinZorlak.jpg')
  },
  {
    id: '4',
    title: 'Amina Muzurovic',
    description: 'muzi@gmail.com',
    image: require('../../assets/profiles/AminaMuzurović.jpg')
  },
  {
    id: '5',
    title: 'Faris Spica',
    description: 'spica_u_vodi@gmail.com',
    image: require('../../assets/profiles/FarisŠpica.jpg')
  },
  {
    id: '6',
    title: 'Tajib Smajlovic',
    description: 'tajci_rajif@gmail.com',
    image: require('../../assets/profiles/TajibSmajlović.jpg')

  },
  {
    id: '7',
    title: 'Ferhat Avdic',
    description: 'wannabe_rajif@gmail.com',
    image: require('../../assets/profiles/FerhatAvdić.jpg')
  },
  {
    id: '9',
    title: 'Nadja Sisic',
    description: 'nadja_sisic@gmail.com',
    image: require('../../assets/profiles/NađaŠišić.jpg')

  },
];


export default class People extends Component {
  static navigationOptions = {
    header: null
  }
  state = {
    result: []
  };
  constructor(props) {
    super(props);
    this.state = {
      data: DATA,
      viewState: 'pending'
    };
  }

  async componentDidMount() {
    // const result = await getNews();
    // this.setState({ result });
    console.log(config.authHeader);
    axios.get(config.apiUrl + 'employees', config.authHeader)
    .then(res => {
      let mappedData = res.data.map(x=>{
        return {
          id: x.id,
          name: x.fullName,
          email: x.email
        }
      });
      console.log('employees: ', mappedData);
      this.setState({data: mappedData, viewState: 'success'});
    })
    .catch(err=>{
      console.log(err);
      this.setState({viewState: 'fail'})
    });

  }
  openItem = (item) => {
    this.props.navigation.navigate("LIST-ITEM", { item: item, hasCalendar: true });
  }
  goHome = ()=>{
    this.props.navigation.navigate("Home");
  }

  render() {
    return (
      
      <React.Fragment>
        {this.state.viewState === 'success' && 
        <View style={styles.container}>
          
            <Header style={styles.header}>
              <Left style={styles.left}>
                <Icon size={25} name="ios-menu" onPress={() => this.props.navigation.openDrawer()} />
              </Left>
              <Body style={styles.headerBody}>
              <Text style={styles.headerText}>EMPLOYEES</Text>
              </Body>
              <Right style={styles.right}/>
            </Header>
            <List
              data={this.state.data}
              openItem={this.openItem}
              icon="md-person"
            />
        
    </View>}
    {this.state.viewState === 'pending' && 
      <View style={styles.loaderWrapper}>
        <Loader style={styles.loader}/>
      </View>
    }
    {this.state.viewState === 'fail' && 
      <View style={styles.loaderWrapper}>
        <Text style={styles.unauthorized}>Something went wrong</Text>
        <Button title="Go Back" id="btn-go-home" onPress={this.goHome} color={theme.COLORS.red}/>
      </View>
    }
      </React.Fragment>
    );
  }
}


const styles = {
  unauthorized:{
    fontSize: 24,
    color: theme.COLORS.white,
    marginBottom: 24
  },
  loaderWrapper:{
    flex: 1,
    marginTop: Constants.statusBarHeight,
    backgroundColor: theme.COLORS.black,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
    backgroundColor: theme.COLORS.black,
  },
  header: {
    backgroundColor: 'white',
    alignItems: 'center'
    
  },
  headerText:{
    fontWeight: 'bold',
    fontSize: 24
  },
  left:{
    flex:0,
    marginRight:'auto',
    marginLeft: 10
  },
  headerBody:{
    flex:0,
    marginLeft: -30
  },
  right: {
    flex: 0,
    marginLeft: 'auto'
  }
};
