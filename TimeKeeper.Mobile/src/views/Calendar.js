import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { Agenda } from "react-native-calendars";
import moment from "moment";
import { getCalendarMonth } from "../utils/api";
import config from '../config';
import theme from '../assets/Theme';
import { Icon, Header, Left, Body, Right } from 'native-base'
import Constants from 'expo-constants';
import Loader from '../components/Loader';


export default class Calendar extends React.Component {
  static navigationOptions = {
    header: null
  }
  state = {
    date: this.props.navigation.getParam("date", "null"),
    id: this.props.navigation.getParam("id", "0"),
    items: null,
    viewState: 'pending'
  };
  async componentDidMount() {
    const month = moment(this.state.date).format("MM");
    const year = moment(this.state.date).format("YYYY");
    let data = await getCalendarMonth(this.state.id, year, month);
    if (data !== undefined) {
      let newData = this.parseData(data);
      if (data.length > 0) {
        this.setItems(newData);
        this.setState({viewState: 'success'});
      }
    }else{
      this.setState({viewState: 'fail'});
    }
  }
  setItems(data) {
    this.setState({items: data});
  }
  parseData(DATA) {
    let items = {};
    for (let i = 0; i < DATA.length; i++) {
      let stringDate = moment(DATA[i].date).format("YYYY-MM-DD");
      items[stringDate] === undefined
        ? (items[stringDate] = [
            {
              height: 100,
              name: DATA[i].details[0] ? DATA[i].details[0].name : "No details"
            }
          ])
        : items[stringDate].push({
            height: 100,
            name: DATA[i].details[0] ? DATA[i].details[0].name : "No details"
          });
    }
    return items;
  }
  render() {
    return (
      <React.Fragment>
        {this.state.viewState === 'success' && 
        <View style={styles.container}>
          <Agenda
            items={this.state.items}
            // loadItemsForMonth={this.loadItems.bind(this)}
            selected={this.state.date}
            renderItem={this.renderItem.bind(this)}
            renderEmptyDate={this.renderEmptyDate.bind(this)}
            rowHasChanged={this.rowHasChanged.bind(this)}
          />
        </View>}
        {this.state.viewState === 'pending' && 
          <View style={styles.loaderWrapper}>
            <Loader style={styles.loader}/>
          </View>
        }
        {this.state.viewState === 'fail' && 
          <View style={styles.loaderWrapper}>
            <Text style={styles.unauthorized}>Something went wrong</Text>
            <Button title="Go Back" id="btn-go-home" onPress={this.goHome} color={theme.COLORS.red}/>
          </View>
        }
      </React.Fragment>
    );
  }

  loadItems(day) {
    setTimeout(() => {
      for (let i = -15; i < 85; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = this.timeToString(time);
        if (!this.state.items[strTime]) {
          this.state.items[strTime] = [];
          const numItems = Math.floor(Math.random() * 5);
          for (let j = 0; j < numItems; j++) {
            this.state.items[strTime].push({
              name: "Item for " + strTime,
              height: Math.max(50, Math.floor(Math.random() * 150))
            });
          }
        }
      }
      const newItems = {};
      Object.keys(this.state.items).forEach(key => {
        newItems[key] = this.state.items[key];
      });
      this.setState({
        items: newItems
      });
    }, 1000);
    // console.log(`Load Items for ${day.year}-${day.month}`);
  }

  renderItem(item) {
    return (
      <View style={[styles.item, { height: item.height }]}>
        <Text>{item.name}</Text>
      </View>
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}>
        <Text>This is empty date!</Text>
      </View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split("T")[0];
  }
}

const styles = StyleSheet.create({
  unauthorized:{
    fontSize: 24,
    color: theme.COLORS.white,
    marginBottom: 24
  },
  loaderWrapper:{
    flex: 1,
    marginTop: Constants.statusBarHeight,
    backgroundColor: theme.COLORS.black,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1
  },
  item: {
    backgroundColor: "white",
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30
  },
  header: {
    backgroundColor: 'white',
    alignItems: 'center'
    
  },
  headerText:{
    fontWeight: 'bold',
    fontSize: 24
  },
  left:{
    flex:0,
    marginRight:'auto',
    marginLeft: 10
  },
  headerBody:{
    flex:0,
    marginLeft: -30
  },
  right: {
    flex: 0,
    marginLeft: 'auto'
  }
});