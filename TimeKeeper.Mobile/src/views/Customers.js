import React, { Component } from "react";
import { getNews } from "../server";
import { SafeAreaView, View, FlatList, Text, Button } from "react-native";
import List from "../components/List";
import Constants from 'expo-constants';
import Loader from '../components/Loader';


import { Icon, Header, Left, Body, Right } from 'native-base'
import theme from '../assets/Theme';
import axios from 'axios';
import config from '../config';
const DATA = [
  {
    id: '1',
    title: 'ImageNetConsulting',
    description: 'Project: TK.Mobile'
  },
  {
    id: '2',
    title: 'Big Data Scoring',
    description: 'Project: TK.Web'
  },
  {
    id: '3',
    title: 'New York Times',
    description: 'Project: TK.API'
  },
  {
    id: '4',
    title: 'Fine Art Limited',
    description: 'Project: TK.IDP'
  },
  {
    id: '5',
    title: 'Art Colony',
    description: 'Project: TK.DLL'
  },
];


export default class Customers extends Component {
  static navigationOptions = {
    header: null
  }
  state = {
    result: [],
  };
  constructor(props) {
    super(props);
    this.state = {
      data: DATA,
      viewState: 'pending'
    };
  }

  async componentDidMount() {
    // const result = await getNews();
    // this.setState({ result });
    axios.get(config.apiUrl + 'customers', config.authHeader)
    .then(res => {
      let mappedData = res.data.map(x=>{
        return {
          id: x.id,
          name: x.name,
          email: x.email,
          phone: x.phone,
          status: x.status.name
        }
      });
      console.log('customers: ', mappedData);
      this.setState({data: mappedData, viewState: 'success'});
    })
    .catch(err=>{console.log(err); this.setState({ viewState: 'fail'})});

  }
  openItem = (item) => {
    this.props.navigation.navigate("LIST-ITEM", { item });
  }
  goHome = ()=>{
    this.props.navigation.navigate("Home");
  }

  render() {
    return (
      <React.Fragment>
        {this.state.viewState === 'success' && 
        <View style={styles.container}>
          
        <Header style={styles.header}>
          <Left style={styles.left}>
            <Icon size={25} name="ios-menu" onPress={() => this.props.navigation.openDrawer()} />
          </Left>
          <Body style={styles.headerBody}>
           <Text style={styles.headerText}>CUSTOMERS</Text>
          </Body>
          <Right style={styles.right}/>
        </Header>
        <List
          data={this.state.data}
          openItem={this.openItem}
          icon="md-attach"
        />
      </View>}
    {this.state.viewState === 'pending' && 
      <View style={styles.loaderWrapper}>
        <Loader style={styles.loader}/>
      </View>
    }
    {this.state.viewState === 'fail' && 
      <View style={styles.loaderWrapper}>
        <Text style={styles.unauthorized}>Something went wrong</Text>
        <Button title="Go Back" id="btn-go-home" onPress={this.goHome} color={theme.COLORS.red}/>
      </View>
    }
      </React.Fragment>
    );
  }
}
const styles = {
  unauthorized:{
    fontSize: 24,
    color: theme.COLORS.white,
    marginBottom: 24
  },
  loaderWrapper:{
    flex: 1,
    marginTop: Constants.statusBarHeight,
    backgroundColor: theme.COLORS.black,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1,
    marginTop: Constants.statusBarHeight,
    backgroundColor: theme.COLORS.black,
  },
  header: {
    backgroundColor: 'white',
    alignItems: 'center'
    
  },
  headerText:{
    fontWeight: 'bold',
    fontSize: 24
  },
  left:{
    flex:0,
    marginRight:'auto',
    marginLeft: 10
  },
  headerBody:{
    flex:0,
    marginLeft: -30
  },
  right: {
    flex: 0,
    marginLeft: 'auto'
  }
};
