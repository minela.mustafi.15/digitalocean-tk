import React, { Component } from "react";
import { StyleSheet, Text, View, Image, Button } from "react-native";
import theme from '../assets/Theme';
import { Icon } from 'native-base';

import RNModal from "../components/Modal";
import moment from "moment";
import DateTimePicker from "react-native-modal-datetime-picker";
import Constants from 'expo-constants';


export default class SingleItemView extends Component {
  static navigationOptions = {
    // title: 'PROFILE',
    headerStyle: {
      backgroundColor: theme.COLORS.red,
      elevation: 0,
      shadowOpacity: 0,
      borderBottomWidth: 0,
    },
    headerTintColor: theme.COLORS.white,
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };
  state={
    open: false,
    isDateTimePickerVisible: false,
    stringDate: null,
    date: null,
    employeeData: this.props.navigation.getParam('item')
  }
  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  handleOpenCalendar = () => {
    this.setState({ open: false });
    this.props.navigation.navigate("Calendar", {
      date: this.state.date,
      id: this.state.employeeData.id
    });
  };
  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };

  handleDatePicked = date => {
    this.setState({ date: date });
    console.log("A date picked: ", date);
    stringDate = JSON.parse(JSON.stringify(moment(date).format("MMM Do YY")));
    this.setState({ stringDate: stringDate });
    this.hideDateTimePicker();
  };
  render() {
    const { id, title, description, name, email, phone, status, customer, team, image} = this.props.navigation.getParam('item');
    const hasCalendar = 
    this.props.navigation.getParam('hasCalendar')
    ? this.props.navigation.getParam('hasCalendar')
    : false;

    return (
      <React.Fragment>
        <View style={styles.container} id="item-container">
        <View style={styles.header} id="item-header">
          {image && <Image style={styles.image} id="item-image" source={image}/>}
          {name && <Text style={styles.title} id="item-name"> {name} </Text>}
          {title && <Text style={styles.title} id="item-title"> {title} </Text>}
          {hasCalendar && <Button color={theme.COLORS.black} id="btn-calendar" title="See Calendar" onPress={this.handleOpen}/>}
        </View>
        <View style={styles.body} id="item-body">
          {description && <Text style={styles.text} id="item-description"> {description}</Text>}
          {email && <Text style={styles.text} id="item-email"> {email}</Text>}
          {phone && <Text style={styles.text}>{phone}</Text>}
          {status && <Text style={styles.text}>{status}</Text>}
          {customer && <Text style={styles.text}><Icon name="md-attach" size={14} /> {customer}</Text>}
          {team && <Text style={styles.text}><Icon name="md-people" size={14} /> {team}</Text>}
        </View>
      </View>
      <RNModal visible={this.state.open} onClose={this.handleClose}>
      <View style={styles.modal}>
        <Button color={theme.COLORS.red} title= {!this.state.date ? "Select date" : "Change date"} onPress={this.showDateTimePicker}/>
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />
        <View style={styles.pickedDate}>
          {!this.state.date ? null : (
            <Text>Picked date {this.state.stringDate}</Text>
          )}
        </View>
        {this.state.date ? (
          <Button color={theme.COLORS.red} onPress={this.handleOpenCalendar} title="open"/>
        ) : null}
      </View>
    </RNModal>
      </React.Fragment>
    );
  }
}
const rem = 16;
const styles = StyleSheet.create({
  pickedDate:{
    marginVertical: 24
  },
  modal:{
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    backgroundColor: theme.COLORS.beige,
    height: '100%'
  },
  header:{
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.COLORS.red,
    // paddingTop: 3*rem,
    paddingBottom: 3*rem,
    paddingLeft: rem,
    paddingRight: rem
  },
  image:{
    width: 120,
    height: 120,
    borderRadius: 60,
    borderColor: theme.COLORS.white,
    borderWidth: 2,
    shadowOpacity: 2
  },
  title:{
    fontSize: 2*rem,
    marginTop: rem,
    marginBottom: rem,
    color: theme.COLORS.white
  },
  body:{
    // backgroundColor: theme.COLORS.beige,
    paddingTop: 1.5*rem,
    paddingBottom:1.5*rem,
    paddingLeft: rem,
    paddingRight: rem
  },
  text:{
    fontSize: rem,
    marginBottom: rem
  }

});

