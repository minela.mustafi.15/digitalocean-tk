import React, { Component } from 'react';
import { View, Image,Text } from 'react-native';
import { Button } from '../components';
import {Input} from '../components';
import axios from 'axios';
import config from '../config';
import {storeData, retrieveData} from '../utils/deviceStorage';
export default class Login extends Component {
  static navigationOptions = {
    header: null
  }
  state = {
    user: null,
    body: null,
    login: {
      username: null,
      password: null
    },
    errors: {
      username: false,
      password: false
    }
  }
  login = (credentials) => {
    // console.log('login ran');
    axios.post('http://192.168.60.74/timekeeper/login-mobile', credentials
    // {
    //   "username": "michaeljo",
    //   "password": "$ch00l"
    // }
    )
    .then(res=>{
      // console.log('in login response');
      config.authHeader.headers.Authorization = 'Bearer ' + res.data.token;
      config.token = res.data.token;
      storeData('user', res.data.user);
      storeData('token', res.data.token);
      // console.log(config.authHeader.Authorization);
      // console.log(res.data.user);
      // console.log(res.data.token);
      // retrieveData('user').then(user => {
      //   console.log(user);
      // }).catch(err=>{ppđ
      //   console.log(err);
      // })
      // console.log(config.token);

      this.setState({user: res.data.user});
      this.props.navigation.navigate("LoggedInRoutes");
    })
    .catch(err=>{
      console.log(err);
      // console.log('in catch');
    });
    // axios.get('https://192.168.44.148:44350/api/mobile/employees', config.authHeader)
    // .then(res=>{
    //   console.log(res);
    //   this.setState({body: res});
    // })
    // .catch(err=>{
    //   console.log(err);
    // })
  }
  handleChange = (prop, text) => {
    /// VALIDATE REQUIRED AND UPDATE ERRORS
    if(!this.state.login[prop] || this.state.login[prop] === ''){
      this.setState(prevState => ({errors: {
        ...prevState.errors,
        [prop]: true
      }}
      ));
    }else{
      this.setState(prevState => ({errors: {
        ...prevState.errors,
        [prop]: false
      }}
      ));
    }
    console.log('in handle change');
    this.setState(prevState=>({
      login:{
      ...prevState.login,
      [prop]: text
      } 
  }));
  }
  handleSubmit = (event) => {
    if(!this.state.login.username || this.state.login.username === ''){
      this.setState(prevState => ({errors: {
        ...prevState.errors,
        username: true
      }}
      ));
    }else{
      this.setState(prevState => ({errors: {
        ...prevState.errors,
        username: false
      }}
      ));
    }
    if(!this.state.login.password || this.state.login.password === ''){
      this.setState(prevState => ({errors: {
        ...prevState.errors,
        password: true
      }}
      ));
    }else{
      this.setState(prevState => ({errors: {
        ...prevState.errors,
        password: false
      }}
      ));
    }
    /// CHECK ERRORS
    console.log(this.state.login);
    console.log(this.state.errors);
    if(!this.state.errors.username && !this.state.errors.password){
      this.login(this.state.login);
    }
    event.preventDefault();
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.mainTitle}>Log into TimeKeeper</Text>
        
        <Input 
        style={styles.username} 
        placeholder="Username" 
        value={this.state.login.username} 
        onChangeText={(text) => this.handleChange("username", text)} 
        autoCompleteType='username'>
        </Input>
        {this.state.errors.username && <Text>Username is required.</Text>}
        <Input style={styles.password}  
        placeholder="Password" 
        secureTextEntry={true}
        value={this.state.login.password} 
        onChangeText={(text) => this.handleChange("password", text)}
        autoCompleteType='password'>
        </Input>
        {this.state.errors.password && <Text>Password is required.</Text>}
        <Button onPress={this.handleSubmit} outline>Login</Button> 
        
        <Text>{this.state.body}</Text>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1, 
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10
  },
  logo:{
    marginTop:10
  },
  mainTitle: {
    fontFamily: 'Roboto',
    fontSize:25,    
    fontWeight: 'bold',
    marginTop:10,

  },
  title:{
    marginTop:10,
    marginBottom:30,
    fontSize:20, 
  },
 
}