import React, { Component } from 'react';
import { View, Image, Text, ImageBackground,Button } from 'react-native';
// import { Button } from '../components';
import { Input } from '../components'
import Puzzle from '../../assets/puzzle.png'
import BackgroundImage from '../../assets/clockwork.jpg'
import theme from '../assets/Theme';
import {AsyncStorage} from 'react-native';
 
export default class Welcome extends Component {
    logout = async () => {
        await AsyncStorage.multiRemove(['token', 'user'], (err)=> {
            if(err) console.log(err);        
            this.props.navigation.navigate("Login");
        });
    }
    render() {
        return (
            <View >
                <ImageBackground source={BackgroundImage} style={styles.background}>
                    {/* <Image style={styles.logo}
                        source={Puzzle}
                    /> */}
                    <Text style={styles.mainTitle}>TimeKeeper</Text>
                    <Text style={styles.title}> Overview the databases</Text>
                    <Text style={styles.title}> Check employee records</Text>
                    <Button color={theme.COLORS.black} id="btn-logout" title="Log out" onPress={this.logout}/>
                </ImageBackground>
            </View>
        )
    }
}

const styles = {
    container: {
        flex: 1,

        backgroundColor: 'white',
        
        padding: 10
    },
    logo: {
        marginTop: 150,
        marginLeft:30
    },
    mainTitle: {
        fontSize: 30,
        fontWeight: 'bold',
        marginTop: 50,
        color:'white'

    },
    title: {
        marginTop: 10,
        fontSize: 20,
        color:'white'
    },
    background: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100%',

    }

}