import { createBottomTabNavigator } from "react-navigation-tabs";
import { createStackNavigator } from "react-navigation-stack";
import People from "../views/People";
// import Profile from "../views/Profile";
import SingleItemView from '../views/SingleItemView';
import Projects from "../views/Projects";
import Customers from "../views/Customers";
import Teams from "../views/Teams";
import Calendar from "../views/Calendar";
import Welcome from "../views/Welcome";
import Login from '../views/Login';
import { createDrawerNavigator } from "react-navigation-drawer";
import theme from "../assets/Theme";
import React from "react";
import { Icon } from "native-base";
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const StackNavigator = createStackNavigator(
  {
    Profile: {
      screen: SingleItemView
    },
    Calendar: {
      screen: Calendar
    }
  },
  {
    drawerStyle: {
      backgroundColor: "#c6cbef",
      width: 240
    }
  }
);

const StackNavigatorEmployee = createStackNavigator({
  EMPLOYEES: {
    screen: People
  },
  "LIST-ITEM": {
    screen: StackNavigator
  }
});

const DrawerNavigator = createDrawerNavigator({
  EMPLOYEES: {
    screen: StackNavigatorEmployee
  },
  CUSTOMERS: {
    screen: Customers
  },
  PROJECTS: {
    screen: Projects
  },
  TEAMS: {
    screen: Teams
  }
});

const LoggedInRoutes = createBottomTabNavigator(
  {
    Home: {
      screen: Welcome,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-home" style={{color: tintColor}} size={25} />
        )
      }
    },
    Database: {
      screen: DrawerNavigator,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-menu" style={{color: tintColor}} size={25} />
        )
      }
    }
  },
  {
    tabBarOptions: {
      showLabel: false,
      activeTintColor: theme.COLORS.white,
      inactiveTintColor: theme.COLORS.white,
      inactiveBackgroundColor: theme.COLORS.black,
      activeBackgroundColor: theme.COLORS.red,
      labelStyle: {
        fontSize: 16,
        marginBottom: 16
      },
      showIcon: true,
      style:{
        borderTopWidth: 0
      }
    }
  }
);

export default LoggedInRoutes;
