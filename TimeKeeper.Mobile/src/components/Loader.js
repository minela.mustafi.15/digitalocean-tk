import React from "react";
import { View } from "react-native";
import { ActivityIndicator } from "react-native";
import theme from '../assets/Theme';

const Loader = ({ size = 100, color = theme.COLORS.red }) => (
  <View>
    <ActivityIndicator size={size} color={color} />
  </View>
);

export default Loader;
