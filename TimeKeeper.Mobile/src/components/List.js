import React from 'react';
import {
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Text, Image
} from 'react-native';
import Constants from 'expo-constants';
import {Item} from './Item';
import theme from '../assets/Theme';

export default function App(props) {
  const {data, openItem, icon, dataLayout} = props;
  return (
      <FlatList
        data={data}
        renderItem={({ item }) => (
          <Item
            item={item}
            openItem={openItem}
            icon={icon}
          />
        )}
        keyExtractor={item => item.id + ''}
      />
  );
}
 