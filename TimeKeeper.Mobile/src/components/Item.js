import React from 'react';
import {
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Text, Image, View
} from 'react-native';
import { Icon } from 'native-base';
import Constants from 'expo-constants';
import theme from '../assets/Theme';

function Item({ item, openItem, icon }) {
  const { id, title, description, name, email, phone, status, customer, team} = item;
  return (
    <TouchableOpacity
    onPress={() => openItem(item)}
      style={styles.item} 
    >
      <Icon name={icon} style={styles.icon}/>
      <View style={styles.info}>
        {title && <Text style={styles.title}>{title}</Text>}
        {name && <Text style={styles.title}>{name}</Text>}
        {description && <Text style={[styles.text, {textAlign: 'justify'}]}>{description}</Text>}
        {email && <Text style={styles.text}>{email}</Text>}
        {phone && <Text style={styles.text}>{phone}</Text>}
        {status && <Text style={styles.text}>{status}</Text>}
        {customer && <Text style={styles.text}><Icon name="md-attach" size={14} /> {customer}</Text>}
        {team && <Text style={styles.text}><Icon name="md-people" size={14} /> {team}</Text>}
      </View>
      <Icon name="md-arrow-dropright" size={20} style={styles.openItemIcon} />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: theme.COLORS.white,
    padding: 20,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: theme.COLORS.grey
  },
  info:{
    flexDirection: 'column',
    flex: 1
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black'
  },
  text: {
    fontSize: 14,
    color: 'black',
    marginBottom: 7
  },
  image: {
    width: 40,
    height: 40
  },
  icon:{
    marginRight: 20
  },
  openItemIcon:{
    marginLeft: 20
  }
});
export { Item };