export default { 
    COLORS: {
      DEFAULT: 'rgba(255,255,255,0.25)',
      PRIMARY: 'rgba(31,178,204,1)',
      WHITE: '#fff',
      TRANSPARENT: 'rgba(247,247,247)',
      BUTTONCOLOR: '#26a69a',
      LISTCOLOR:'rgb(57, 54, 67)',

      black: 'rgba(15, 17, 8, 1)',
      // white: 'rgba(255, 248, 240, 1)',
      white: '#fff',
      red: 'rgba(182, 30, 36, 1)',
      beige: 'rgba(221, 213, 208, 1)',
      grey: 'rgba(42, 76, 85, 1)'
    }
  }