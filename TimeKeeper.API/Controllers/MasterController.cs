﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TimeKeeper.DTO.Factories;
using TimeKeeper.DAL;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasterController : BaseController
    {
        public MasterController(TimeKeeperContext context, IDeltaLog log) : base(context, log) { }

        /// <summary>
        /// Fetches the list of teams
        /// </summary>
        /// <returns>Team list</returns>
        [HttpGet("teams")]
        public IActionResult GetTeams() => Ok(Unit.Teams.Get().Result.Select(x => x.Master()).ToList());

        /// <summary>
        /// Fetches the list of roles
        /// </summary>
        /// <returns>Role list</returns>
        [HttpGet("roles")]
        public IActionResult GetRoles() => Ok(Unit.Roles.Get().Result.Select(x => x.Master()).ToList());

        /// <summary>
        /// Fetches the list of customers
        /// </summary>
        /// <returns>Customer list</returns>
        [HttpGet("customers")]
        public IActionResult GetCustomers() => Ok(Unit.Customers.Get().Result.Select(x => x.Master()).ToList());

        /// <summary>
        /// Fetches the list of projects
        /// </summary>
        /// <returns>Project list</returns>
        [HttpGet("projects")]
        public IActionResult GetProjects() => Ok(Unit.Projects.Get().Result.Select(x => x.Master()).ToList());

        /// <summary>
        /// Fetches the list of employees
        /// </summary>
        /// <returns>Employee list</returns>
        [HttpGet("employees")]
        public IActionResult GetEmployees() => Ok(Unit.Employees.Get().Result.Select(x => x.Master()).ToList());

        /// <summary>
        /// Fetches the list of days
        /// </summary>
        /// <returns>Day list</returns>
        [HttpGet("calendar")]
        public IActionResult GetCalendar() => Ok(Unit.Calendar.Get().Result.Select(x => x.Master()).ToList());

        /// <summary>
        /// Fetches the list of tasks
        /// </summary>
        /// <returns>Task list</returns>
        [HttpGet("tasks")]
        public IActionResult GetDetails() => Ok(Unit.Details.Get().Result.Select(x => x.Master()).ToList());

        /// <summary>
        /// Fetches the list of members
        /// </summary>
        /// <returns>Member list</returns>
        [HttpGet("members")]
        public IActionResult GetMembers() => Ok(Unit.Members.Get().Result.Select(x => x.Master("team")).ToList());
    }
}