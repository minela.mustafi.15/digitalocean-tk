﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.API.Services;
using TimeKeeper.DAL;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Factories;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : BaseController
    {
        public CustomersController(TimeKeeperContext context, IDeltaLog log) : base(context, log)
        {
        }

        /// <summary>
        /// Fetches all the data of type Customer
        /// </summary>
        /// <returns>List of type calendar</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy ="IsAdmin")]
        public async Task<IActionResult> GetAll(int page = 1, int pageSize = 10000)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                if (role == "user") return Unauthorized("Unauthorized");
                var query = await Unit.Customers.Get();

                Log.Information("Fetching list of customers");
                var pagination = PaginationService<Customer>.Pagination(ref query, page, pageSize);
                HttpContext.Response.Headers.Add("pagination", JsonConvert.SerializeObject(pagination));
                return Ok(query.ToList().Select(x => x.Create()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns single customer entry
        /// </summary>
        /// <param name="id">Used to fetch entry by id</param>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [Authorize(Policy = "IsAdmin")]
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                Log.Information($"Fetching customer with id {id}");
                Customer customer = await Unit.Customers.Get(id);
                if (customer == null)
                {
                    Log.NotFound($"Requested resource with {id} does not exist");
                    return NotFound($"Requested resource with {id} does not exist");
                }
                return Ok(customer.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Posts single entry of type customer
        /// </summary>
        /// <param name="customer">Object of type customer to be inserted</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [Authorize(Policy = "IsAdmin")]
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] Customer customer)
        {
            try
            {
                Log.Information("Try to add new customer");
                Unit.Customers.Insert(customer);
                await Unit.Save();
                return Ok(customer.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Updates existing entry of type DCustomer
        /// </summary>
        /// <param name="id">Identifies an entry</param>
        /// <param name="customer">Provides data body</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [Authorize(Policy = "IsAdmin")]
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Put(int id, [FromBody] Customer customer)
        {
            try
            {
                Log.Information($"Try to change customer with the id {id}");
                Unit.Customers.Update(customer, id);
                await Unit.Save();
                return Ok(customer.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes an entry
        /// </summary>
        /// <param name="id">Used to fetch an entry of type Customer for deletion</param>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [Authorize(Policy = "IsAdmin")]
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Log.Information($"Try to delete customer with the id {id}");
                Customer c = await Unit.Customers.Get(id);
                Unit.Customers.Delete(id);
                await Unit.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns a list of statuses for this master model, or throws a Bad request.
        /// Serves to expose dependent entity
        /// </summary>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [Route("statuses")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetCustomerStatuses()
        {
            try
            {
                var query = await Unit.CustomerStatuses.Get();
                var statuses = query.ToList().Select(x => x.Master()).ToList();
                return Ok(statuses);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }
    }
}