//using Microsoft.AspNetCore.Authentication;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.IdentityModel.Protocols.OpenIdConnect;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Security.Claims;
//using System.Text;
//using System.Threading.Tasks;
//using TimeKeeper.DAL;
//using TimeKeeper.Domain.Entities;
//using TimeKeeper.Utilities.Logging;

//namespace TimeKeeper.API.Controllers
//{
//    [ApiController]
//    public class UsersController : BaseController
//    {
//        public UsersController(TimeKeeperContext context, IDeltaLog log) : base(context, log) { }

//        [HttpGet]
//        [Route("/api/users")]
//        public IActionResult Get()
//        {
//            var currentUser = HttpContext.User as ClaimsPrincipal;
//            List<string> claims = new List<string>();
//            foreach (Claim claim in currentUser.Claims) claims.Add(claim.Value);
//            var users = Unit.Users.Get();
//            return Ok(new { claims, users });
//        }



//        [NonAction]
//        private void LogIdentity()
//        {
//            var identityToken = HttpContext.GetTokenAsync(OpenIdConnectParameterNames.IdToken);
//            Log.Information($"Identity token: {identityToken.Result}");
//            foreach (var claim in User.Claims)
//            {
//                Log.Information($"Claim type: {claim.Type} - value: {claim.Value}");
//            }
//        }

//        [HttpGet]
//        [Route("/login-web")]
//        [Authorize]
//        public IActionResult Login()
//        {
//            if (User.Identity.IsAuthenticated)
//            {
//                var accessToken = HttpContext.GetTokenAsync(OpenIdConnectParameterNames.AccessToken).Result;
//                var response = new
//                {
//                    Id = User.Claims.FirstOrDefault(c => c.Type == "sub").Value.ToString(),
//                    Name = User.Claims.FirstOrDefault(c => c.Type == "given_name").Value.ToString(),
//                    Role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString(),
//                    accessToken // Bearer {accessToken}
//                };
//                return Ok(response);
//            }
//            else
//            {
//                return NotFound();
//            }
//        }

//        [AllowAnonymous]
//        [Route("/logout-web")]
//        [HttpGet]
//        public async Task Logout()
//        {
//            if (User.Identity.IsAuthenticated)
//            {
//                await HttpContext.SignOutAsync("Cookies");
//                await HttpContext.SignOutAsync("oidc");
//            }
//        }
//    }
//}