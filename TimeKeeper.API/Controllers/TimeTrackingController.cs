﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.BLL;
using TimeKeeper.DAL;
using TimeKeeper.DTO.Factories;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TimeTrackingController : BaseController
    {
        protected TimeTrackingService Tracker;

        public TimeTrackingController(TimeKeeperContext context, IDeltaLog logger) : base(context, logger)
        {
            Tracker = new TimeTrackingService(Unit);
        }

        /// <summary>
        /// Returns employee time tracking module payload
        /// </summary>
        /// <param name="empId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet("employee/{empId}/{year}/{month}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> EmployeeTracker(int empId, int year, int month)
        {
            try
            {
                return Ok(await Tracker.EmployeeTimeTracking(empId, year, month));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns time tracking payload for team selected
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet("team/{teamId}/{year}/{month}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOrLeader")]
        public async Task<IActionResult> TeamTracker(int teamId, int year, int month)
        {
            try
            {
                return Ok(await Tracker.TeamTimeTracking(teamId, year, month));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Employee month
        /// </summary>
        /// <param name="empId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet("employee/{empId}/{year}/{month}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetEmployeeMonth(int empId, int year, int month)
        {
            try
            {
                return Ok(await Tracker.GetEmployeeMonth(empId, year, month));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Employee single day
        /// </summary>
        /// <param name="empId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        [HttpGet("employee-day/{empId}/{year}/{month}/{day}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetEmployeeDay(int empId, int year, int month, int day)
        {
            try
            {
                var query = await Unit.Calendar.Get(x => x.Employee.Id == empId && x.Date.Year == year && x.Date.Month == month && x.Date.Day == day);
                return Ok(query.Select(x => x.Create()));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }
    }
}