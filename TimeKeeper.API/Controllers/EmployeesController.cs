﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.API.Services;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.DTO.Factories;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : BaseController
    {
        public EmployeesController(TimeKeeperContext context, IDeltaLog log) : base(context, log)
        {
        }

        /// <summary>
        /// Fetches all data of type Employee
        /// </summary>
        /// <returns>List of type Employee</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAll(int page = 1, int pageSize = 10000)
        {
            try
            {
                var query = await Unit.Employees.Get();
                Log.Information($"Try to fetch a list of employees");

                var pagination = PaginationService<Employee>.Pagination(ref query, page, pageSize);
                HttpContext.Response.Headers.Add("pagination", JsonConvert.SerializeObject(pagination));
                return Ok(query.ToList().Select(x => x.Create()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns single Employee entry
        /// </summary>
        /// <param name="id">Used to fetch entry by id</param>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                Log.Information($"Try to fetch employee with the id {id}");
                Employee employee = await Unit.Employees.Get(id);
                if (employee == null)
                {
                    Log.NotFound($"Requested resource with {id} does not exist");
                    return NotFound($"Requested resource with {id} does not exist");
                }
                if ((role =="user" || role == "lead") &&  employee.Id!=userid)
                {
                    return Unauthorized();
                }
                else
                return Ok(employee.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Posts single entry of type Employee
        /// </summary>
        /// <param name="employee">Object of type employee to be inserted</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [Authorize(Policy = "IsAdmin")]
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] Employee employee)
        {
            try
            {
                Log.Information("Try to add a new employee");
                Unit.Employees.Insert(employee);
                await Unit.Save();
                return Ok(employee.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Updates existing entry of type Employee
        /// </summary>
        /// <param name="id">Identifies an entry</param>
        /// <param name="employee">Provides data body</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> Put(int id, [FromBody] Employee employee)
        {
            try
            {
                Log.Information($"Try to change employee with the id {id}");
                Unit.Employees.Update(employee, id);
                await Unit.Save();
                return Ok(employee.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes an entry
        /// </summary>
        /// <param name="id">Used to fetch an entry of type Employee for deletion</param>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [Authorize(Policy = "IsAdmin")]
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Log.Information($"Try to delete employee with the id {id}");
                Unit.Employees.Delete(id);
                await Unit.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns a list of statuses for this master model, or throws a Bad request.
        /// Serves to expose dependent entity
        /// </summary>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [Route("statuses")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetEmployeeStatuses()
        {
            try
            {
                var query = await Unit.EmployeeStatuses.Get();
                var statuses = query.ToList().Select(x => x.Master()).ToList();
                return Ok(statuses);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }

        /// <summary>
        /// Returns a list of types for this master model, or throws a Bad request.
        /// Serves to expose dependent entity
        /// </summary>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [Route("positions")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetEmployeePositions()
        {
            try
            {
                var query = await Unit.PositionTypes.Get();
                var statuses = query.ToList().Select(x => x.Master()).ToList();
                return Ok(statuses);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }
    }
}