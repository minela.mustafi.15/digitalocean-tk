﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.API.Services;
using TimeKeeper.DAL;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Factories;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : BaseController
    {
        public ProjectsController(TimeKeeperContext context, IDeltaLog log) : base(context, log)
        {
        }

        /// <summary>
        /// Fetches all data of type Project
        /// </summary>
        /// <returns>List of type Project</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy ="AdminOrLeader")]
        public async Task<IActionResult> GetAll(int page = 1, int pageSize = 10000)
        {
            try
            {
                Log.Information("Fetching list of projects");
                var query = await Unit.Projects.Get();
                var pagination = PaginationService<Project>.Pagination(ref query, page, pageSize);
                HttpContext.Response.Headers.Add("pagination", JsonConvert.SerializeObject(pagination));

                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());
                if (role == "lead")
                {
                    query = await Unit.Projects.Get(x => x.Team.Members.Any(y => y.Employee.Id == userid));
                    return Ok(query.ToList().Select(x =>x.Create()).ToList());
                }
                else
                return Ok(query.ToList().Select(x => x.Create()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns single Project entry
        /// </summary>
        /// <param name="id">Used to fetch entry by id</param>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [Authorize(Policy = "AdminOrLeader")]
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());
                Log.Information($"Fetching project with id {id}");
                Project project = await Unit.Projects.Get(id);
                if (project == null)
                {
                    Log.NotFound($"Requested resource with {id} does not exist");
                    return NotFound($"Requested resource with {id} does not exist");
                }
                if (role == "lead" && !project.Team.Members.Any(x => x.Employee.Id == userid))
                {
                    return Unauthorized();
                }
                return Ok(project.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Posts single entry of type Project
        /// </summary>
        /// <param name="project">Object of type Project to be inserted</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [Authorize(Policy = "IsAdmin")]
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] Project project)
        {
            try
            {
                Log.Information($"Trying to add new project");
                Unit.Projects.Insert(project);
                await Unit.Save();
                return Ok(project.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Updates existing entry of type Project
        /// </summary>
        /// <param name="id">Identifies an entry</param>
        /// <param name="project">Provides data body</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [Authorize(Policy = "IsAdmin")]
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Put(int id, [FromBody] Project project)
        {
            try
            {
                Log.Information($"Trying to update project with id {id}");
                Unit.Projects.Update(project, id);
                await Unit.Save();
                return Ok(project.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes an entry
        /// </summary>
        /// <param name="id">Used to fetch an entry of type Project for deletion</param>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad RequestS</response>
        [Authorize(Policy = "IsAdmin")]
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Log.Information($"Trying to delete project with id {id}");
                Unit.Projects.Delete(id);
                await Unit.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns a list of types for this master model, or throws a Bad request.
        /// Serves to expose dependent entity
        /// </summary>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [Route("pricing")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetPricingTypes()
        {
            try
            {
                var query = await Unit.PricingTypes.Get();
                var statuses = query.ToList().Select(x => x.Master()).ToList();
                return Ok(statuses);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }

        /// <summary>
        /// Returns a list of types for this master model, or throws a Bad request.
        /// Serves to expose dependent entity
        /// </summary>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [Route("statuses")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetProjectStatuses()
        {
            try
            {
                var query = await Unit.ProjectStatuses.Get();
                var statuses = query.ToList().Select(x => x.Master()).ToList();
                return Ok(statuses);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }
    }
}