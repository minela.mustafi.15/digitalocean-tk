﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using TimeKeeper.API.Services;
using TimeKeeper.BLL;
using TimeKeeper.DAL;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Authorize(AuthenticationSchemes ="TokenAuthentication")]
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected UnitOfWork Unit;
        protected static IDeltaLog Log;
        protected readonly AccessHandler Access;
        protected StoredProcedureService StoredProcedures;

        public BaseController(TimeKeeperContext context, IDeltaLog log)
        {
            Unit = new UnitOfWork(context);
            Log = log;
            Access = new AccessHandler(Unit);
            StoredProcedures = new StoredProcedureService(Unit);
        }

        [NonAction]
        public IActionResult HandleException(Exception e)
        {
            Log.BadRequest(e.Message);
            return BadRequest(e);
        }
    }
}