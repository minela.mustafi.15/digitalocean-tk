﻿using Microsoft.AspNetCore.Mvc;
using System;
using TimeKeeper.API.Services;
using TimeKeeper.DTO.Models;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : ControllerBase
    {
        /// <summary>
        /// This controller serves only to send contact request mail
        /// </summary>
        /// <param name="mail"></param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [HttpPost]
        public IActionResult PostEmail([FromBody] MailModel mail)
        {
            try
            {
                string mailTo = mail.Email;
                string subject = $"Contact request from {mail.Name}";
                string body = $"{mail.Message}, {mail.Phone}";
                MailService.Send(mailTo, subject, body);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}