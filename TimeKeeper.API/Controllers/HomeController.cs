﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace TimeKeeper.API.Controllers
{
    [AllowAnonymous]
    [Route("/")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        public IActionResult Get()
        {
            return Ok("Welcome to Home page");
        }

        [HttpGet("AccessDenied")]
        public IActionResult AccessDenied()
        {
            return Ok("It seems that you do not have access to this page!");
        }
    }
}