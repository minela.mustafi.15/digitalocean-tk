﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.API.Services;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.DTO.Factories;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : BaseController
    {
        public RolesController(TimeKeeperContext context, IDeltaLog log) : base(context, log)
        {
        }

        /// <summary>
        /// Fetches all data of type Role
        /// </summary>
        /// <returns>List of type Role</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy ="IsAdmin")]
        public async Task<IActionResult> GetAll(int page = 1, int pageSize = 10000)
        {
            try
            {
                Log.Information("Fetching a list of available roles.");
                var query = await Unit.Roles.Get();
                var pagination = PaginationService<Role>.Pagination(ref query, page, pageSize);
                HttpContext.Response.Headers.Add("pagination", JsonConvert.SerializeObject(pagination));
                var result = query.ToList().Select(x => x.Create()).ToList();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns single Role entry
        /// </summary>
        /// <param name="id">Used to fetch entry by id</param>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                Log.Information($"Fetching role with id of {id}");
                Role role = await Unit.Roles.Get(id);
                if (role == null)
                {
                    Log.NotFound($"Requested resource with {id} does not exist");
                    return NotFound($"Requested resource with {id} does not exist");
                }
                return Ok(role.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Posts single entry of type Role
        /// </summary>
        /// <param name="role">Object of type Role to be inserted</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> Post([FromBody] Role role)
        {
            try
            {
                Log.Information("Trying to insert new role");
                Unit.Roles.Insert(role);
                await Unit.Save();
                return Ok(role.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Updates existing entry of type Role
        /// </summary>
        /// <param name="id">Identifies an entry</param>
        /// <param name="role">Provides data body</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> Put(int id, [FromBody] Role role)
        {
            try
            {
                Log.Information($"Trying to update role with id {id}");
                Unit.Roles.Update(role, id);
                await Unit.Save();
                return Ok(role.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes an entry
        /// </summary>
        /// <param name="id">Used to fetch an entry of type Role for deletion</param>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Log.Information($"Trying to delete role with id: {id}");
                Unit.Roles.Delete(id);
                await Unit.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }
    }
}