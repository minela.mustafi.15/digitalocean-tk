﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TimeKeeper.BLL;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.DTO.Factories;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MobileController : BaseController
    {
        protected TimeTrackingService Tracker;

        public MobileController(TimeKeeperContext context, IDeltaLog logger) : base(context, logger)
        {
            Tracker = new TimeTrackingService(Unit);
        }

        /// <summary>
        /// Fetches all the data of type Customer
        /// </summary>
        /// <returns>List of type calendar</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [AllowAnonymous]
        [HttpGet("customers")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAllCustomers()
        {
            try
            {
                var query = await Unit.Customers.Get();
                return Ok(query.ToList().Select(x => x.CreateMobile()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Fetches all data of type Employee
        /// </summary>
        /// <returns>List of type Employee</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [AllowAnonymous]
        [HttpGet("employees")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAllEmployees()
        {
            try
            {
                var query = await Unit.Employees.Get();
                return Ok(query.ToList().Select(x => x.Create()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Fetches all data of type Project
        /// </summary>
        /// <returns>List of type Project</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [AllowAnonymous]
        [HttpGet("projects")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAllProjects()
        {
            try
            {
                var query = await Unit.Projects.Get();
                return Ok(query.ToList().Select(x => x.CreateMobile()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Fetches all data of type Role
        /// </summary>
        /// <returns>List of type Role</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [AllowAnonymous]
        [HttpGet("roles")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAllRoles()
        {
            try
            {
                var query = await Unit.Roles.Get();
                return Ok(query.ToList().Select(x => x.Create()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Fetches all data of type Team
        /// </summary>
        /// <returns>List of type Team</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [AllowAnonymous]
        [HttpGet("teams")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAllTeams()
        {
            try
            {
                var query = await Unit.Teams.Get();
                return Ok(query.ToList().Select(x => x.CreateMobile()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }


        /// <summary>
        /// Employee month
        /// </summary>
        /// <param name="empId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet("employee/{empId}/{year}/{month}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetEmployeeMonth(int empId, int year, int month)
        {
            try
            {
                return Ok(await Tracker.GetEmployeeMonth(empId, year, month));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Employee single day
        /// </summary>
        /// <param name="empId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        [HttpGet("employee-day/{empId}/{year}/{month}/{day}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetEmployeeDay(int empId, int year, int month, int day)
        {
            try
            {
                var query = await Unit.Calendar.Get(x => x.Employee.Id == empId && x.Date.Year == year && x.Date.Month == month && x.Date.Day == day);
                return Ok(query.Select(x => x.Create()));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Insert new employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody] Employee employee)
        {
            try
            {
                Log.Information("Try to add a new employee");
                Unit.Employees.Insert(employee);
                await Unit.Save();
                return Ok(employee.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Edit employee
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Put(int id, [FromBody] Employee employee)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                Log.Information($"Try to change employee with the id {id}");
                if (role != "admin" || employee.Id != userid)
                {
                    return Unauthorized();
                }
                Unit.Employees.Update(employee, id);
                await Unit.Save();
                return Ok(employee.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }
    }
}