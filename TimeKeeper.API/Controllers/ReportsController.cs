﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TimeKeeper.BLL;
using TimeKeeper.DAL;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : BaseController
    {
        protected ReportingService Reporting;

        public ReportsController(TimeKeeperContext context, IDeltaLog logger) : base(context, logger)
        {
            Reporting = new ReportingService(Unit);
        }

        /// <summary>
        /// Returns projects annual overview payload
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("projects-annual/{year}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> AnnualProjectOverview(int year)
        {
            try
            {
                return Ok(await Reporting.GetAnnual(year));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Gets annual overview for the project
        /// </summary>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("projects-annual-stored/{year}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> AnnualProjectStored(int year)
        {
            try
            {
                return Ok(await Reporting.GetAnnualStored(year));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns monthly overview payload
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet("monthly-overview/{year}/{month}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOrLeader")]
        public async Task<IActionResult> GetMonthlyOverview(int year, int month)
        {
            try
            {
                return Ok(await Reporting.GetMonthlyOverview(year, month));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Monthly overview with stored procedure
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet("monthly-overview-stored/{year}/{month}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOrLeader")]
        public IActionResult GetMonthlyStored(int year, int month)
        {
            try
            {
                return Ok(Reporting.GetMonthlyStored(year, month));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns project history payload
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("project-history/{projectId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> GetProjectHistory(int projectId)
        {
            try
            {
                Log.Information($"Try to get project history for project with id:{projectId}");
                return Ok(await Reporting.GetProjectHistoryModel(projectId));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Project history with stored procedure
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        [HttpGet("project-history-stored/{projectId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> GetProjectHistoryStored(int projectId)
        {
            try
            {
                Log.Information($"Try to get project history for project with id:{projectId}");
                return Ok(await Reporting.GetProjectHistoryStored(projectId));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }
    }
}