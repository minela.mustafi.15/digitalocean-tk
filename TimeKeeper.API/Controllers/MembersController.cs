﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.API.Services;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.DTO.Factories;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MembersController : BaseController
    {
        protected readonly MembershipService membershipService;
        public MembersController(TimeKeeperContext context, IDeltaLog log) : base(context, log)
        {
            membershipService = new MembershipService(context);
        }

        /// <summary>
        /// Fetches all data of type Member
        /// </summary>
        /// <returns>List of type Member</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminLeadOrMember")]
        public async Task<IActionResult> GetAll(int page = 1, int pageSize = 10000)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                Log.Information("Trying to fetch list of members..");
                var query = await Unit.Members.Get();
                var pagination = PaginationService<Member>.Pagination(ref query, page, pageSize);
                HttpContext.Response.Headers.Add("pagination", JsonConvert.SerializeObject(pagination));

                if (role == "user")
                {
                    var result = await Unit.Members.Get(x => x.Team.Members.Any(y => y.Employee.Id == userid));
                    return Ok(result.Select(z => z.Create()).ToList());
                }
                else
                return Ok(query.ToList().Select(x => x.Create()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns single Member entry
        /// </summary>
        /// <param name="id">Used to fetch entry by id</param>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminLeadOrMember")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                Log.Information($"Fetching member by id {id}");
                Member member = await Unit.Members.Get(id);
                if (member == null)
                {
                    Log.NotFound($"Requested resource with {id} does not exist");
                    return NotFound($"Requested resource with {id} does not exist");
                }
                if (role == "user" && !member.Team.Members.Any(x => x.Employee.Id == userid))
                {
                    return Unauthorized();
                }
                else
                return Ok(member.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Posts single entry of type Member
        /// </summary>
        /// <param name="member">Object of type member to be inserted</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy="AdminOrLeader")]
        public async Task<IActionResult> Post([FromBody] Member member)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                Log.Information("Posting new member..");
                if (role == "lead" && !member.Team.Members.Any(x => x.Employee.Id == userid))
                {
                    return Unauthorized();
                }
                Unit.Members.Insert(member);
                await Unit.Save();
                return Ok(member.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Updates existing entry of type Member
        /// </summary>
        /// <param name="id">Identifies an entry</param>
        /// <param name="member">Provides data body</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy ="AdminOrLeader")]
        public async Task<IActionResult> Put([FromBody]Member member, int id)
        {
            try
            {
                Log.Information($"Updating entry with id {id}");
                Unit.Members.Update(member, id);
                await Unit.Save();
                return Ok(member.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Revokes membership in a team
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPut("revoke-membership/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOrLeader")]
        public async Task<IActionResult> RevokeMembership(int id)
        {
            try
            {
                Log.Information($"Revoking membership for member, id: {id}");
                membershipService.RevokeMembership(id);
                await Unit.Save();
                return Ok();
            }
            catch (Exception ex)
            {

                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns member into the team
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPut("invoke-returnee/{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOrLeader")]
        public async Task<IActionResult> InvokeReturnee(int id)
        {
            try
            {
                Log.Information($"Revoking membership for member, id: {id}");
                membershipService.InvokeReturnee(id);
                await Unit.Save();
                return Ok();
            }
            catch (Exception ex)
            {

                return HandleException(ex);
            }
        }


        /// <summary>
        /// Deletes an entry
        /// </summary>
        /// <param name="id">Used to fetch an entry of type Member for deletion</param>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy ="AdminOrLeader")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Log.Information($"Trying to delete entry with id {id}");
                Unit.Members.Delete(id);
                await Unit.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns a list of statuses for this master model, or throws a Bad request.
        /// Serves to expose dependent entity
        /// </summary>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [Route("statuses")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetMembersStatuses()
        {
            try
            {
                var query = await Unit.MemberStatuses.Get();
                var statuses = query.ToList().Select(x => x.Master()).ToList();
                return Ok(statuses);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }
    }
}