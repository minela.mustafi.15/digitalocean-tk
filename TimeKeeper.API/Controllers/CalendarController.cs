﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.API.Services;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Factories;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalendarController : BaseController
    {
        public CalendarController(TimeKeeperContext context, IDeltaLog logger)
            : base(context, logger)
        {
        }

        /// <summary>
        /// Fetches all data of type calendar
        /// </summary>
        /// <returns>List of type calendar</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy="AdminLeadOrMember")]
        public async Task<IActionResult> Get(int page = 1, int pageSize = 10000)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());


                Log.Information("Fetching calendar");
                var calendars = await Unit.Calendar.Get();

                var pagination = PaginationService<Day>.Pagination(ref calendars, page, pageSize);
                HttpContext.Response.Headers.Add("pagination", JsonConvert.SerializeObject(pagination));

                if ((role == "lead" && calendars.Any(x =>
                x.Details.Any(y => y.Project.Team.Members.Any(z => z.Employee.Id!=userid)))) ||
                role =="user" && !(calendars.Any(x =>
                x.Details.Any(y => y.Project.Team.Members.Any(z => z.Employee.Id != userid)))))
                {
                    return Unauthorized();
                }
                return Ok(calendars.ToList().Select(x => x.Create()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns single calendar entry
        /// </summary>
        /// <param name="id">Used to fetch entry by id</param>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminLeadOrOwner")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                Log.Information($"Try to fetch team with id {id}");
                Day day = await Unit.Calendar.Get(id);
                if (day == null)
                {
                    // make extension method and implement it
                    Log.NotFound($"Requested resource with {id} does not exist");
                    return NotFound($"Requested resource with {id} does not exist");
                }
                if (role == "lead" && !day.Details.Any(x => x.Project.Team.Members.Any(y => y.Employee.Id == userid)) ||
                    role == "user" && !day.Details.Any(x => x.Project.Team.Members.Any(y => y.Employee.Id == userid)))
                {
                    return Unauthorized();
                }
                return Ok(day.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Posts single entry of type calendar
        /// </summary>
        /// <param name="day">Object of type day to be inserted</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminLeadOrOwner")]
        public async Task<IActionResult> Post([FromBody] Day day)
        {
            try
            {
                Log.Information("Try to insert new day");
                Unit.Calendar.Insert(day);
                await Unit.Save();
                return Ok(day.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Updates existing entry of type Day
        /// </summary>
        /// <param name="id">Identifies an entry</param>
        /// <param name="day">Provides data body</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminLeadOrOwner")]
        public async Task<IActionResult> Put(int id, [FromBody] Day day)
        {
            try
            {
                Log.Information($"Trying to update day with id {id}");
                Unit.Calendar.Update(day, id);
                await Unit.Save();
                return Ok(day.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes an entry
        /// </summary>
        /// <param name="id">Used to fetch an entry of type Day for deletion</param>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy ="IsAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Log.Information($"Deleting day with id {id}");
                Unit.Calendar.Delete(id);
                await Unit.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns a list of types for this master model, or throws a Bad request.
        /// Serves to expose dependent entity
        /// </summary>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [Route("types")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetDayTypes()
        {
            try
            {
                var query = await Unit.DayTypes.Get();
                var daytypes = query.ToList().Select(x => x.Master()).ToList();
                return Ok(daytypes);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }
    }
}