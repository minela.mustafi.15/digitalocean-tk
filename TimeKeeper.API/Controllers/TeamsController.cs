﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.API.Services;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.DTO.Factories;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : BaseController
    {
        public TeamsController(TimeKeeperContext context, IDeltaLog logger) : base(context, logger)
        {
        }

        /// <summary>
        /// Fetches all data of type Team
        /// </summary>
        /// <returns>List of type Team</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [Authorize(Policy ="AdminLeadOrMember")]
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetAll(int page = 1, int pageSize = 10000)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                Log.Information("Fetching list of teams");
                var query = await Unit.Teams.Get();
                var pagination = PaginationService<Team>.Pagination(ref query, page, pageSize);
                HttpContext.Response.Headers.Add("pagination", JsonConvert.SerializeObject(pagination));

                if (role == "user" || role == "lead")
                {
                    query = await Unit.Teams.Get(x => x.Members.Any(y => y.Employee.Id == userid));
                    return Ok(query.ToList().Select(x => x.Create()).ToList());
                }
                else
                    return Ok(query.ToList().Select(x => x.Create()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns single Team entry
        /// </summary>
        /// <param name="id">Used to fetch entry by id</param>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminLeadOrMember")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                Log.Information($"Try to fetch team with id {id}");
                Team team = await Unit.Teams.Get(id);
                if (team == null)
                {
                    Log.NotFound($"Requested resource with {id} does not exist");
                    return NotFound($"Requested resource with {id} does not exist");
                }
                if (role == "user"|| (role=="lead" && !team.Members.Any(x => x.Employee.Id == userid)))
                {
                    return Unauthorized();
                }
                return Ok(team.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Posts single entry of type Team
        /// </summary>
        /// <param name="team">Object of type Team to be inserted</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> Post([FromBody] Team team)
        {
            try
            {
                Log.Information("Trying to insert new team.");
                Unit.Teams.Insert(team);
                await Unit.Save();
                return Ok(team.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Updates existing entry of type Team
        /// </summary>
        /// <param name="id">Identifies an entry</param>
        /// <param name="team">Provides data body</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> Put(int id, [FromBody] Team team)
        {
            try
            {
                Log.Information($"Trying to update team with id {id}");
                Unit.Teams.Update(team, id);
                await Unit.Save();
                return Ok(team.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes an entry
        /// </summary>
        /// <param name="id">Used to fetch an entry of type Team for deletion</param>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Log.Information($"Deleting team with id {id}");
                Unit.Teams.Delete(id);
                await Unit.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }
    }
}