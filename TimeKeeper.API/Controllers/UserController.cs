﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TimeKeeper.DAL;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Factories;
using TimeKeeper.DTO.Models;

using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        public UserController(TimeKeeperContext context, IDeltaLog log) : base(context, log)
        {
        }
        /// <summary>
        /// Password hashing endpoint.Use only once per seed
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("password")]
        public async Task<IActionResult> GetUsersAndUpdate()
        {
            var query = await Unit.Users.Get();
            foreach (User user in query)
            {
                user.Password = user.Username.HashWith(user.Password);
                Unit.Context.Entry(user).CurrentValues.SetValues(user);
            }
            await Unit.Save();
            return Ok();
        }

        /// <summary>
        /// Returns list of users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var query = await Unit.Users.Get();
                return Ok(query.Select(x => x.Create()).ToList().OrderBy(x => x.Name));
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// Returns specific user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                User user = await Unit.Users.Get(id);
                if (user == null)
                {
                    return NotFound();
                }
                else
                {
                    return Ok(user.Create());
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// Adds new user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] User user)
        {
            try
            {
                Unit.Users.Insert(user);
                await Unit.Save();
                return Ok(user.Create());
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// Changes existing User entry
        /// </summary>
        /// <param name="user"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromBody] User user, int id)
        {
            try
            {
                Unit.Users.Update(user, id);
                await Unit.Save();
                return Ok(user.Create());
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        /// <summary>
        /// Deletes existing user entry
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Unit.Users.Delete(id);
                await Unit.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("/login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            try
            {
                User user = await Access.Check(model.Username, model.Password);
                if (user != null)
                {
                    string token = Access.GetToken(user);
                    return Ok(new { user = user.Create(), token });
                }
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}