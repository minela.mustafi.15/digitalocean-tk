using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TimeKeeper.BLL;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardsController : BaseController
    {
        protected DashboardService Dashboard;
        public DashboardsController(TimeKeeperContext context, IDeltaLog logger) : base(context, logger)
        {
            Dashboard = new DashboardService(Unit, StoredProcedures);
        }

        /// <summary>
        /// Returns personal dashboard view data
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        [HttpGet("personal/{employeeId}/{year}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> PersonalDashboard(int employeeId, int year)
        {
            try
            {
                return Ok(await Dashboard.GetEmployeeYearDashboard(employeeId, year));
            }
            catch (Exception ex)
            {

                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns personal dashboard data with stored procedure
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        //[AllowAnonymous]
        [HttpGet("personal-stored/{employeeId}/{year}/{month}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> GetPersonalDashboardStored(int employeeId, int year, int month)
        {
            try
            {
                return Ok(await Dashboard.GetPersonalDashboardStored(employeeId, year, month));
            }
            catch (Exception ex)
            {

                return HandleException(ex);
            }
        }

        /// <summary>
        /// Exposes team dashboard for given year and month
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns>Data payload</returns>
        [HttpGet("team/{teamId}/{year}/{month}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOrLeader")]
        public async Task<IActionResult> TeamDashboard(int teamId, int year, int month)
        {
            try
            {
                return Ok(await Dashboard.GetTeamDashboardInfo(teamId, year, month));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns admin dashboard for requested year and month
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns>Data payload</returns>
        [HttpGet("admin/{year}/{month}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> AdminDashboard(int year, int month)
        {
            try
            {
                return Ok(await Dashboard.GetAdminDashboardInfo(year, month));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Gets company dashboard
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns>Data required by this dashboard</returns>
        [HttpGet("company/{year}/{month}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public IActionResult CompanyDashboard(int year, int month)
        {
            try
            {
                return Ok(Dashboard.GetCompanyDashboard(year,month));
            }
            catch (Exception ex)
            {

                return HandleException(ex);
            }
        }

        /// <summary>
        /// Team stored
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        [HttpGet("team-dashboard-stored/{teamId}/{year}/{month}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminOrLeader")]
        public async Task<IActionResult> TeamDashboardStored(int teamId, int year, int month)
        {
            try
            {
                var team = await Unit.Teams.Get(teamId);
                return Ok(Dashboard.GetTeamDashboardStored(team, year, month));
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

    }
}