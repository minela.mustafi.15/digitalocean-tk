﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.API.Services;
using TimeKeeper.DAL;
using TimeKeeper.Domain.Entities;
using TimeKeeper.DTO.Factories;
using TimeKeeper.Utilities.Logging;

namespace TimeKeeper.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetailsController : BaseController
    {
        public DetailsController(TimeKeeperContext context, IDeltaLog log) : base(context, log)
        {
        }

        /// <summary>
        /// Fetches all data of type Detail
        /// </summary>
        /// <returns>List of type Detail</returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy ="AdminLeadOrMember")]
        public async Task<IActionResult> GetAll(int page = 1, int pageSize = 10000)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                Log.Information("Try to fetch a list of tasks");
                var query = await Unit.Details.Get();
                var pagination = PaginationService<Detail>.Pagination(ref query, page, pageSize);
                HttpContext.Response.Headers.Add("pagination", JsonConvert.SerializeObject(pagination));

                if(role == "lead")
                {
                    query = Unit.Details.Get(x => x.Project.Team.Members.Any(y => y.Employee.Id == userid)).Result;
                    return Ok(query.ToList().Select(x=>x.Create()).ToList());
                }
                else if (role=="user")
                {
                    query = Unit.Details.Get(x => x.Day.Employee.Id == userid).Result;
                    return Ok(query.ToList().Select(x=>x.Create()).ToList());
                }
                else
                return Ok(query.ToList().Select(x => x.Create()).ToList());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Returns single Detail entry
        /// </summary>
        /// <param name="id">Used to fetch entry by id</param>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminLeadOrMember")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                Log.Information($"Try to fetch task with the id {id}");
                var detail = await Unit.Details.Get(id);
                if (detail == null)
                {
                    Log.NotFound($"Requested resource with {id} does not exist");
                    return NotFound($"Requested resource with {id} does not exist");
                }
                if ((role == "lead" && !(detail.Project.Team.Members.Any(x => x.Employee.Id==userid))) ||
                    (role == "user" && !(detail.Day.Employee.Id==userid)))
                {
                    return Unauthorized();
                }
                return Ok(detail.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Posts single entry of type Detail
        /// </summary>
        /// <param name="detail">Object of type detail to be inserted</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminLeadOrOwner")]
        public async Task<IActionResult> Post([FromBody] Detail detail)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                var day = Unit.Calendar.Get(detail.Day.Id).Result;
                var project = Unit.Projects.Get(detail.Project.Id).Result;


                Log.Information("Try to add new task");
                if (role == "lead" && !project.Team.Members.Any(x => x.Employee.Id == userid) ||
                    role == "user" && !(day.Employee.Id == userid))
                {
                    return Unauthorized();
                }
                Unit.Details.Insert(detail);
                await Unit.Save();
                return Ok(detail.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Updates existing entry of type Detail
        /// </summary>
        /// <param name="id">Identifies an entry</param>
        /// <param name="detail">Provides data body</param>
        /// <response status="200">Returns OK</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "AdminLeadOrOwner")]
        public async Task<IActionResult> Put(int id, [FromBody] Detail detail)
        {
            try
            {
                var role = User.Claims.FirstOrDefault(c => c.Type == "role").Value.ToString();
                int userid = int.Parse(User.Claims.FirstOrDefault(x => x.Type == "sub").Value.ToString());

                var day = Unit.Calendar.Get(detail.Day.Id).Result;
                var project = Unit.Projects.Get(detail.Project.Id).Result;

                Log.Information($"Try to change task with the id {id}");
                if (role == "lead" && !detail.Project.Team.Members.Any(x => x.Employee.Id == userid) ||
                    role == "user" && !(detail.Day.Employee.Id == userid))
                {
                    return Unauthorized();
                }
                Unit.Details.Update(detail, id);
                await Unit.Save();
                return Ok(detail.Create());
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }

        /// <summary>
        /// Deletes an entry
        /// </summary>
        /// <param name="id">Used to fetch an entry of type Detail for deletion</param>
        /// <returns></returns>
        /// <response status="200">Returns OK</response>
        /// <response status="404">Returns NotFound</response>
        /// <response status="400">Returns Bad Request</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [ProducesResponseType(400)]
        [Authorize(Policy = "IsAdmin")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                Log.Information($"Try to delete task with the id {id}");

                Unit.Details.Delete(id);
                await Unit.Save();
                return Ok();
            }
            catch (Exception ex)
            {
                return HandleException(ex);
            }
        }
    }
}