﻿using System.Net;
using System.Net.Mail;
using System.Text;

namespace TimeKeeper.API.Services
{
    public static class MailService
    {
        public static void Send(string mailTo, string subject, string body)
        {
            SmtpClient client = new SmtpClient
            {
                Port = 587,
                Host = "smtp.gmail.com",
                EnableSsl = true,
                Timeout = 10000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("ntg.infodesk@gmail.com", "Company19892019")
            };

            MailMessage message = new MailMessage("phr@gmail.com", "nela.gloom@hotmail.com", subject, body);
            message.BodyEncoding = UTF8Encoding.UTF8;
            message.IsBodyHtml = true;
            message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            message.ReplyToList.Add(mailTo);

            client.Send(message);
        }
    }
}