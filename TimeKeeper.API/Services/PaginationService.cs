﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TimeKeeper.API.Services
{
    public class PaginationService<T> where T : class
    {
        public static object Pagination(ref IEnumerable<T> query, int page = 1, int pageSize = 10000)
        {
            int totalItems = query.Count();
            int totalPages = (int)Math.Ceiling(totalItems / (decimal)pageSize);
            if (page < 1) page = 1;
            if (page > totalPages) page = totalPages;
            int currentPage = page - 1;
            query = query.Skip(currentPage * pageSize).Take(pageSize);
            var pagination = new
            {
                pageSize,
                totalItems,
                totalPages,
                page,
            };
            return pagination;
        }
    }
}