﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.DAL;
using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.API.Services
{
    public class MembershipService
    {
        protected readonly TimeKeeperContext _context;
        public MembershipService(TimeKeeperContext context)
        {
            _context = context;
        }
        public void RevokeMembership(int id)
        {
            Member member = _context.Members.Find(id);
            member.Status = new MemberStatus { Id = 4 };
            _context.Entry(member).CurrentValues.SetValues(member);
        }

        public void InvokeReturnee(int id)
        {
            Member member = _context.Members.Find(id);
            if (member.Status.Id == 4)
            {
                member.Status = new MemberStatus { Id = 1 };
                _context.Entry(member).CurrentValues.SetValues(member);
            }
        }
    }
}
