﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IdentityModel.Tokens.Jwt;
using TimeKeeper.API.Authorization;
using TimeKeeper.API.Services;
using TimeKeeper.DAL;
using TimeKeeper.Uilities.MailService;
using TimeKeeper.Utilities.Logging;
using TimeKeeper.Utilities.MailService;

namespace TimeKeeper.API
{
    public class Startup
    {
        public static IConfigurationRoot Configuration;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder().SetBasePath(env.ContentRootPath)
                                                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            Configuration = builder.Build();
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc()
                .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling =
                                                    Newtonsoft.Json.ReferenceLoopHandling.Serialize)
                .AddJsonOptions(x => x.SerializerSettings.PreserveReferencesHandling =
                                                    Newtonsoft.Json.PreserveReferencesHandling.None)
                .AddJsonOptions(x => x.SerializerSettings.DateTimeZoneHandling =
                                                    Newtonsoft.Json.DateTimeZoneHandling.Local);

            services.AddAuthorization(o =>
            {

                o.AddPolicy("IsAdmin", builder =>
                {
                    builder.AddRequirements(new IsRoleRequirement());
                });
                o.AddPolicy("AdminOrLeader", builder =>
                {
                    builder.AddRequirements(new AdminOrLeadRequirement());
                });
                o.AddPolicy("AdminLeadOrMember", builder =>
                {
                    builder.AddRequirements(new AdminLeadOrMemberRequirement());
                });
                o.AddPolicy("AdminLeadOrOwner", builder =>
                {
                    builder.AddRequirements(new AdminLeadOrOwnerRequirement());
                });
            });

            services.AddScoped<IAuthorizationHandler, RequireAdminRole>();
            services.AddScoped<IAuthorizationHandler, AdminOrLeadHandler>();
            services.AddScoped<IAuthorizationHandler, AdminLeadOrMemberHandler>();
            services.AddScoped<IAuthorizationHandler, AdminLeadOrOwnerHandler>();


            string connectionString = Configuration["ConnectionString"];
            services.AddDbContext<TimeKeeperContext>(o => { o.UseNpgsql(connectionString); });


            services.AddSingleton<IDeltaLog, DeltaLogger>();


            services.AddSingleton<IEmailService, EmailService>();
            services.Configure<EmailSettings>(Configuration.GetSection("Email:smtp"));
            services.AddSwaggerDocument();

            services.Configure<IISOptions>(o =>
            {
                o.AutomaticAuthentication = false;
            });

            //Comment out for back end work / uncomment when working with front end


            services.AddAuthentication("TokenAuthentication")
                   .AddScheme<AuthenticationSchemeOptions, TokenAuthenticationHandler>("TokenAuthentication", null);

            #region implicit idp auth
            // Comment out for back end work / uncomment when working with front end
            //services.AddAuthentication(options =>
            //{
            //    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //}).AddJwtBearer(o =>
            //{
            //    o.Authority = "https://localhost:44300/";
            //    o.Audience = "timekeeper";
            //    o.RequireHttpsMetadata = false;
            //});
            #endregion

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            #region hybrid idp auth
            ////Comment out for Front end / Uncomment for Back end
            //services.AddAuthentication(o =>
            //{
            //    o.DefaultScheme = "Cookies";
            //    o.DefaultChallengeScheme = "oidc";
            //}).AddCookie("Cookies", o =>
            //{
            //    o.AccessDeniedPath = "/AccessDenied";
            //}).AddOpenIdConnect("oidc", o =>
            //{
            //    o.SignInScheme = "Cookies";
            //    o.Authority = "https://localhost:44300";
            //    o.ClientId = "tk2019";
            //    o.ClientSecret = "mistral_talents";
            //    o.ResponseType = "code id_token";
            //    o.Scope.Add("openid");
            //    o.Scope.Add("profile");
            //    o.Scope.Add("address");
            //    o.Scope.Add("roles");
            //    o.Scope.Add("teams");
            //    o.Scope.Add("timekeeper");
            //    o.SaveTokens = true;
            //    o.GetClaimsFromUserInfoEndpoint = true;
            //    o.ClaimActions.MapUniqueJsonKey("address", "address");
            //    o.ClaimActions.MapUniqueJsonKey("role", "role");
            //    o.ClaimActions.MapUniqueJsonKey("team", "team");
            //    o.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        NameClaimType = JwtClaimTypes.GivenName,
            //        RoleClaimType = JwtClaimTypes.Role,
            //    };
            //});
            #endregion

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();
            app.UseOpenApi();
            app.UseSwaggerUi3();
            app.UseCors(c => c.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader().AllowCredentials());
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}