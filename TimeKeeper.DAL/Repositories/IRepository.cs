﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TimeKeeper.DAL.Repositories
{
    public interface IRepository<Entity>
    {
        Task<IEnumerable<Entity>> Get();

        Task<IEnumerable<Entity>> Get(Func<Entity, bool> where);

        Task<Entity> Get(int id);

        void Insert(Entity entity);

        void Update(Entity entity, int id);

        void Delete(Entity entity);

        void Delete(int id);
    }
}