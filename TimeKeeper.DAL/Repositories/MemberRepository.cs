﻿using Library.DAL;
using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.DAL.Repositories
{
    internal class MemberRepository : Repository<Member>
    {
        protected new TimeKeeperContext _context;

        public MemberRepository(TimeKeeperContext context) : base(context)
        {
            _context = context;
        }

        public void Build(Member member)
        {
            member.Employee = _context.Employees.Find(member.Employee.Id);
            member.Role = _context.Roles.Find(member.Role.Id);
            member.Status = _context.MemberStatuses.Find(member.Status.Id);
            member.Team = _context.Teams.Find(member.Team.Id);
        }

        public override void Insert(Member member)
        {
            Build(member);
            base.Insert(member);
        }

        public override void Update(Member member, int id)
        {
            Member old = dbSet.Find(id);
            Build(old);
            _context.Entry(old).CurrentValues.SetValues(member);
            old.Employee = _context.Employees.Find(member.Employee.Id);
            old.Role = _context.Roles.Find(member.Role.Id);
            old.Status = _context.MemberStatuses.Find(member.Status.Id);
            old.Team = _context.Teams.Find(member.Team.Id);
        }

        public override void Delete(int id)
        {
            Member member = dbSet.Find(id);
            Delete(member);
        }
    }
}