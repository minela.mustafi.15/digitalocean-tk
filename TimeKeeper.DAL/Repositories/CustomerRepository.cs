﻿using Library.DAL;
using System;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.DAL.Repositories
{
    public class CustomerRepository : Repository<Customer>
    {
        private new TimeKeeperContext _context;

        public CustomerRepository(TimeKeeperContext context) : base(context)
        {
            _context = context;
        }

        public void Build(Customer customer)
        {
            customer.Status = _context.CustomerStatuses.Find(customer.Status.Id);
        }

        public override void Insert(Customer customer)
        {
            Build(customer);
            base.Insert(customer);
        }

        public override void Update(Customer customer, int id)
        {
            Customer old = dbSet.Find(id);
            Build(old);
            _context.Entry(old).CurrentValues.SetValues(customer);
            old.Status = _context.CustomerStatuses.Find(customer.Status.Id);
        }

        public override void Delete(int id)
        {
            Customer customer = dbSet.Find(id);
            if (customer.Projects.Count != 0)
                throw new Exception("Object can't be deleted, because it has children entities!");

            Delete(customer);
        }
    }
}