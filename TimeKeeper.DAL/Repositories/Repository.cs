﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TimeKeeper.DAL;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain.Entities;

namespace Library.DAL
{
    public class Repository<Entity> : IRepository<Entity> where Entity : class
    {
        protected TimeKeeperContext _context;
        protected DbSet<Entity> dbSet;

        public Repository(TimeKeeperContext context)
        {
            _context = context;
            dbSet = _context.Set<Entity>();
        }

        public void ValidateUpdate(Entity oldEntity, Entity newEntity, int id)
        {
            if (oldEntity == null)
                throw new ArgumentException($"There is no object with id: {id} in database");
            if (id != (newEntity as BaseClass).Id)
                throw new ArgumentException($"Error! Id of the sent object: {(newEntity as BaseClass).Id} and id in url: {id} are not the same");
        }

        public virtual async Task<IEnumerable<Entity>> Get()
        {
            return await dbSet.ToListAsync();
        }

        public virtual async Task<IEnumerable<Entity>> Get(Func<Entity, bool> where)
        {
            return await dbSet.ToAsyncEnumerable<Entity>().Where(where).ToList();
        }

        public virtual async Task<Entity> Get(int id)
        {
            Entity entity = await dbSet.FindAsync(id);
            if (entity == null)
                return null;
            return entity;
        }

        public virtual void Insert(Entity entity)
        {
            entity.Build(_context);
            dbSet.Add(entity);
        }

        public virtual void Update(Entity entity, int id)
        {
            entity.Build(_context);
            Entity old = dbSet.Find(id);
            ValidateUpdate(old, entity, id);
            _context.Entry(old).CurrentValues.SetValues(entity);
            old.Relate(entity);
        }

        public void Delete(Entity entity)
        {
            dbSet.Remove(entity);
        }

        public virtual void Delete(int id)
        {
            Entity old = dbSet.Find(id);
            if (old != null && old.CanDelete()) Delete(old);
        }
    }
}