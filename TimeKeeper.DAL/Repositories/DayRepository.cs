﻿using Library.DAL;
using System;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.DAL.Repositories
{
    public class DayRepository : Repository<Day>
    {
        private new TimeKeeperContext _context;

        public DayRepository(TimeKeeperContext context) : base(context)
        {
            _context = context;
        }

        public void Build(Day day)
        {
            day.Employee = _context.Employees.Find(day.Employee.Id);
            day.Type = _context.DayTypes.Find(day.Type.Id);
        }

        public override void Insert(Day day)
        {
            Build(day);
            base.Insert(day);
        }

        public override void Update(Day day, int id)
        {
            Day old = dbSet.Find(id);
            Build(old);
            _context.Entry(old).CurrentValues.SetValues(day);
            old.Employee = _context.Employees.Find(day.Employee.Id);
        }

        public override void Delete(int id)
        {
            Day day = dbSet.Find(id);
            if (day.Details.Count != 0)
                throw new Exception("Object can't be deleted, because it has children entities!");

            Delete(day);
        }
    }
}