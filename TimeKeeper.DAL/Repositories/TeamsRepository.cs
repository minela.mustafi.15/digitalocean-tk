﻿using Library.DAL;
using System;
using TimeKeeper.Domain;

namespace TimeKeeper.DAL.Repositories
{
    public class TeamsRepository : Repository<Team>
    {
        public TeamsRepository(TimeKeeperContext context) : base(context)
        {
        }

        public override void Delete(int id)
        {
            Team team = dbSet.Find(id);
            if (team.Projects.Count != 0 || team.Members.Count != 0)
                throw new Exception("Object can't be deleted, because it has children entities!");

            Delete(team);
        }
    }
}