﻿using Library.DAL;
using System;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.DAL.Repositories
{
    internal class ProjectRepository : Repository<Project>
    {
        protected new TimeKeeperContext _context;

        public ProjectRepository(TimeKeeperContext context) : base(context)
        {
            _context = context;
        }

        public void Build(Project project)
        {
            project.Status = _context.ProjectStatuses.Find(project.Status.Id);
            project.Customer = _context.Customers.Find(project.Customer.Id);
            project.Pricing = _context.PricingTypes.Find(project.Pricing.Id);
            project.Team = _context.Teams.Find(project.Team.Id);
        }

        public override void Insert(Project project)
        {
            Build(project);
            base.Insert(project);
        }

        public override void Update(Project project, int id)
        {
            Project old = dbSet.Find(id);
            Build(old);
            _context.Entry(old).CurrentValues.SetValues(project);
            old.Status = _context.ProjectStatuses.Find(project.Status.Id);
            old.Customer = _context.Customers.Find(project.Customer.Id);
            old.Pricing = _context.PricingTypes.Find(project.Pricing.Id);
            old.Team = _context.Teams.Find(project.Team.Id);
        }

        public override void Delete(int id)
        {
            Project project = dbSet.Find(id);
            if (project.Details.Count != 0)
                throw new Exception("Object can't be deleted, because it has children entities!");

            Delete(project);
        }
    }
}