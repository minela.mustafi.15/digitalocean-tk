﻿using Library.DAL;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.DAL.Repositories
{
    internal class DetailRepository : Repository<Detail>
    {
        protected new TimeKeeperContext _context;

        public DetailRepository(TimeKeeperContext context) : base(context)
        {
            _context = context;
        }

        public void Build(Detail detail)
        {
            detail.Day = _context.Calendar.Find(detail.Day.Id);
            detail.Project = _context.Projects.Find(detail.Project.Id);
        }

        public override void Insert(Detail detail)
        {
            Build(detail);
            base.Insert(detail);
        }

        public override void Update(Detail detail, int id)
        {
            Detail old = dbSet.Find(id);
            Build(old);
            _context.Entry(old).CurrentValues.SetValues(detail);
            old.Day = _context.Calendar.Find(detail.Day.Id);
            old.Project = _context.Projects.Find(detail.Project.Id);
        }
    }
}