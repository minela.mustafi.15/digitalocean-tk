﻿using Library.DAL;
using System;
using TimeKeeper.Domain;

namespace TimeKeeper.DAL.Repositories
{
    internal class RolesRepository : Repository<Role>
    {
        protected new TimeKeeperContext _context;

        public RolesRepository(TimeKeeperContext context) : base(context)
        {
            _context = context;
        }

        public override void Delete(int id)
        {
            Role role = dbSet.Find(id);
            if (role.Members.Count != 0)
                throw new Exception("Object can't be deleted, because it has children entities!");

            Delete(role);
        }
    }
}