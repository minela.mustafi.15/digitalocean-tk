﻿using Library.DAL;
using System;
using TimeKeeper.Domain;

namespace TimeKeeper.DAL.Repositories
{
    public class EmployeeRepository : Repository<Employee>
    {
        private new TimeKeeperContext _context;

        public EmployeeRepository(TimeKeeperContext context) : base(context)
        {
            _context = context;
        }

        public void Build(Employee employee)
        {
            employee.Position = _context.PositionTypes.Find(employee.Position.Id);
            employee.Status = _context.EmployeeStatuses.Find(employee.Status.Id);
        }

        public override void Insert(Employee employee)
        {
            Build(employee);
            base.Insert(employee);
        }

        public override void Update(Employee employee, int id)
        {
            Employee old = dbSet.Find(id);
            Build(old);
            _context.Entry(old).CurrentValues.SetValues(employee);
            old.Status = _context.EmployeeStatuses.Find(employee.Status.Id);
        }

        public override void Delete(int id)
        {
            Employee employee = dbSet.Find(id);
            if (employee.Members.Count != 0 || employee.Calendar.Count != 0)
                throw new Exception("Object can't be deleted, because it has children entities!");

            Delete(employee);
        }
    }
}