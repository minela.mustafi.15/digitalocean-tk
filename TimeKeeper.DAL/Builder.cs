﻿using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.DAL
{
    public static class Builder
    {
        public static void Build<T>(this T entity, TimeKeeperContext context)
        {
            if (typeof(T) == typeof(Project)) Create(entity as Project, context);
            if (typeof(T) == typeof(Member)) Create(entity as Member, context);
            if (typeof(T) == typeof(Day)) Create(entity as Day, context);
            if (typeof(T) == typeof(Detail)) Create(entity as Detail, context);
        }

        private static void BuildPassword(User user)
        {
            if (!string.IsNullOrWhiteSpace(user.Password)) user.Password = user.Username.HashWith(user.Password);
        }

        public static void Relate<T>(this T oldEntity, T newEntity)
        {
            if (typeof(T) == typeof(Project)) Modify(oldEntity as Project, newEntity as Project);
            if (typeof(T) == typeof(Member)) Modify(oldEntity as Member, newEntity as Member);
            if (typeof(T) == typeof(Day)) Modify(oldEntity as Day, newEntity as Day);
            if (typeof(T) == typeof(Detail)) Modify(oldEntity as Detail, newEntity as Detail);
        }

        private static void Modify(Project oldP, Project newP)
        {
            oldP.Customer = newP.Customer;
            oldP.Team = newP.Team;
            oldP.Status = newP.Status;
            oldP.Pricing = newP.Pricing;
        }

        private static void Create(Project p, TimeKeeperContext context)
        {
            p.Customer = context.Customers.Find(p.Customer.Id);
            p.Team = context.Teams.Find(p.Team.Id);
            p.Status = context.ProjectStatuses.Find(p.Status.Id);
            p.Pricing = context.PricingTypes.Find(p.Pricing.Id);
        }

        private static void Create(Member m, TimeKeeperContext context)
        {
            m.Employee = context.Employees.Find(m.Employee.Id);
            m.Team = context.Teams.Find(m.Team.Id);
            m.Role = context.Roles.Find(m.Role.Id);
            m.Status = context.MemberStatuses.Find(m.Status.Id);
        }
        private static void Modify(Member oldM, Member newM)
        {
            oldM.Employee = newM.Employee;
            oldM.Team = newM.Team;
            oldM.Role = newM.Role;
            oldM.Status = newM.Status;
        }

        private static void Create(Day d, TimeKeeperContext context)
        {
            d.Employee = context.Employees.Find(d.Employee.Id);
            d.Type = context.DayTypes.Find(d.Type.Id);
        }

        private static void Modify(Day oldD, Day newD)
        {
            oldD.Employee = newD.Employee;
            oldD.Type = newD.Type;
        }

        private static void Create(Detail d, TimeKeeperContext context)
        {
            d.Day = context.Calendar.Find(d.Day.Id);
            d.Project = context.Projects.Find(d.Project.Id);
        }
        private static void Modify(Detail oldD, Detail newD)
        {
            oldD.Day = newD.Day;
            oldD.Project = newD.Project;
        }
    }
}