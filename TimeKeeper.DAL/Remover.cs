﻿using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.DAL
{
    public static class Remover
    {
        public static bool CanDelete<T>(this T entity)
        {
            bool result = false;
            if (typeof(T) == typeof(Team)) result = !HasChildren(entity as Team);
            if (typeof(T) == typeof(Detail)) result = HasChildren(entity as Detail);
            return result;
        }

        private static bool HasChildren(Team t)
        {
            return (t.Projects.Count + t.Members.Count != 0);
        }

        private static bool HasChildren(Detail d)
        {
            return (d.Project !=  null);
        }
    }
}