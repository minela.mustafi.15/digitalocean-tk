﻿using Library.DAL;
using System;
using System.Threading;
using System.Threading.Tasks;
using TimeKeeper.DAL.Repositories;
using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.DAL
{
    public class UnitOfWork : IDisposable
    {
        protected TimeKeeperContext _context;
        public CancellationToken token;

        public UnitOfWork(TimeKeeperContext context)
        {
            _context = context;
        }

        private IRepository<Customer> _customers;
        private IRepository<Day> _calendar;
        private IRepository<Detail> _details;
        private IRepository<Employee> _employees;
        private IRepository<Member> _members;
        private IRepository<Project> _projects;
        private IRepository<Role> _roles;
        private IRepository<Team> _teams;
        private IRepository<User> _users;

        private IRepository<CustomerStatus> _customerStatuses;
        private IRepository<EmployeeStatus> _employeeStatuses;
        private IRepository<DayType> _dayTypes;
        private IRepository<PricingType> _pricingTypes;
        private IRepository<ProjectStatus> _projectStatus;
        private IRepository<PositionType> _positionTypes;
        private IRepository<MemberStatus> _memberStatuses;

        public TimeKeeperContext Context { get { return _context; } }

        public IRepository<Customer> Customers => _customers ?? (_customers = new CustomerRepository(_context));
        public IRepository<Day> Calendar => _calendar ?? (_calendar = new DayRepository(_context));
        public IRepository<Detail> Details => _details ?? (_details = new DetailRepository(_context));
        public IRepository<Employee> Employees => _employees ?? (_employees = new EmployeeRepository(_context));
        public IRepository<Member> Members => _members ?? (_members = new MemberRepository(_context));
        public IRepository<Project> Projects => _projects ?? (_projects = new ProjectRepository(_context));
        public IRepository<Role> Roles => _roles ?? (_roles = new Repository<Role>(_context));
        public IRepository<Team> Teams => _teams ?? (_teams = new TeamsRepository(_context));

        // new
        public IRepository<User> Users => _users ?? (_users = new Repository<User>(_context));

        public IRepository<CustomerStatus> CustomerStatuses => _customerStatuses ?? (_customerStatuses = new Repository<CustomerStatus>(_context));
        public IRepository<EmployeeStatus> EmployeeStatuses => _employeeStatuses ?? (_employeeStatuses = new Repository<EmployeeStatus>(_context));
        public IRepository<DayType> DayTypes => _dayTypes ?? (_dayTypes = new Repository<DayType>(_context));
        public IRepository<ProjectStatus> ProjectStatuses => _projectStatus ?? (_projectStatus = new Repository<ProjectStatus>(_context));
        public IRepository<PricingType> PricingTypes => _pricingTypes ?? (_pricingTypes = new Repository<PricingType>(_context));
        public IRepository<PositionType> PositionTypes => _positionTypes ?? (_positionTypes = new Repository<PositionType>(_context));
        public IRepository<MemberStatus> MemberStatuses => _memberStatuses ?? (_memberStatuses = new Repository<MemberStatus>(_context));

        public async Task<int> Save()
        {
            return await _context.SaveChangesAsync(token);
        }

        public void Dispose() => _context.Dispose();
    }
}