using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TimeKeeper.Domain;
using TimeKeeper.Domain.Entities;

namespace TimeKeeper.DAL
{
    public class TimeKeeperContext : DbContext
    {
        private string _conStr;

        #region ctors

        public TimeKeeperContext() : base()
        {
            _conStr = "User ID=postgres; Password=postgres; Server=localhost; Port=5432; Database=timekeeper; Integrated Security=true; Pooling=true;";
        }

        public TimeKeeperContext(DbContextOptions<TimeKeeperContext> options) : base(options)
        {
        }

        public TimeKeeperContext(string conStr)
        {
            _conStr = conStr;
        }

        #endregion ctors

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Day> Calendar { get; set; }
        public DbSet<Detail> Details { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        public DbSet<EmployeeStatus> EmployeeStatuses { get; set; }
        public DbSet<CustomerStatus> CustomerStatuses { get; set; }
        public DbSet<ProjectStatus> ProjectStatuses { get; set; }
        public DbSet<MemberStatus> MemberStatuses { get; set; }
        public DbSet<DayType> DayTypes { get; set; }
        public DbSet<PricingType> PricingTypes { get; set; }
        public DbSet<PositionType> PositionTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder builder)
        {
            if (_conStr != null)
            {
                builder.UseNpgsql(_conStr);
            }
            builder.UseLazyLoadingProxies(true);
            base.OnConfiguring(builder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>().OwnsOne(x => x.Address);

            modelBuilder.Entity<Customer>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<Day>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<Detail>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<Employee>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<Member>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<Project>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<Role>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<Team>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<User>().HasQueryFilter(x => !x.Deleted);

            modelBuilder.Entity<EmployeeStatus>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<MemberStatus>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<CustomerStatus>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<DayType>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<ProjectStatus>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<PricingType>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<PositionType>().HasQueryFilter(x => !x.Deleted);
            modelBuilder.Entity<MemberStatus>().HasQueryFilter(x => !x.Deleted);
        }

        public override async Task<int> SaveChangesAsync(CancellationToken token)
        {
            foreach (var entry in ChangeTracker.Entries().Where(x => x.State == EntityState.Deleted && x.Entity is BaseClass))
            {
                entry.State = EntityState.Modified;
                entry.CurrentValues["Deleted"] = true;
            }
            return await base.SaveChangesAsync(token);
        }
    }
}