﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using TimeKeeper.DAL;

namespace TimeKeeper.IDP
{
    public static class Config
    {
        public static List<TestUser> GetUsers()
        {
            List<TestUser> users = new List<TestUser>();
            using (TimeKeeperContext context = new TimeKeeperContext())
            {
                foreach (var user in context.Users.ToList())
                {
                    List<string> teamList = context.Members.Where(m => m.Employee.Id == user.Id).Select(t => t.Id.ToString()).ToList();
                    string teams = string.Join(",", teamList);
                    users.Add(new TestUser
                    {
                        SubjectId = user.Id.ToString(),
                        Username = user.Username,
                        Password = user.Password,
                        Claims = new List<Claim>
                        {
                        new Claim("given_name", user.Name.Split(' ')[0]),
                        new Claim("family_name", user.Name.Split(' ')[1]),
                        new Claim("role", user.Role),
                        new Claim("team", teams)
                        }
                    });
                }
            }
            return users;
        }

        public static IEnumerable<IdentityResource> GetResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Address(),
                new IdentityResource("roles", "Your roles", new List<string>{"role" }),
                new IdentityResource("teams", "Your Engagement(s)", new List<string>{"team" }),
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("timekeeper", "Time Keeper API", new List<string>{"role"})
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new List<Client> {
                new Client
                {
                    ClientName="TimeKeeper",
                    ClientId="tk2019",
                    //AllowedGrantTypes=GrantTypes.Hybrid,
                    //RedirectUris={ "https://localhost:44350/signin-oidc" },
                    //PostLogoutRedirectUris = { "https://localhost:44350/signout-callback-oidc" },
                    AllowedGrantTypes=GrantTypes.Implicit,
                    RedirectUris = { "http://localhost:3000/auth-callback" },
                    PostLogoutRedirectUris = {"http://localhost:3000/logout/callback"},
                    RequireConsent = false,

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Address,
                        "roles",
                        "timekeeper",
                        "teams"
                    },
                    ClientSecrets={new Secret("mistral_talents".Sha256())},

                    AllowOfflineAccess = true,
                    AllowAccessTokensViaBrowser = true,
                    AllowedCorsOrigins = {"http://localhost:3000", "http://localhost:3300", "https://localhost:44350", "http://localhost:8000", "http://localhost:8000"},
                    AccessTokenLifetime = 3600

                },
                new Client
                {
                    ClientName="TimeKeeperMobile",
                    ClientId="tk2019Mobile",
                    //AllowedGrantTypes=GrantTypes.Hybrid,
                    //RedirectUris={ "https://localhost:44350/signin-oidc" },
                    RedirectUris = { "http://localhost:3000/auth-callback" },
                    //PostLogoutRedirectUris = { "https://localhost:44350/signout-callback-oidc" },
                    PostLogoutRedirectUris = {"http://localhost:3000/logout/callback"},
                    AllowedGrantTypes=GrantTypes.Implicit,
                    RequireConsent = false,

                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.Address,
                        "roles",
                        "timekeeper",
                        "teams"
                    },
                    ClientSecrets={new Secret("mistral_talents".Sha256())},

                    AllowOfflineAccess = true,
                    AllowAccessTokensViaBrowser = true,
                    AllowedCorsOrigins = {"http://localhost:3000", "http://localhost:3300", "https://localhost:44350","https://192.168.44.148:44350"},
                    AccessTokenLifetime = 3600
                }
            };
        }
    }
}