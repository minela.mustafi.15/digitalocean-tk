﻿namespace TimeKeeper.Utilities.Logging
{
    public interface IDeltaLog
    {
        void Trace(string message);

        void Information(string message);

        void Warning(string message);

        void Debug(string message);

        void Error(string message);

        void NotFound(string message);

        void BadRequest(string message);

        void Fatal(string message);
    }
}