﻿using NLog;
using TimeKeeper.Uilities.MailService;

namespace TimeKeeper.Utilities.Logging
{
    public class DeltaLogger : IDeltaLog
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();

        protected IEmailService _email;

        public DeltaLogger()
        {
        }

        public DeltaLogger(IEmailService email)
        {
            _email = email;
        }

        public void Debug(string message)
        {
            logger.Debug(message);
        }

        public void Error(string message)
        {
            _email.Send(message);
            logger.Error(message);
        }

        public void NotFound(string message)
        {
            _email.Send(message);
            logger.Error(message);
        }

        public void BadRequest(string message)
        {
            _email.Send(message);
            logger.Error(message);
        }

        public void Information(string message)
        {
            logger.Info(message);
        }

        public void Trace(string message)
        {
            logger.Trace(message);
        }

        public void Warning(string message)
        {
            logger.Warn(message);
        }

        public void Fatal(string message)
        {
            logger.Fatal(message);
        }
    }
}