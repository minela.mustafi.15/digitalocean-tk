﻿using Microsoft.Extensions.Options;
using System.Net;
using System.Net.Mail;
using System.Text;
using TimeKeeper.Uilities.MailService;

namespace TimeKeeper.Utilities.MailService
{
    public class EmailService : IEmailService
    {
        private readonly EmailSettings _emailSettings;

        public EmailService()
        {
        }

        public EmailService(IOptions<EmailSettings> options)
        {
            _emailSettings = options.Value;
        }

        public void Send(string body)
        {
            string from = "phr@gmail.com";
            string mailTo = "nela.gloom@hotmail.com";
            string subject = "Error message from Time Keeper";
            using (MailMessage message = new MailMessage(from, mailTo, subject, body))
            {
                message.BodyEncoding = UTF8Encoding.UTF8;
                message.IsBodyHtml = true;
                message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                message.ReplyToList.Add(mailTo);

                using (SmtpClient client = new SmtpClient())
                {
                    client.Host = _emailSettings.Host;
                    client.Port = _emailSettings.Port;
                    client.EnableSsl = _emailSettings.EnableSsl;
                    client.Timeout = _emailSettings.Timeout;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = _emailSettings.UseDefaultCredentials;
                    client.Credentials = new NetworkCredential(_emailSettings.Username, _emailSettings.Password);

                    client.Send(message);
                };
            };
        }
    }
}