﻿namespace TimeKeeper.Uilities.MailService
{
    public interface IEmailService
    {
        void Send(string body);
    }
}