FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY TimeKeeper.API/*.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY TimeKeeper.API/. ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /app
COPY --from=build-env /app/out .

EXPOSE 8000

ENTRYPOINT ["dotnet", "TimeKeeper.API.dll"]